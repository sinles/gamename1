eventsTable.banditForestAttack = eventBase:extend("banditForestAttack", {
  Description = "You were ambushed by small group of bandits!"
});

eventsMetaTable.banditForestAttack = {
  MinDifficulty = 0,
  MaxDifficulty = 40,
}

function eventsTable.banditForestAttack:GetChoices( ... )
  local choice1 = GameName1.Data.EventChoice();
  choice1.Description = "Fight!";

  local outcomeFight = GameName1.Data.EventOutcome();
  outcomeFight:SetFight();
  outcomeFight.Fight.LevelName = "forest";

  outcomeFight.Fight:AddCreature(enemies.forestEnemies.bandit);
  outcomeFight.Fight:AddCreature(enemies.forestEnemies.bandit);
  outcomeFight.Fight:AddCreature(enemies.forestEnemies.bandit);
  choice1:AddOutcome(outcomeFight, 1);

  local choice2 = GameName1.Data.EventChoice();
  choice2.Description = "Flee";

  local outcomeFleeSucces = GameName1.Data.EventOutcome();
  choice2:AddOutcome(outcomeFight, 0.3);
  choice2:AddOutcome(outcomeFleeSucces, 0.3);

  return 
  {
    choice1, choice2
  }
end