eventsTable = {}
eventsMetaTable = {}

function eventsTable.Create(name)
	local cl = eventsTable[name]
	local ev = cl:new()

	return ev
end

function eventsTable.SelectAvailable(difficulty)
  local ret = linq.where(eventsMetaTable, function(v)
    local a = difficulty >= v.MinDifficulty and difficulty <= v.MaxDifficulty;
    return a
  end)

	return linq.keys(ret)
end

require("events.eventBase")
require("events.banditForestAttack")