skillsTable = {}

function skillsTable.Create(name, tileMap, creature)
  local skillClass = skillsTable[name]
  local skill = skillClass:new()

  skill.creature = creature
  skill.tileMap = tileMap
  skill:Inited();

  return skill
end

require "skills.skillBase"
require "skills.projectiles.projectileBase"
require "skills.slash"
require "skills.projectiles.fireball"
require "skills.projectiles.shootArrow"