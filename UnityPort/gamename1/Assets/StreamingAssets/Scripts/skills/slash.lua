slashSkill = skillBase:extend("slashSkill", {})
skillsTable.slash= slashSkill;


function slashSkill:Inited()
  self.SpriteName = "skills/slash"
end
function slashSkill:Apply(data)
  local attackTiles = self:GetTargetTiles(data)
  
  if(attackTiles == nil) then
    return nil 
  end
  for i,attackTile in ipairs(attackTiles) do
    local targetCreature = self.tileMap:GetCreatureFromTile(attackTile);
    if (targetCreature ~= nil) then
      local power = self.creature:GetAttackDamage();
      targetCreature:RecieveDamage(power);
    end
  end
end

function slashSkill:GetTargetTiles(data)
    if (data.targetTile == nil) then
        return nil;
    end

    local direction = data.targetTile - self.creature.Tile;

    if (direction.Magnitude > 1.001) then
        return nil;
    end

    local sideDirection = Point(direction.Y, direction.X);
    local attackTiles =
    {
        self.creature.Tile + direction,
        self.creature.Tile + direction + sideDirection,
        self.creature.Tile + direction - sideDirection,
    }
    return attackTiles
end
