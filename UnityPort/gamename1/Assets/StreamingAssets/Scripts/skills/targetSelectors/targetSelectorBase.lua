targetSelectorBase = class("SkillBase",
{
})

function targetSelectorBase:init(tileMap)
	self.tileMap = tileMap;
end

--selects creatures from tile map and does some operation on them
function targetSelectorBase:Get(data, handler)
	if(handler == nil) then
		print("handler nil")
	end

	local points = self:GetPoints(data)

	if(points == nil) then
		print("points nil")
	end

	local creatures = {}
	for i,v in ipairs(points) do
		local creature = self.tileMap:GetCreatureFromTile(v)
		if(creature ~= nil) then
			table.insert(creatures, creature);
		end
	end

	for i,c in ipairs(creatures) do
		handler(c)
	end
end

function targetSelectorBase:GetPoints(data)
	return {};
end
