require("skills.targetSelectors.targetSelectorBase")

rectTargetSelector = targetSelectorBase:extend("rectTargetSelector", {})

function rectTargetSelector:GetPoints(data)
  local center = data.position;
  local size = data.size;

  local fromX = center.x - data.size.x/2;
  local fromY = center.y - data.size.y/2;
  local toX = center.x + data.size.x/2;
  local toY = center.y + data.size.y/2;

  local result = {}
  for i=fromX,toX do
    for j=fromY,toY do
      table.insert(result, Point(i,j));
    end
  end

  return result;
end