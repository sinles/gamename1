skillBase = class("SkillBase",
{
	NeedsTarget = false,
	SpriteName = "empty",
	EnergyCost = 10,
	Range = 0
})


function skillBase:Inited()
end

function skillBase:Apply(data)
end

function skillBase:GetTargetTiles(data)
end

function skillBase:IsDone()
	return true;
end

function skillBase:IsInRange(tile)
	if(tile == nil) then
		print("IsInRange: Tile is nil!")
		return false;
	end

	if(self.Range == 0) then
		return true;
	end

	local direction = tile - self.creature.Tile;
	if (direction.Magnitude > self.Range) then
			return false;
	end

	return true;
end

function skillBase:Update()
end

function skillBase:OnDone()
end