require("skills.targetSelectors.rectTargetSelector")

skillsTable.fireball = projectileBase:extend("fireball", {})

function skillsTable.fireball:Inited()
  self.targetSelector = rectTargetSelector:new(self.tileMap);
  self.view = "projectiles/fireball";
  self.SpriteName = "projectiles/fireball";
  self.Range = 4
end

function skillsTable.fireball:GetTargetSelectorParams(data)
  return 
  {
    position = {x = data.targetTile.X, y = data.targetTile.Y},
    size = {x = 2, y = 2}
  };
end

function skillsTable.fireball:ApplyToCreature(creature)
   creature:RecieveDamage(5);
end

function skillsTable.fireball:OnDoneToTiles(tiles)
  for i,v in ipairs(tiles) do
    local fx = GameName1.Actors.FxActor("Fx/fireball_expl");
    fx.Position = GameName1.Levels.TileHelper.TileToPosition(v);
    fx:DestroyInSeconds(0.25);
    self.creature:AddActor(fx);
  end
end
