require("skills.targetSelectors.rectTargetSelector")

projectileBase = skillBase:extend("projectileBase", 
{
  targetSelector = nil,
  speed = 4,
  view = "not_set",
  elapsed = 0,
})

function projectileBase:Apply(data)
  if(not self:IsInRange(data.targetTile)) then
    return false;
  end

  local projectile = GameName1.Actors.ProjectileActor(data.targetTile, self.speed, self.view);
  projectile.Position = self.creature.Position;

  self.data = data;
  self.duration = projectile.FlightDuration;
  self.creature:AddActor(projectile);
end

function projectileBase:Update()
  self.elapsed = self.elapsed + UnityEngine.Time.deltaTime;
end

function projectileBase:IsDone()
  if(self.elapsed >= self.duration) then
    return true;
  end

  return false;
end
function projectileBase:OnDone()
  if(self.data ~= nil) then
    self.targetSelector:Get(self:GetTargetSelectorParams(self.data), 
    function (creature)
      self:ApplyToCreature(creature);
    end);

    local tiles = self.targetSelector:GetPoints(self:GetTargetSelectorParams(self.data));
    self:OnDoneToTiles(tiles)
  end
end

function projectileBase:OnDoneToTiles(tiles)
end

function projectileBase:GetTargetTiles(data)
  local tiles = self.targetSelector:GetPoints(self:GetTargetSelectorParams(data));
  return tiles;
end

function projectileBase:GetTargetSelectorParams(data)

end


function projectileBase:ApplyToCreature(creature)

end