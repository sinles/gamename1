require("skills.targetSelectors.singleTargetSelector")

skillsTable.shootArrow = projectileBase:extend("shootArrow", {})

function skillsTable.shootArrow:Inited()
  self.targetSelector = singleTargetSelector:new(self.tileMap);
  self.view = "projectiles/projectileArrow";
  self.SpriteName = "projectiles/arrow";
  self.Range = 4
end

function skillsTable.shootArrow:GetTargetSelectorParams(data)
  return 
  {
    position = data.targetTile
  };
end

function skillsTable.shootArrow:ApplyToCreature(creature)
   creature:RecieveDamage(4);
end

function skillsTable.shootArrow:OnDoneToTiles(tiles)
end
