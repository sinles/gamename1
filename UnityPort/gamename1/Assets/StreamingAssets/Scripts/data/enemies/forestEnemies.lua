local createCreature = function(fillFunction)
  local creature = GameName1.Data.UnitData()
  fillFunction(creature)
  return creature
end

enemies.forestEnemies = 
{
  bandit = createCreature(
    function (creature)
      creature.MaxHealth = 6;
      creature.MaxActionPoints = 5;
      creature.Damage = 2;
      creature.CritChance = 0.4;
      creature.Sprite = "Bandit";
      creature:AddSkill("slash");
      creature:AddSkill("fireball");
    end),
  banditLeader = createCreature(
    function (creature)
      creature.MaxHealth = 12;
      creature.MaxActionPoints = 5;
      creature.Damage = 3;
      creature.CritChance = 0.4;
      creature.Sprite = "Bandit";
      creature:AddSkill("slash");
      creature:AddSkill("fireball");
    end),
}

