levelTable = {}

function levelTable.Create(name)
  local levelClass = levelTable[name]
  local level = levelClass:new()


  return level
end

require("levels.level")
require("levels.forest")