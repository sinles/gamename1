forestLevel = levelBase:extend("forestLevel", {})
levelTable["forest"] = forestLevel;

function forestLevel:GenTileMap()
  local tileMap = GameName1.Tiles.TileMap(12, 9)

  local map = 
  {
		"gggggggggggg",
		"gggffgggggff",
		"gggffgggggff",
		"gggggfffggff",
		"gfffffffggff",
		"gfffffffgggf",
		"gggggfffgggg",
		"gggggggggggg",
		"gfgggggggggf"
  }

  for i,v in ipairs(map) do
      tileMap:CreateTileRow(v, i - 1)
  end

  return tileMap
end


function forestLevel:GenCreatures()
  local creature = GameName1.Actors.CreatureInfo()

  creature.Data.MaxHealth = 6;
  creature.Data.MaxActionPoints = 5;
  creature.Data.Damage = 2;
  creature.Data.CritChance = 0.4;
  creature.Data.Sprite = "Bandit";
  creature.Data:AddSkill("slash");

  return {creature, creature, creature};
end