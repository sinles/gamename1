aiTable.aiHumanPlayer = aiBase:extend("aiHumanPlayer", {})

function aiTable.aiHumanPlayer:Update()
  self:UpdateInput();
end

function aiTable.aiHumanPlayer:UpdateInput()
  if (self.creature ~= null) then
    local input = UnityEngine.Input;
    if (input.GetMouseButtonDown(0)) then
      self:OnClick();
    elseif(input.GetMouseButtonDown(1)) then
      self.creature:ResetSkill();
    elseif (self:GetMovementInput(UnityEngine.KeyCode.LeftArrow)) then
      self:MoveInDirection(-1, 0);
    elseif (self:GetMovementInput(UnityEngine.KeyCode.RightArrow)) then
      self:MoveInDirection(1, 0);
    elseif (self:GetMovementInput(UnityEngine.KeyCode.UpArrow)) then
      self:MoveInDirection(0, 1);
    elseif (self:GetMovementInput(UnityEngine.KeyCode.DownArrow)) then
      self:MoveInDirection(0, -1);
    elseif (self:GetMovementInput(UnityEngine.KeyCode.S)) then
      self.creature:Skip();
    elseif (input.GetKeyDown(UnityEngine.KeyCode.E)) then
      self.creature:Use();
    end
  end
end

function aiTable.aiHumanPlayer:OnClick()
  if (UnityEngine.EventSystems.EventSystem.current:IsPointerOverGameObject()) then
    return nil;
  end

  if(self.creature.Abilities.SelectedSkillId == nil) then 
    self:MoveToMouse();
  else
    local tileUnderMouse = GameName1.Levels.TileHelper.GetTileUnderMouse();

    local input = GameName1.Network.Commands.UseSkillCmd.Input();
    input:SetTarget(tileUnderMouse);
    input.CreatureId = self.creature.Id;
    input.SkillId = self.creature.Abilities.SelectedSkillId;

    GameName1.Network.Commands.LuaCommandRunner.UseSkillCmd(input);
  end


end

function aiTable.aiHumanPlayer:MoveToMouse()
  local selectedTile = GameName1.Levels.TileHelper.GetTileUnderMouse();
  self:SendMoveCmd(selectedTile);
end

function aiTable.aiHumanPlayer:GetMovementInput(key)
  return UnityEngine.Input.GetKeyDown(key);
end

function aiTable.aiHumanPlayer:MoveInDirection(x, y)
  x = x + self.creature.Tile.X;
  y = y + self.creature.Tile.Y;

  self:SendMoveCmd(Point(x, y));
end

function aiTable.aiHumanPlayer:SendMoveCmd(target)
  local input = GameName1.Network.Commands.MoveCreatureCmd.Input()
  input.CreatureId = self.creature.Id;
  input.Target = target;

  GameName1.Network.Commands.LuaCommandRunner.MoveCmd(input);
end

-- function aiTable.aiHumanPlayer:GetCreatureOnTile(point)
-- {
--   foreach (var aliveCreature in AliveCreatures)
--   {
--     if (aliveCreature.Tile == point)
--     {
--       return aliveCreature;
--     }
--   }

--   return null;
-- }