aiSimple = aiBase:extend("aiSimple", {})
aiTable["aiSimple"] = aiSimple;

function aiSimple:Update()
	local closestTarget = self.fightManager:GetClosestOppositeCreature(self.fightPlayer, self.creature.Tile)
	if(closestTarget == nil) then return nil end

	local x = closestTarget.TileX
	local y = closestTarget.TileY


	local d = closestTarget.Tile:Distance(self.creature.Tile);
	if(d <= 1.01) then
		self.creature:UseSkill("fireball", {targetTile = closestTarget.Tile})
	else
		if(self.creature.ActionPoints > 1) then
			if(not self.creature:Move(x, y, 1)) then
				self.creature:Skip()
			end
		else
			if(not self.creature:Move(x, y, 0)) then
				self.creature:Skip()
			end
		end
	end
end
