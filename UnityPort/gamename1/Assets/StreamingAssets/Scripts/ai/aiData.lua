aiTable = {}

function aiTable.Create(name, fightManager, fightPlayer, creature)
	local cl = aiTable[name]
	local ai = cl:new()

	ai.creature = creature
	ai.fightManager = fightManager
	ai.fightPlayer = fightPlayer

	return ai
end

require "ai.aiBase"
require "ai.aiSimple"
require "ai.aiHumanPlayer"
require "ai.aiHumanDummy"
