class = require "oop.30log"
linq = require "linq"

local function main()
    require "data.enemies.enemies"

    require "skills.skillsData"
    require "ai.aiData"
    require "levels.levelData"
    require "events.eventsData"
end

main()