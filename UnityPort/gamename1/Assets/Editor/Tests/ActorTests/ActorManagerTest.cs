﻿using GameName1.Actors;
using NUnit.Framework;

namespace Tests.ActorTests
{
  public class ActorManagerTest
  {
    private class UpdateActorCreator : Actor
    {
      public UpdateActorChecker InnerActor;

      public override void Update()
      {
        InnerActor = new UpdateActorChecker();
        AddActor(InnerActor);
      }
    }

    private class UpdateActorChecker:Actor
    {
      public bool UpdateCalled;
      public bool CreateCalled;

      public override void Created()
      {
        CreateCalled = true;
      }

      public override void Update()
      {
        UpdateCalled = true;
      }
    }

    private class DestroyedActorOnUpdate : Actor
    {
      public bool DestroyedCalled;

      public override void Update()
      {
        Destroy();
      }

      public override void Destroyed()
      {
        DestroyedCalled = true;
      }
    }

    private ActorManager _manager;

    [SetUp]
    public void Init()
    {
      _manager = new ActorManager();
    }


    [Test]
    public void Add()
    {
      bool added = false;
      _manager.ActorAdded += a => { added = true; };

      var actor = new FakeActor();
      _manager.Add(actor);
      Assert.IsTrue(_manager.Actors.Contains(actor));
      Assert.IsTrue(added);
    }

    [Test]
    public void Remove()
    {
      bool removed = false;
      _manager.ActorRemoved += a => { removed = true; };
      var actor = new FakeActor();

      _manager.Add(actor);
      _manager.Remove(actor);
      Assert.IsFalse(_manager.Actors.Contains(actor));
      Assert.IsTrue(removed);
    }

    [Test]
    public void AddOnUpdate()
    {
      var actor = new UpdateActorCreator();
      _manager.Add(actor);
      _manager.Update();

      Assert.IsTrue(_manager.Actors.Contains(actor.InnerActor));
      Assert.IsTrue(actor.InnerActor.UpdateCalled);
      Assert.IsTrue(actor.InnerActor.CreateCalled);
    }

    [Test]
    public void RemoveOnUpdate()
    {
      var actor = new DestroyedActorOnUpdate();
      _manager.Add(actor);
      _manager.Update();

      Assert.IsFalse(_manager.Actors.Contains(actor));
      Assert.IsTrue(actor.DestroyedCalled);
    }
  }
}
