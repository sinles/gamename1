﻿using GameName1.Actors;
using NUnit.Framework;
using UnityEngine;

namespace Tests.ActorTests
{
  internal class FakeActor : Actor
  {
  }

  public class ActorTest
  {
    private Actor _actor;

    [SetUp]
    public void Init()
    {
      _actor = new FakeActor();
    }

    [Test]
    public void ChangePositionByVector()
    {
      bool changed = false;

      _actor.PositionChanged += () =>
      {
        changed = true;
      };

      _actor.Position = new Vector2(3, 4);

      Assert.IsTrue(changed);

      Assert.AreEqual(_actor.Position.x, 3);
      Assert.AreEqual(_actor.Position.y, 4);
    }

    [Test]
    public void ChangePositionByValues()
    {
      int changed = 0;

      _actor.PositionChanged += () =>
      {
        changed++;
      };

      _actor.X = 6;
      _actor.Y = 8;

      Assert.IsTrue(changed == 2);

      Assert.AreEqual(_actor.Position.x, 6);
      Assert.AreEqual(_actor.Position.y, 8);
    }

    [Test]
    public void Destroy()
    {
      _actor.Destroy();
      Assert.IsTrue(_actor.IsDestroyed);
    }

    [Test]
    public void AddNewActor()
    {
      var other = new FakeActor();
      var other2 = new FakeActor();
      _actor.AddActor(other);
      _actor.AddActor(other2);

      var addedActors = _actor.PurgeAddedActors();
      Assert.IsTrue(addedActors.Count == 2);
      Assert.IsTrue(addedActors.Contains(other));
      Assert.IsTrue(addedActors.Contains(other2));

      Assert.IsTrue(_actor.PurgeAddedActors() == null);
    }
  }
}