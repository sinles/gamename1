﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameName1.Actors;
using GameName1.Data;
using GameName1.Lua;
using GameName1.Tiles;
using Glide;
using NSubstitute;
using NUnit.Framework;
using UnityEngine;

namespace Tests.ActorTests
{
  public class CreatureActorTest
  {
    private CreatureActor _actor;
    private TileMap _tileMap;

    [SetUp]
    public void Init()
    {
      _tileMap = new TileMap(15, 15);
      _tileMap.FillWith(new TileGrass());
      _actor = CreateActor();
      _actor.SetTile(new Point(0,0), true);

      new Tweener();
    }

    [Test]
    public void UseAction()
    {
      _actor.UseActionPoint();
      _actor.UseActionPoint();
      Assert.IsTrue(_actor.ActionPoints == _actor.Info.Data.MaxActionPoints - 2);
    }

    [Test]
    public void ResetActionPoints()
    {
      _actor.UseActionPoint();
      _actor.UseActionPoint();
      _actor.ResetActionPoints();

      Assert.IsTrue(_actor.ActionPoints == _actor.Info.Data.MaxActionPoints);
    }

    [Test]
    public void UseAllActionPoints()
    {
      _actor.UseActionPoint();
      _actor.UseAllActionPoints();

      Assert.IsFalse(_actor.HasActionPoints());
    }

    [Test]
    public void RecieveDamage()
    {
      _actor.RecieveDamage(10);
      Assert.AreEqual((int)_actor.Health, 90);

      _actor.RecieveDamage(90);

      Assert.IsTrue(_actor.IsDead);
    }

    [Test]
    public void MoveSimple()
    {
      int count = _actor.ActionPoints;

      _actor.Move(3, 0);

      UpdateMovement(count);

      Assert.IsTrue(_actor.Tile == new Point(3,0), _actor.Tile.ToString());
    }

    [Test]
    public void MoveAroundWall()
    {
      _tileMap[1, 0] = new TileWall();

      int count = _actor.ActionPoints;

      Point target = new Point(2, 0);
      
      _actor.Move(target);

      UpdateMovement(count);

      Assert.IsTrue(_actor.Tile == new Point(2, 1), _actor.Tile.ToString());

      _actor.ResetActionPoints();

      _actor.Move(target);
      UpdateMovement(1);

      Assert.IsTrue(_actor.Tile == new Point(2, 0), _actor.Tile.ToString());
    }

    [Test]
    public void AttackCreature()
    {
      var otherActor = CreateActor(1);
      otherActor.SetTile(new Point(2,0), true);

      float healthChange = 0f;
      otherActor.HealthChanged += f =>
      {
        healthChange += f;
      };

      int count = _actor.ActionPoints;

      Point target = new Point(2, 0);

      _actor.Move(target);

      UpdateMovement(count);

      Assert.IsTrue(_actor.Tile == new Point(1, 0), _actor.Tile.ToString());
      Assert.IsTrue(healthChange < 0f, healthChange.ToString());
    }

    [Test]
    public void InitSkills()
    {
      var stubLoader = Substitute.For<SkillLuaLoader>();

      stubLoader
        .GetSkill(null, null, null)
        .Returns(new SkillLuaWrapper(null, null, null) { Name = "FakeSkill" });

      _actor.InitSkills(stubLoader);
      Assert.AreEqual(1, _actor.Abilities.GetAvailableSkills().Count);
    }

    private CreatureActor CreateActor(int side = 0)
    {
      var actor = new CreatureActor(_tileMap, new CreatureInfo(new UnitData
      {
        Damage = 1,
        MaxHealth = 100,
        MaxStamina = 100,
        MaxActionPoints = 3,
        SkillIds = new List<string> { "FakeSkill"}
      }), side);
      _tileMap.AddActor(actor);
      return actor;
    }

    private void UpdateMovement(int count)
    {
      for (int i = 0; i <= count; i++)
      {
        _actor.Update();
        Tweener.I.Update(1.0f);
      }
    }
  }
}
