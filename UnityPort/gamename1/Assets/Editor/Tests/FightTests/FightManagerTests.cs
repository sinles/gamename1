﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameName1.Fight;
using NUnit.Framework;

namespace Tests.FightTests
{
  public class FightManagerTests
  {
    FightManager _fightManager;

    [SetUp]
    public void Init()
    {
      _fightManager = new FightManager();

      _fightManager.SetPlayers(new List<FightPlayer>()
      {
        CreatePlayer(0),
        CreatePlayer(0),
        CreatePlayer(1),
        CreatePlayer(1)
      });

    }

    private static FakeFightPlayer CreatePlayer(int side)
    {
      var player = new FakeFightPlayer();
      player.Side = side;
      player.CreateFakeCreatures(side);
      return player;
    }

    [Test]
    public void StartNewStep()
    {
      var firstPlayer = _fightManager.CurrentFightPlayer;

      _fightManager.Start();
      _fightManager.EndStep();

      Assert.AreNotEqual(firstPlayer, _fightManager.CurrentFightPlayer, "Should have been changed");
    }

    [Test]
    public void GetOpposite()
    {
      var opposite = _fightManager.GetOpposite(_fightManager.Players.FirstOrDefault());
      Assert.AreEqual(opposite.Count, 2);
      Assert.IsTrue(opposite.All(p => p.Side == 1));
    }

    [Test]
    public void EndFight()
    {
      foreach (var fightPlayer in _fightManager.Players.Where(p => p.Side == 0))
      {
        foreach (var aliveCreature in fightPlayer.AliveCreatures)
        {
          aliveCreature.RecieveDamage(10000);
        }
      }

      Assert.IsTrue(_fightManager.FightEnded);
      Assert.IsTrue(_fightManager.MainPlayerLost);
    }
  }
}
