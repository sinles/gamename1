﻿using System.Collections.Generic;
using GameName1.Actors;
using GameName1.Data;
using GameName1.Fight;
using GameName1.Tiles;
using NUnit.Framework;

namespace Tests.FightTests
{
  internal class FakeFightPlayer : FightPlayer
  {
    public System.Action OnUpdate;

    public override void StartStep()
    {
      base.StartStep();
    }

    protected override void InternalUpdate()
    {
      base.InternalUpdate();
      SelectedCreatureActor.Skip();
      if (OnUpdate != null)
        OnUpdate();
    }

    protected override void OnCreaturesSet()
    {
      base.OnCreaturesSet();
      Assert.AreEqual(3, Creatures.Count, "Creatures count");
    }

    public void CreateFakeCreatures(int side = 0)
    {
      SetCreatures(new List<CreatureActor>
      {
        CreateActor(side),
        CreateActor(side),
        CreateActor(side)
      });
    }

    private CreatureActor CreateActor(int side = 0)
    {
      var actor = new CreatureActor(new TileMap(100, 100), new CreatureInfo(new UnitData
      {
        Damage = 1,
        MaxHealth = 100,
        MaxStamina = 100,
        
        MaxActionPoints = 3,
      }), side);

      return actor;
    }
  }

  public class FightPlayerTests
  {
    private FakeFightPlayer _fightPlayer;

    [SetUp]
    public void Init()
    {
      _fightPlayer = new FakeFightPlayer();
      _fightPlayer.CreateFakeCreatures();
    }


    [Test]
    public void StartStep_CreatureSelection()
    {
      _fightPlayer.StartStep();
      Assert.IsNotNull(_fightPlayer.SelectedCreatureActor);
    }

    [Test]
    public void StartStep_CreatureSelection_WhenSomeDead()
    {
      _fightPlayer.AliveCreatures[0].RecieveDamage(5000);
      _fightPlayer.AliveCreatures[1].RecieveDamage(5000);
      _fightPlayer.StartStep();
      Assert.IsNotNull(_fightPlayer.SelectedCreatureActor);
    }

    [Test]
    public void StartStep_CreatureSelection_WhenAllDead()
    {
      _fightPlayer.AliveCreatures[0].RecieveDamage(5000);
      _fightPlayer.AliveCreatures[0].RecieveDamage(5000);
      _fightPlayer.AliveCreatures[0].RecieveDamage(5000);
      _fightPlayer.StartStep();
      Assert.IsNotNull(_fightPlayer.SelectedCreatureActor);
      Assert.IsTrue(_fightPlayer.StepEnded);
    }

    [Test]
    public void StartStep_AllCreaturesResetPoints()
    {
      foreach (var creature in _fightPlayer.AliveCreatures)
      {
        creature.UseAllActionPoints();
      }

      _fightPlayer.StartStep();

      foreach (var creature in _fightPlayer.AliveCreatures)
      {
        Assert.IsTrue(creature.HasActionPoints(), "Creature should have action points reset");
      }
    }

    [Test]
    public void Update()
    {
      bool updated = false;
      bool updated2 = false;

      _fightPlayer.StartStep();
      _fightPlayer.Update();
      _fightPlayer.OnUpdate += () => { updated = true; };
      _fightPlayer.StartStep();
      _fightPlayer.OnUpdate += () => { updated2 = true; };
      _fightPlayer.Update();

      Assert.IsTrue(updated && updated2, "Internal update should have been called");
    }

    [Test]
    public void EndStep()
    {
      _fightPlayer.StartStep();

      for (int i = 0; i < 4; i++)
      {
        _fightPlayer.Update();
      }

      Assert.IsTrue(_fightPlayer.StepEnded);
    }

    #region Helpers

    #endregion
  }
}
