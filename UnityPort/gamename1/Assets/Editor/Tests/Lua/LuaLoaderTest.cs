﻿using System;
using System.Diagnostics;
using GameName1.Actors;
using GameName1.Data;
using GameName1.Fight;
using GameName1.Lua;
using GameName1.Tiles;
using NUnit.Framework;
using SLua;

namespace Tests.Lua
{
  public class LuaLoaderTest
  {
    private LuaRootLoader _rootLoader;

    [SetUp]
    public void Init()
    {
      _rootLoader = new LuaRootLoader(false);
    }

    [Test]
    public void TestLoad()
    {
      bool loaded = false;

      loaded = Load();

      Assert.IsTrue(loaded, "loading timeout");
    }

    private bool Load()
    {
      bool loaded = false;
      _rootLoader.LoadFinished += () =>
      {
        loaded = true;

        if (_rootLoader.AiLoader == null)
          UnityEngine.Debug.LogError("Ai loader not loaded!!! ");

        Assert.IsNotNull(_rootLoader.AiLoader);
      };

      try
      {
        _rootLoader.Init();
      }
      catch (Exception e)
      {
        UnityEngine.Debug.LogError("Not compiled!!! " + e);
        Assert.Fail(e.ToString());
      }

      TimeSpan maxDuration = TimeSpan.FromSeconds(2);
      Stopwatch sw = Stopwatch.StartNew();

      while (!loaded)
      {
        if (sw.Elapsed > maxDuration)
        {
          UnityEngine.Debug.LogError("Not loaded!!! ");
          break;
        }
      }
      return loaded;
    }
    
    [Test]
    public void TestSimpleSkillLoad()
    {
      Load();

      var tileMap = new TileMap(30, 30);
      tileMap.FillWith(new TileEmpty());

      var creature = new CreatureActor(tileMap, new UnitData());

      var skill = _rootLoader.SkillLoader.GetSkill("slash", tileMap, creature);
      skill.Apply(new DictLua().ToLua(_rootLoader.State));
    }

    [Test]
    public void TestSimpleAiBehaviourLoad()
    {
      Load();

      var tileMap = new TileMap(30, 30);
      tileMap.FillWith(new TileEmpty());

      var creature = new CreatureActor(tileMap, new UnitData());

      var fightManager = new FightManager();
      var fightPlayer = new FakeFightPlayer();

      var behaviour = _rootLoader.AiLoader.GetBehaviour("aiSimple", fightManager, fightPlayer, creature);
      behaviour.Update();
    }

    private class FakeFightPlayer:FightPlayer
    {
      
    }
  }
}
