﻿using System;
using System.Collections.Generic;
using GameObjectExtensions;
using UnityEngine;
using UnityEngine.UI;
using Event = GameName1.Data.Event;

namespace GameName1.UI
{
  public class EventStartedUIControl : UIView
  {
    public GameObject ChoicePrefab;
    public Text Header;
    public Transform ChoicesRoot;

    private readonly List<GameObject> _buttons = new List<GameObject>();
    private EventChoiceState _state;

    public void SetState(EventChoiceState eventChoiceState)
    {
      _state = eventChoiceState;
      _state.TravelEventChanged += StateOnTravelEventChanged;
      Rebuild();
    }

    private void StateOnTravelEventChanged()
    {
      Clear();
      Rebuild();
    }

    public void Rebuild()
    {
      Header.text = _state.TravelEvent.Description;

      Clear();
      foreach (var eventChoice in _state.TravelEvent.Choices)
      {
        var choice = ChoicePrefab.Create();
        choice.transform.SetParent(ChoicesRoot, false);

        choice.GetComponentInChildren<Text>().text = eventChoice.Description;
        choice.GetComponentInChildren<Button>().onClick.AddListener(() =>
        {
          _state.MakeChoice(eventChoice);
          //todo:
        });
      }
    }

    private void Clear()
    {
      ClearRoot(ChoicesRoot);

      //foreach (var button in _buttons)
      //{
      //  button.Chosen -= EventChoiceButtonOnChosen;
      //}

      _buttons.Clear();
    }
  }
}
