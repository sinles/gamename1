﻿using UnityEngine;
using System.Collections;
using Glide;
using UnityEngine.UI;

public class ProgressBar : MonoBehaviour
{
    public float ChangeSpeed = 0.5f;
    public EaseType EaseType;

    private Slider _slider;
    private Tween _tween;

	void Start ()
	{
	    _slider = GetComponent<Slider>();
    }

    public void SetValue(float value)
    {
        if (_tween != null)
        {
            _tween.Cancel();
        }


        if (_slider == null)
        {
            return;
        }
        _tween = Tweener.I.Tween(_slider, new { value = value }, ChangeSpeed).Ease(Ease.FromType(EaseType));

    }
}
