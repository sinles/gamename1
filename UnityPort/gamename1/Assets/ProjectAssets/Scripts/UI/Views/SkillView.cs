﻿using System;
using GameName1.Actors;
using GameName1.Data;
using GameObjectExtensions;
using UnityEngine;

public class SkillView : UIView
{
  public RectTransform ButtonsRoot;
  public SkillButtonView ButtonPrefab;
  private FightState _state;
  private CreatureActor _creature;


  public void SetState(FightState state)
  {
    _state = state;

    foreach (var player in state.FightManager.Players)
    {
      player.CreatureStepStarted += PlayerOnCreatureStepStarted;
    }
  }

  private void PlayerOnCreatureStepStarted(CreatureActor creature)
  {
    _creature = creature;
    UpdateButtons();
  }

  private void UpdateButtons()
  {
    ClearRoot(ButtonsRoot);

    foreach (var skill in _creature.Abilities.GetAvailableSkills())
    {
      var spritePath = skill.SkillWrapper.SpriteName;
      var sprite = Resources.Load<Sprite>("Graphics/Sprites/" + spritePath);

      if (sprite == null)
      {
        Debug.LogError("Sprite not loaded: "+ spritePath);
      }

      var button = ButtonPrefab.gameObject.Create<SkillButtonView>();
      button.Image.sprite = sprite;
      button.gameObject.transform.SetParent(ButtonsRoot, false);
      button.Button.onClick.AddListener(() => OnButtonClick(skill));
    }
  }

  private void OnButtonClick(SkillData skill)
  {
    _creature.SelectSkill(skill.SkillId);
  }
}
