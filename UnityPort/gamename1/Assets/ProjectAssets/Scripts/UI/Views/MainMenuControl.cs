﻿using System.Collections;
using System.Collections.Generic;
using GameName1.Actors;
using GameName1.Data;
using GameName1.Fight;
using GameName1.Network;
using GameName1.Network.Commands;
using GameName1.Travel;
using UnityEngine;
using UnityEngine.UI;

namespace GameName1.UI
{
  public class MainMenuControl : UIView
  {
    public InputField Input;

    public override void OnEnable()
    {
      NetworkManager.I.Connected += NetworkManagerOnConnected;
      PlayerProfileManager.I.Added += OnAdded;


      Locations.Init();
      TileMaps.Init();

#if UNITY_EDITOR
      StartCoroutine(FakeStart());
#endif
    }

    private IEnumerator FakeStart()
    {
      StartClicked();

      yield return new WaitForSeconds(0.5f);
      SendClicked();
      yield return null;
      SendClicked();
    } 

    private void OnAdded(PlayerProfile playerProfile)
    {
      if (PlayerProfileManager.I.GetAll().Count >= 2)
      {
        StartFight();
      }
    }

    public void SendClicked()
    {
      new CreatePlayerProfileCmd(new CreatePlayerProfileCmd.Input
      {
        Id = RandomTool.U.NextInt(),
        Nickname = NetworkManager.I.IsServer ? "Boris" : "SomeGuy"
      }, true).Execute();
    }

    public void JoinClicked()
    {
      string value = Input.text;

      if (string.IsNullOrEmpty(value))
      {
        value = "localhost";
      }

      NetworkManager.I.Join(value);
    }

    public void StartClicked()
    {
      NetworkManager.I.Create();
    }

    private void NetworkManagerOnConnected()
    {
      new CreatePlayerProfileCmd(new CreatePlayerProfileCmd.Input
      {
        Id = RandomTool.U.NextInt(),
        Nickname = NetworkManager.I.IsServer ? "Boris" : "SomeGuy"
      }, true).Execute();
    }

    private void StartFight()
    {
   //   List<CreatureInfo> creatures = new List<CreatureInfo>();

      //AddBandit(creatures);
      //AddBandit(creatures);
      //AddBandit(creatures);
      //AddBandit(creatures);


      //var eventsLoader = AppRoot.I.LuaLoader.EventsLoader;
      //var available = eventsLoader.SelectAvailable(2);

      //var resultEvent = RandomTool.D.NextChoice(available);

      //Debug.Log(resultEvent);
      //var @event = eventsLoader.GetEvent(resultEvent);


      //var level = AppRoot.I.LuaLoader.LevelLoader.GetLevel("forest");
      //var creatures = level.GenCreatures();
      //FightInfo fightInfo = new FightInfo {Creatures = creatures};

      //AppRoot.I.SetState(new FightState(fightInfo));

      AppRoot.I.SetState(new EventChoiceState());
    }

    //private static void AddBandit(List<CreatureInfo> creatures)
    //{
    //  var creature = new CreatureInfo();
    //  creature.Data.MaxHealth = 6;
    //  creature.Data.MaxActionPoints = 5;
    //  creature.Data.Damage = 2;
    //  creature.Data.CritChance = 0.4f;
    //  creature.Data.Sprite = "Bandit";
    //  creature.Data.SkillIds= new List<string>()
    //  {
    //    "slash"
    //  };
    //  creatures.Add(creature);
    //}
  }
}
