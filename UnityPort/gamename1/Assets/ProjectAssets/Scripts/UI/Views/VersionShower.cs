﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class VersionShower : MonoBehaviour
{
    private const string cVersion = "0.0.0.1";

    public void Start()
    {
        GetComponent<Text>().text = cVersion;
    }
}
