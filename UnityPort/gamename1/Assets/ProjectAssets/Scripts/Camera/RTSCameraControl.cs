﻿using UnityEngine;
using GameName1.Actors;
using GameName1.Levels;

public class RTSCameraControl : MonoBehaviour
{
  public float MouseBorder = 0.1f;
  public float Sensetivity = 1f;
  public static RTSCameraControl I { get; private set; }

  public Rect ClampRect;

  void Awake()
  {
    I = this;
  }

  void Start()
  {
    CameraControl.I.Target = transform;
  }

  void OnDisable()
  {
  }

  void Update()
  {
    UpdatePosition();
  }

  private void UpdatePosition()
  {
    Vector2 mousePos = CameraControl.I.Camera.ScreenToViewportPoint(Input.mousePosition);
    mousePos.x = Mathf.Clamp(mousePos.x, 0, 1);
    mousePos.y = Mathf.Clamp(mousePos.y, 0, 1);

    if (mousePos.x > 1 || mousePos.x < 0)
    {
      mousePos.x = 0;
    }

    if (mousePos.y > 1 || mousePos.y < 0)
    {
      mousePos.y = 0;
    }


    Vector2 movement = new Vector2();

    if (mousePos.x < MouseBorder)
    {
      movement.x = mousePos.x - MouseBorder;
    }
    else if (mousePos.x > 1f - MouseBorder)
    {
      movement.x = 1f - mousePos.x - MouseBorder;
      movement.x *= -1;
    }

    if (mousePos.y < MouseBorder)
    {
      movement.y = mousePos.y - MouseBorder;
    }
    else if (mousePos.y > 1f - MouseBorder)
    {
      movement.y = 1f - mousePos.y - MouseBorder;
      movement.y *= -1;
    }

    float spd = movement.magnitude;
    if (movement != Vector2.zero)
    {
      movement.Normalize();
      movement = movement*Mathf.Clamp(spd, -Sensetivity, Sensetivity);


      transform.position = (Vector2) transform.position + movement*Sensetivity*Time.deltaTime;

      var pos = transform.position;
      //pos.x = Mathf.Clamp(pos.x, 0, TileHelper.TileSize*World.I.TileMap.Width);
      //pos.y = Mathf.Clamp(pos.y, 0, TileHelper.TileSize*World.I.TileMap.Height);

      pos.x = Mathf.Clamp(pos.x, ClampRect.x, ClampRect.x + ClampRect.width);
      pos.y = Mathf.Clamp(pos.y, ClampRect.y, ClampRect.y + ClampRect.height);

      transform.position = pos;
    }
  }
}
