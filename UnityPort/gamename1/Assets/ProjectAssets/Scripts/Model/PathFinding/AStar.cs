﻿using System;
using System.Collections.Generic;
using GameName1.Tiles;
using UnityEngine;

namespace GameName1.PathFinding
{
    public delegate void AsyncMethodCaller(Point startPosition, Point endPosition);

    public class AStar
    {
        private Path path;

        public Path Path
        {
            get
            {
                if (HasEnded)
                {
                    return path;
                }
                return null;
            }
        }

        private TileMap _tileMap;
        private readonly Func<Tile, bool> _tileSolidChecker;

        public byte[,] FogMap;

        public bool HasEnded = false;
        public bool ImprovedPathFinding = false;
        public bool IgnoreObstacles = false;
        public bool DiagonalMovesAllowed = false;



        byte[,] pointMap;
        BinaryHeap OpenList;

        public AStar(TileMap tileMap, Func<Tile, bool> tileSolidChecker = null)
        {
            _tileMap = tileMap;
            _tileSolidChecker = tileSolidChecker;
        }

        public void FoundWayAsync(Point startPosition, Point endPosition)
        {
            HasEnded = false;
            AsyncMethodCaller caller = new AsyncMethodCaller(FindPath);
            caller.BeginInvoke(startPosition, endPosition, null, null);
        }
        public void FindPath(Point startPosition, Point endPosition)
        {
            path = new Path();

            var wayList = FindCellWay(startPosition, endPosition);
            if (wayList != null)
            {
                foreach (var p in wayList)
                    path.AddPoint(new Vector2(p.X * Levels.TileHelper.TileSize + Levels.TileHelper.TileSize/2f,// + Settings.CellSize/2,
                                              p.Y * Levels.TileHelper.TileSize + Levels.TileHelper.TileSize/2f));// + Settings.CellSize/2));
            }
            HasEnded = true;
        }

        public List<Point> FindCellWay(Point startCell, Point endCell)
        {
            pointMap = new byte[_tileMap.Width, _tileMap.Height];
            OpenList = new BinaryHeap();

            WayPoint startPoint = new WayPoint(startCell, null, true);
            startPoint.CalculateCost(_tileMap, endCell,0, ImprovedPathFinding);

            OpenList.Add(startPoint);

            while (OpenList.Count != 0)
            {
                WayPoint node = OpenList.Get();
                if (node.PositionX == endCell.X && node.PositionY == endCell.Y)
                {
                    WayPoint nodeCurrent = node;
                    var points = new List<Point>();

                    while (nodeCurrent != null)
                    {
                        points.Insert(0, new Point(nodeCurrent.PositionX, nodeCurrent.PositionY));
                        nodeCurrent = nodeCurrent.Parent;
                    }
                    return points;
                }

                OpenList.Remove(); 
                Point temp = new Point(node.PositionX, node.PositionY);
                if (DiagonalMovesAllowed)
                {
                    //       if (CheckPassability(temp.X - 1, temp.Y) || CheckPassability(temp.X, temp.Y - 1))
                    //           AddNode(node, -1, -1, false, endCell);
              //      if (_tileMap.InRange(temp.X + 1, temp.Y))
                        if (CheckPassability(temp.X, temp.Y - 1) && _tileMap[temp.X + 1, temp.Y].IsSolid)
                            AddNode(node, 1, -1, false, endCell);
               //     if (_tileMap.InRange(temp.X - 1, temp.Y))
                        if (CheckPassability(temp.X, temp.Y - 1) && _tileMap[temp.X - 1, temp.Y].IsSolid)
                            AddNode(node, -1, -1, false, endCell);

                    if (_tileMap.InRange(temp.X, temp.Y + 1))//temporary
                        if (CheckPassability(temp.X + 1, temp.Y) && _tileMap[temp.X, temp.Y + 1].IsSolid)
                            AddNode(node, 1, 1, false, endCell);
                    if (_tileMap.InRange(temp.X, temp.Y + 1))//temporary FIX IT!!!
                        if (CheckPassability(temp.X - 1, temp.Y) && _tileMap[temp.X, temp.Y + 1].IsSolid)
                            AddNode(node, -1, 1, false, endCell);
                    //      if (CheckPassability(temp.X + 1, temp.Y) || CheckPassability(temp.X, temp.Y + 1))
                    //        AddNode(node, 1, 1, false, endCell);

                    if (CheckPassability(temp.X, temp.Y))
                    {

                    }
                }

                AddNode(node, -1, 0, true, endCell);
                AddNode(node, 0, -1, true, endCell);
                AddNode(node, 1, 0, true, endCell);
                AddNode(node, 0, 1, true, endCell);
            }
            return null;
        }



        private bool CheckPassability(int x, int y)
        {
            //if (!IgnoreObstacles)
            //{
            //    MapObject obstacle = gameField.things.QueryObject(new Point(x, y));
            //    if (obstacle != null)
            //        if (!obstacle.Passable)
            //            return false;
            //}
            //if(FogMap!= null)
            //    if (FogMap[x, y] == 1) return false;
            if (!_tileMap.InRange(new Point(x, y))) return false;
            var tile = _tileMap[x, y];

            if (_tileSolidChecker != null)
            {
                if (!_tileSolidChecker(tile))
                {
                    return false;
                }
            }

            return !tile.IsSolid;
        }


        private bool CheckPassability(Point cell)
        {
            return CheckPassability(cell.X, cell.Y);
        }


        private void AddNode(WayPoint node, sbyte offSetX, sbyte offSetY, bool type, Point endCell)
        {
            Point pos = new Point(node.PositionX + offSetX, node.PositionY + offSetY);

            if (!CheckPassability(pos))
                return;
            if (pointMap[pos.X, pos.Y] != 1)
            {
               // byte addCost = _tileMap[pos.X, pos.Y + 1] == CellType.Free ? (byte)20 : (byte)0; 

                WayPoint temp = new WayPoint(pos, node,  type);
                temp.CalculateCost(_tileMap, endCell, 0, ImprovedPathFinding);
                OpenList.Add(temp);
                pointMap[pos.X, pos.Y] = 1;
            }
        }

        private Point VectorToPoint(Vector2 vector)
        {
            return new Point((int)vector.x, (int)vector.y);
        }

        private Vector2 PointToVector(Point point)
        {
            return new Vector2(point.X, point.Y);
        }
    }
}