﻿using GameName1.Levels;
using GameObjectExtensions;
using UnityEngine;

namespace GameName1.Fight
{
  public class SelectionCursor
  {
    private readonly GameObject _cursorGo;

    public SelectionCursor()
    {
      _cursorGo = GameData.I.TileData.AvailableTile.Create();
    }

    public void Show()
    {
      _cursorGo.gameObject.SetActive(true);
    }

    public void Hide()
    {
      _cursorGo.gameObject.SetActive(false);
    }

    public void Update(Vector3 position, Color? color = null)
    {
      var tileUnderMouse = Levels.TileHelper.GetTileUnderMouse();
      _cursorGo.transform.position = position;
      Levels.TileHelper.TileToPosition(tileUnderMouse);

      if (color != null)
      {
        _cursorGo.GetComponentInChildren<SpriteRenderer>().color = color.Value;
      }
    }
  }
}
