﻿using System.Collections.Generic;
using GameName1.Actors;
using GameName1.Lua;
using SLua;
using UnityEngine.Assertions;

namespace GameName1.Fight
{
  [CustomLuaClass]
  public class LuaAIPlayer:FightPlayer
  {
    public string AiName { get; private set; }
    private readonly AILuaLoader _ailua;
    private readonly FightManager _fightManager;

    private readonly Dictionary<CreatureActor, AILuaBehaviourWrapper> _behaviours =
      new Dictionary<CreatureActor, AILuaBehaviourWrapper>();

    public LuaAIPlayer(AILuaLoader ailua, FightManager fightManager, string aiName) 
      : base()
    {
      AiName = aiName;
      _ailua = ailua;
      _fightManager = fightManager;
    }

    protected override void OnCreaturesSet()
    {
      base.OnCreaturesSet();

      foreach (var creature in Creatures)
      {
        var behaviour = _ailua.GetBehaviour(AiName, _fightManager, this, creature);
        Assert.IsNotNull(behaviour);

        _behaviours[creature] = behaviour;
      }
    }

    protected override void InternalUpdate()
    {
      base.InternalUpdate();
      CurrentBehaviour.Update();
    }

    private AILuaBehaviourWrapper CurrentBehaviour
    {
      get { return _behaviours[SelectedCreatureActor]; }
    }
  }
}
