﻿using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Xml;
using GameName1.Actors;
using GameName1.Extensions;
using GameName1.Tiles;
using GameObjectExtensions;
using UnityEngine;

namespace GameName1.Fight
{
  public class MovementGrid
  {
    private readonly TileMap _tileMap;
    private Cell[,] _cellMap;
    private Queue<Cell> _cellsToCheck;
    private readonly List<GameObject> _movementCells;
    private readonly List<Cell> _availableCells;
    private CreatureActor _creatureActor;

    public MovementGrid(TileMap tileMap)
    {
      _tileMap = tileMap;
      _movementCells = new List<GameObject>();
      _cellsToCheck = new Queue<Cell>();
      _availableCells = new List<Cell>();
    }

    public void Show(CreatureActor creatureActor)
    {
      _creatureActor = creatureActor;

      Update();
    }

    public void HideMovementGrid()
    {
      CleanGrid();
    }

    public void Update()
    {
      CleanGrid();


      if (_creatureActor != null)
      {
        var rootCell = new Cell {Tile = _creatureActor.Tile, ActionPoints = _creatureActor.ActionPoints + 1};

        _cellsToCheck.Enqueue(rootCell);

        while (_cellsToCheck.Count > 0)
        {
          Cell cell = _cellsToCheck.Dequeue();
          CheckCellsAround(cell);
        }
      }

      foreach (var availableCell in _availableCells)
      {
        if (_creatureActor != null)
        {
          if (availableCell.Tile == _creatureActor.Tile)
          {
            continue;
          }
        }

        var go = GameData.I.TileData.AvailableTile.Create();

        var sprite = go.GetComponentInChildren<SpriteRenderer>();
        var creatureOnTile = _tileMap.GetCreatureFromTile(availableCell.Tile);

        if (creatureOnTile == null || creatureOnTile == _creatureActor)
        {
          sprite.color = new Color(30f/255f, 255/255f, 241/255f, 0.6f);
        }
        else
        {
          sprite.color = new Color(220f/255f, 0/255f, 29/255f, 0.75f);
        }
        go.transform.position = Levels.TileHelper.TileToPosition(availableCell.Tile);
        _movementCells.Add(go);
      }
    }

    private void CheckCellsAround(Cell cell)
    {
      if (!_tileMap.InRange(cell.Tile))
      {
        return;
      }

      if (_cellMap[cell.Tile.X, cell.Tile.Y] != null)
      {
        return;
      }

      if (cell.ActionPoints <= 0)
      {
        return;
      }

      if (_tileMap[cell.Tile].IsSolid)
      {
        return;
      }

      _cellMap[cell.Tile.X, cell.Tile.Y] = cell;
      _availableCells.Add(cell);

      int actionPoints = cell.ActionPoints - 1;

      AddCell(cell.Tile.Add(1, 0), actionPoints);
      AddCell(cell.Tile.Add(-1, 0), actionPoints);
      AddCell(cell.Tile.Add(0, 1), actionPoints);
      AddCell(cell.Tile.Add(0, -1), actionPoints);
    }

    private void AddCell(Point pos, int actionPoint)
    {
      Cell cell = new Cell {Tile = pos, ActionPoints = actionPoint};
      _cellsToCheck.Enqueue(cell);
    }

    private void CleanGrid()
    {
      foreach (var movementCell in _movementCells)
      {
        GameObject.Destroy(movementCell.gameObject);
      }
      _movementCells.Clear();
      _availableCells.Clear();
      _cellsToCheck.Clear();



      if (_cellMap == null ||
          _cellMap.GetLength(0) != _tileMap.Width ||
          _cellMap.GetLength(1) != _tileMap.Height)
      {
        _cellMap = new Cell[_tileMap.Width, _tileMap.Height];
      }
      else
        for (int i = 0; i < _cellMap.GetLength(0); i++)
        {
          for (int j = 0; j < _cellMap.GetLength(1); j++)
          {
            _cellMap[i, j] = null;
          }
        }
    }

    private class Cell
    {
      public Point Tile;
      public int ActionPoints;
    }
  }
}