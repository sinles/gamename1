﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameName1.Actors;
using GameName1.Data;
using SLua;

namespace GameName1.Fight
{
  [CustomLuaClass]
  public class FightInfo
  {
    public List<UnitData> Creatures { get; set; }
    public LuaObject LevelParams{ get; set; }
    public string LevelName { get; set; }

    public FightInfo()
    {
      Creatures = new List<UnitData>();
    }

    public void AddCreature(UnitData creatureInfo)
    {
      Creatures.Add(creatureInfo);
    }
  }
}
