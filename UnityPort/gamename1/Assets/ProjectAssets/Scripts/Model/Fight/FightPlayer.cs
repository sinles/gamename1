﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using GameName1.Actors;
using SLua;

namespace GameName1.Fight
{
  [CustomLuaClass]
  public abstract class FightPlayer : ISaveable
  {
    public bool SaveCreatures = true;
    public bool ActivateLight = false;
    public int Side;

    public event Action<CreatureActor> CreatureStepStarted;
    public Action StepStarted;

    public bool IsLocal { get; private set; }

    protected List<CreatureActor> Creatures;
    private int _currentCreature;

    public FightPlayer()
    {
      _currentCreature = 0;
    }

    public void MarkLocal()
    {
      IsLocal = true;
    }

    public void SetCreatures(List<CreatureActor> creatures)
    {
      Creatures = creatures;
      foreach (CreatureActor creature in Creatures)
      {
        creature.SetPlayer(this);
      }

      OnCreaturesSet();
    }

    protected virtual void OnCreaturesSet()
    {
    }

    public void ActivateCreatures(bool active)
    {
      foreach (var creature in Creatures)
      {
        creature.Active = active;
      }
    }

    public virtual void LevelChanged()
    {

    }

    public virtual void StartStep()
    {

      StepEnded = false;
      foreach (var aliveCreature in AliveCreatures)
      {
        aliveCreature.ResetActionPoints();
      }

      if (SelectedCreatureActor == null || SelectedCreatureActor.IsDead)
      {
        if (SelectedCreatureActor != null)
        {
          RemoveSelectedCreature();
        }
        if (Creatures.Count == 0)
        {
          StepEnded = true;
          return;
        }
        GetNextCreature();
      }
      else
      {
        SelectedCreatureActor.OnStepStarted();
        StartCreatureStep(SelectedCreatureActor);
      }
    }


    protected virtual void StartCreatureStep(CreatureActor creature)
    {
      if(CreatureStepStarted != null)
        CreatureStepStarted(creature);
    }

    protected virtual void OnStepEnded()
    {

    }

    public void Update()
    {
      CheckForNextCreature();
      CleanUpDeadCreatures();

      if (SelectedCreatureActor != null)
      {
        if (!SelectedCreatureActor.ActionRunning)
        {
          InternalUpdate();
        }
      }
    }

    protected virtual void InternalUpdate()
    {

    }



    public virtual void Deinit()
    {
    }

    private void CheckForNextCreature()
    {
      if (SelectedCreatureActor != null)
      {
        if ((!SelectedCreatureActor.HasActionPoints() && !SelectedCreatureActor.ActionRunning) 
          || SelectedCreatureActor.IsDead)
        {
          GetNextCreature();
        }
        return;
      }

      GetNextCreature();
    }

    public void SelectCreature(CreatureActor creatureActor)
    {
      if (creatureActor.ActionPoints != 0)
      {
        _currentCreature = AliveCreatures.ToList().IndexOf(creatureActor);
        CheckForNextCreature();
      }
    }

    private void GetNextCreature()
    {
      do
      {
        _currentCreature++;
        if (SelectedCreatureActor != null)
        {
          StartCreatureStep(SelectedCreatureActor);
        }
        if (_currentCreature >= AliveCreatures.Count())
        {
          _currentCreature = 0;
        }
        if (AllCreatuersHaveNoPoints())
        {
          _currentCreature = 0;
          StepEnded = true;
          break;
        }
        if (SelectedCreatureActor != null
            && SelectedCreatureActor.IsDead)
        {
          RemoveSelectedCreature();
        }
      } while (SelectedCreatureActor != null && SelectedCreatureActor.IsDead && SelectedCreatureActor.ActionPoints == 0);
    }

    private bool AllCreatuersHaveNoPoints()
    {
      return AliveCreatures.All(p => p.ActionPoints == 0);
    }

    private void RemoveSelectedCreature()
    {
      SelectedCreatureActor.Destroy();
      Creatures.Remove(SelectedCreatureActor);
    }

    public void Save(XElement node)
    {
      //XmlHelper.WriteSimple(node, "CurrentCreature", _currentCreature);
      //XmlHelper.WriteSimple(node, "Fight", Fight);
      //XmlHelper.WriteClass(node, GetType());
      //if (SaveCreatures)
      //{
      //  var root = XmlHelper.CreateChild(node, "Creatures");
      //  foreach (var creature in Creatures)
      //  {
      //    var creatureNode = XmlHelper.CreateChild(root, "Creature");
      //    creature.Save(creatureNode);
      //  }
      //}
    }

    public void Load(XElement node)
    {
      //Creatures = new List<CreatureActor>();

      //_currentCreature = XmlHelper.ReadInt(node, "CurrentCreature");
      //Fight = XmlHelper.ReadBool(node, "Fight");

      //var root = XmlHelper.GetChild(node, "Creatures");
      //if (root != null)
      //{
      //  foreach (var creatureNode in XmlHelper.GetChildren(root, "Creature"))
      //  {
      //    var creature = XmlHelper.ReadClass<CreatureActor>(creatureNode);
      //    creature.Load(creatureNode);
      //    creature.SetPlayer(this);
      //    Creatures.Add(creature);
      //  }
      //}
    }

    private void CleanUpDeadCreatures()
    {
      foreach (var creatureActor in Creatures)
      {
        if (creatureActor.IsDead)
        {
          creatureActor.Destroy();
        }
      }

      Creatures.RemoveAll(p => p.IsDead);
    }

    private bool _stepEnded;

    public bool StepEnded
    {
      get { return _stepEnded; }
      set
      {
        if (!_stepEnded && value)
        {
          OnStepEnded();
        }

        _stepEnded = value;
      }
    }

    public CreatureActor SelectedCreatureActor
    {
      get
      {
        if (_currentCreature >= Creatures.Count)
        {
          return null;
        }

        return Creatures[_currentCreature];
      }
    }

    public List<CreatureActor> AliveCreatures
    {
      get { return Creatures.Where(p => !p.IsDead).ToList(); }
    }

    public bool HasCreatures
    {
      get { return Creatures.Any(p => !p.IsDead); }
    }


    public CreatureActor MainCreatureActor
    {
      get { return Creatures.FirstOrDefault(); }
    }

    public bool SkipFrameOnStepStart { get; set; }
  }
}
