﻿using GameName1.Levels;
using Glide;
using SLua;
using UnityEngine;

namespace GameName1.Actors
{
  [CustomLuaClass]
  public class ProjectileActor : Actor
  {
    public string View { get; private set; }
    public float FlightDuration { get; private set; }

    private readonly Point _targetTile;

    public ProjectileActor(Point targetTile, float speed, string view)
    {
      View = view;
      _targetTile = targetTile;

      var targetPosition = TileHelper.TileToPosition(targetTile);
      var distance = Vector2.Distance(Position, targetPosition);

      FlightDuration = distance / speed;
    }

    public override void Created()
    {
      base.Created();

      Tweener.I.Tween(this,
        new { Position = TileHelper.TileToPosition(TargetTile) }, 0.15f)
        .OnComplete(Destroy);
    }

    public Point TargetTile
    {
      get { return _targetTile; }
    }
  }
}
