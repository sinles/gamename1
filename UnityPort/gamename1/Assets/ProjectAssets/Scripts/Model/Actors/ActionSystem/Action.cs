﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using UnityEngine;

namespace GameName1.Actors.ActionSystem
{
    public abstract class Action
    {
        private readonly CreatureActor _creatureActor;


        public Action(CreatureActor creatureActor)
        {
            _creatureActor = creatureActor;
        }

        public bool Start()
        {

            bool started = DoStart();
            if (!started)
            {
                End();
            }

            return started;
        }

        protected virtual bool DoStart()
        {
            return true;
        }

        public virtual void Update()
        {

        }

        public void End()
        {
            IsEnded = true;
        }

        public bool IsEnded { get; private set; }
        protected CreatureActor CreatureActor 
        {
            get
            {
                return _creatureActor;
            }
        }
    }
}
