﻿using System.Collections.Generic;
using System.Linq;
using GameName1.PathFinding;
using GameName1.Tiles;
using Glide;
using UnityEngine;

namespace GameName1.Actors.ActionSystem
{
  public class MoveAction : Action
  {
    private readonly AStar _aStar;

    private List<Point> _path;

    private readonly TileMap _tileMap;
    private readonly Point _targetTile;
    private readonly int _stopDistance;
    private Point _currentTile;
    private Vector2 _startPosition;
    private Vector2 _endPosition;


    public MoveAction(TileMap tileMap, CreatureActor creatureActor, Point targetTile, int stopDistance)
      : base(creatureActor)
    {
      _tileMap = tileMap;
      _targetTile = targetTile;
      _stopDistance = stopDistance;
      _aStar = new AStar(tileMap, TileSolidChecker);
    }



    protected override bool DoStart()
    {
      base.DoStart();

      if (_tileMap.IsFree(_targetTile))
      {
        return TryMoveToTile(_targetTile);
      }


      return false;
    }

    private bool TryMoveToTile(Point newTile)
    {
      if (!_tileMap.InRange(newTile))
      {
        return false;
      }

      _path = _aStar.FindCellWay(CreatureActor.Tile, newTile);

      if (_path == null)
      {
        return false;
      }

      if (_path.Count > _stopDistance)
        _path.RemoveRange(_path.Count - _stopDistance, _stopDistance);
      else
      {
        _path.Clear();
      }

      var creatureOnTile = _tileMap.GetCreatureFromTile(newTile);
      if (_path.Count > 1)
      {
        ContinueMovement();
        return true;
      }

      return false;
    }


    private bool InAttackRange(Point tile)
    {
      int range = CreatureActor.Info.Data.AttackRange;

      float distance = CreatureActor.Tile.Distance(tile);
      if (distance <= range)
      {
        return true;
      }

      return false;
    }


    private bool MoveByTiles(Point newTile)
    {
      _currentTile = newTile;

      var creatureOnTile = _tileMap.GetCreatureFromTile(_currentTile);

      _startPosition = CreatureActor.Position;
      _endPosition = Levels.TileHelper.TileToPosition(_currentTile);

      if (creatureOnTile != null)
      {
        Attack(creatureOnTile);
        return true;
      }


      Move();
      return false;
    }

    private void Move()
    {
      if (CreatureActor.HasActionPoints())
      {
        CreatureActor.UseActionPoint();

        Tweener.I.Tween(CreatureActor, new {X = _endPosition.x, Y = _endPosition.y}, 0.2f)
          .Ease(Ease.BackOut)
          .OnComplete(
            OnMoveCompleted);
      }
      else
      {
        End();
      }
    }

    private void OnMoveCompleted()
    {
      CreatureActor.SetTile(_currentTile, false);
      ContinueMovement();
    }

    private void ContinueMovement()
    {
      _path.RemoveAt(0);
      if (_path.Count > 0)
      {
        var currentTile = _path.First();
        _startPosition = CreatureActor.Position;
        var creatureOnTile = _tileMap.GetCreatureFromTile(_targetTile);
        if (creatureOnTile != null && InAttackRange(_targetTile))
        {
          _endPosition = creatureOnTile.Position;
          Attack(creatureOnTile);
        }
        else
        {
          MoveByTiles(currentTile);
        }
      }
      else
      {
        End();
      }
    }

    private void Attack(CreatureActor creatureActorOnTile)
    {
      Vector2 endPosition = new Vector2();

      endPosition = Vector2.Lerp(CreatureActor.Position, _endPosition, 0.45f);

      CreatureActor.UseAllActionPoints();

      Tweener.I.Tween(CreatureActor, new {X = endPosition.x, Y = endPosition.y}, 0.15f)
        .Ease(Ease.BackOut)
        .OnComplete(() =>
        {
          ProcessAttack(creatureActorOnTile);
          Tweener.I.Tween(CreatureActor, new {X = _startPosition.x, Y = _startPosition.y}, 0.15f)
            .Ease(Ease.BackOut)
            .OnComplete(() =>
            {
              End();
            });
        });
    }

    protected virtual void ProcessAttack(CreatureActor creatureActorOnTile)
    {
      CreatureActor.DoDamage(creatureActorOnTile);
    }

    private bool TileSolidChecker(Tile t)
    {
      bool hasOtherCreature =
        !t.HasAnyTileObjects<CreatureActor>(
          t2 =>
            (t2 != CreatureActor
             && t2.FightPlayer == CreatureActor.FightPlayer)
            && t2.Side == CreatureActor.Side);
      return hasOtherCreature;
    }

    public override void Update()
    {
      base.Update();
    }
  }
}