﻿using GameName1.Lua;
using SLua;

namespace GameName1.Actors.ActionSystem
{
  public class UseSkillAction : Action
  {
    private readonly SkillLuaWrapper _skill;
    private readonly LuaTable _luaParams;

    public UseSkillAction(SkillLuaWrapper skill, CreatureActor creatureActor, LuaTable luaParams)
      : base(creatureActor)
    {
      _skill = skill;
      _luaParams = luaParams;
    }

    protected override bool DoStart()
    {
      if (!CreatureActor.HasActionPoints())
        return false;

      if (_skill.Apply(_luaParams))
      {
        CreatureActor.UseAllActionPoints();
      }
      else
      {
        return false;
      }

      if (_skill.IsDone())
      {
        End();
      }

      return true;
    }

    public override void Update()
    {
      base.Update();

      _skill.Update();

      if (_skill.IsDone())
      {
        End();
        _skill.OnDone();
      }
    }
  }
}
