﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GameName1.Actors
{
  public class ActorManager
  {
    public static ActorManager I;

    public event Action<Actor> ActorAdded;
    public event Action<Actor> ActorRemoved;


    private readonly List<Actor> _actors;

    private bool _updating;

    public ActorManager()
    {
      _actors = new List<Actor>(150);

      I = this;
    }

    public Actor GetById(int id)
    {
      return _actors.Find(p => p.Id == id);
    }

    public T GetById<T>(int id) where T:Actor
    {
      return _actors.Find(p => p.Id == id) as T;
    }

    public void Add(Actor actor)
    {
      if (actor == null) throw new ArgumentNullException("actor");
      _actors.Add(actor);

      OnActorAdded(actor);
    }

    private void OnActorAdded(Actor actor)
    {
      actor.Created();
      if (ActorAdded != null)
        ActorAdded(actor);
    }

    public void Remove(Actor actor)
    {
      if (actor == null) throw new ArgumentNullException("actor");
      _actors.Remove(actor);

      OnActorRemoved(actor);
    }

    private void OnActorRemoved(Actor actor)
    {
      actor.Destroyed();
      if (ActorRemoved != null)
        ActorRemoved(actor);
    }

    public void Update()
    {
      _updating = true;

      for (int i = 0; i < _actors.Count; i++)
      {
        var actor = _actors[i];
        if (actor.Active)
        {
          actor.Update();
        }

        if (actor.IsDestroyed)
        {
          _actors.RemoveAt(i--);
          OnActorRemoved(actor);
        }

        var addedActors = actor.PurgeAddedActors();
        if (addedActors != null)
        {
          foreach (var addedActor in addedActors)
          {
            _actors.Add(addedActor);
            OnActorAdded(addedActor);
          }
        }
      }

      _updating = false;
    }

    public void Clear(params Actor[] ignoreList)
    {
      foreach (var actor in _actors.ToList().Where(gameObject => ignoreList.All(p => p != gameObject)))
      {
        Remove(actor);
      }
    }

    public List<Actor> Actors
    {
      get { return _actors.ToList(); }
    }
  }
}
