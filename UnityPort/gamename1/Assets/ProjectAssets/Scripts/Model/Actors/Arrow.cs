﻿using System;
using GameObjectExtensions;
using UnityEngine;

namespace GameName1.Actors
{
    public class Arrow:Actor
    {
        private const float Speed = 4f;
        private const float Amount = 0.25f;
        private const float Offset = Levels.TileHelper.TileSize/2 + 12;


        private float _elapsed;
        private float _offset;


        public Arrow()
            :base()
        {
            Saveable = false;
        }

        public override void Update()
        {
            base.Update();

            _offset += (float)Math.Cos(_elapsed * Speed) * Amount;
            _elapsed += Time.deltaTime;
        }
    }
}
