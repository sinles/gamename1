﻿using System;
using GameName1.Fight;
using GameName1.Levels;
using GameName1.Tiles;

namespace GameName1.Actors
{
  public class World
  {
    public TileMap TileMap
    {
      get { return Level.TileMap; }
    }

    public ActorManager ActorManager { get; private set; }

    public Level Level{ get; private set; }

    public World()
    {
      ActorManager = new ActorManager();
      ActorManager.ActorRemoved += ActorManagerOnActorRemoved;
    }

    public void AddTileActor(TileActor actor)
    {
      TileMap.AddActor(actor);
      AddActor(actor);
    }

    private void AddActor(Actor actor)
    {
      ActorManager.Add(actor);
    }

    public void Save(FightManager fightManager = null)
    {
      GameSave save = new GameSave();
      save.Save(Level, ActorManager.Actors, null, fightManager == null ? null : fightManager.Players);
    }


    private void ActorManagerOnActorRemoved(Actor actor)
    {
    }

    public void Load()
    {
      GameSave save = new GameSave();
      save.Load();
    }

    public void Update()
    {
      ActorManager.Update();
    }

    public void GoTo(Level level)
    {
      Level = level;
      Level.Create();
      Level.CreateTileMap();
    }
  }
}
