﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameName1.Actors
{
    interface IUseable
    {
        bool CanUse(CreatureActor creatureActor);

        void Use(CreatureActor creatureActor);
    }
}
