﻿using GameName1.Actors.ActionSystem;
using GameName1.Data;
using GameName1.Fight;
using GameName1.Items;
using GameName1.Lua;
using GameName1.Skills;
using GameName1.StatusEffects;
using GameName1.Tiles;
using SLua;
using UnityEngine;
using UnityEngine.Assertions;

namespace GameName1.Actors
{
  [CustomLuaClass]
  public class CreatureInfo
  {
    public readonly Inventory Inventory;
    public readonly CreatureEquipment Equipment;

    public CreatureInfo()
    {
      Inventory = new Inventory();
      Equipment = new CreatureEquipment();

      Data = new UnitData();
    }

    public CreatureInfo(UnitData data)
      : this()
    {
      Data = data;
    }

    public UnitData Data = null;
  }

  [CustomLuaClass]
  public class CreatureActor : TileActor
  {
    public int Side { get; set; }

    protected readonly ActionController ActionController;
    public readonly StatusEffectsManager StatusEffects;

    public event System.Action<float> HealthChanged;

    private readonly CreatureInfo _creatureInfo;
    public readonly Abilities Abilities;

    private float _health;

    public CreatureActor(TileMap tileMap, CreatureInfo info, int side)
      : base(tileMap)
    {
      Assert.IsNotNull(info, "info");
      Assert.IsNotNull(tileMap, "tileMap");

      _creatureInfo = info;

      ActionController = new ActionController(this);
      StatusEffects = new StatusEffectsManager();

      Health = info.Data.MaxHealth;
      Stamina = info.Data.MaxStamina;
      StaminaRestorage = _creatureInfo.Data.StaminaRestoring;
      Health = _creatureInfo.Data.MaxHealth;
      Side = side;

      Abilities = new Abilities();

      ResetActionPoints();

      Assert.AreNotEqual(_creatureInfo, null);
    }


    public CreatureActor(TileMap tileMap, UnitData unitData)
      : this(tileMap, new CreatureInfo(unitData), 3)
    {
    }

    public void InitSkills(SkillLuaLoader skillLuaLoader)
    {
      Abilities.InitSkills(skillLuaLoader, TileMap, this, _creatureInfo.Data.SkillIds);
    }

    public virtual void OnStepStarted()
    {
      StatusEffects.OnStepStarted();

      Stamina += StaminaRestorage;
      Stamina = Mathf.Min(Stamina, Info.Data.MaxStamina);
    }

    public virtual void SetPlayer(FightPlayer fightPlayer)
    {
      FightPlayer = fightPlayer;
    }

    public void UseAllActionPoints()
    {
      ActionPoints = 0;
    }

    public void UseActionPoint()
    {
      if(ActionPoints > 0)
        ActionPoints -= 1;
    }

    public void ResetActionPoints()
    {
      ActionPoints = Info.Data.MaxActionPoints;
    }

    public void PickupItem(Item item)
    {
      _creatureInfo.Inventory.AddItem(item);
    }

    public void Skip()
    {
      if (!ActionController.ActionRunning)
      {
        ActionPoints = 0;
      }
    }

    public bool Use()
    {
      return ActionController.Do(new UseAction(TileMap, this));
    }

    public bool Move(Point target)
    {
      return Move(target.X, target.Y);
    }

    public bool Move(int x, int y, int stopDistance = 0)
    {
      var targetTile = new Point(x, y);
      var moveAction = new MoveAction(TileMap, this, targetTile, stopDistance);
      return ActionController.Do(moveAction);
    }

    public bool UseSkill(string skillId, LuaTable param)
    {
      ResetSkill();

      var skillWrapper = Abilities.GetSkillWrapperById(skillId);
      var skillAction = new UseSkillAction(skillWrapper, this, param);
      return ActionController.Do(skillAction);
    }

    public void RecieveDamage(float damage)
    {
      float armor = GetProtection();
      damage = damage - ((damage/100)*armor);
      Health -= damage;

      if (IsDead)
      {
        Die();
      }
    }

    public float GetProtection()
    {
      return _creatureInfo.Equipment.GetArmorBonus();
    }

    public void Die()
    {
      Destroy();
    }

    public override void Update()
    {
      ActionController.Update();
      base.Update();
    }

    public void DoDamage(CreatureActor other)
    {
      float damage = GetAttackDamage();
      other.RecieveDamage(damage);

    }

    public float GetAttackDamage()
    {
      float critMult = 1f;

      if (RandomTool.D.NextBool(Info.Data.CritChance))
      {
        critMult = 2f;
      }

      return Info.Data.Damage*critMult; //(1.0f + _creatureInfo.Equipment.GetDamageBonus())*skillBonus;
    }

    public bool UseStamina(float staminaUsage)
    {
      if (staminaUsage <= Stamina)
      {

        Stamina -= staminaUsage;
        return true;
      }

      return false;
    }

    public int ActionPoints { get; private set; }

    public bool HasActionPoints()
    {
      return ActionPoints > 0;
    }

    public float Health
    {
      get
      {
        return _health;
      }
      private set
      {
        // ReSharper disable once CompareOfFloatsByEqualityOperator
        if (_health == value) return;

        var change = value - _health;
        _health = value;

        if (HealthChanged != null)
        {
          HealthChanged(change);
        }
      }
    }

    public float Stamina { get; set; }

    public float StaminaRestorage { get; set; }

    public virtual bool IsDead
    {
      get { return Health <= 0; }
    }

    public FightPlayer FightPlayer { get; private set; }

    public bool ActionRunning
    {
      get { return ActionController.ActionRunning; }
    }

    public CreatureInfo Info
    {
      get { return _creatureInfo; }
    }

    public void SelectSkill(string skillId)
    {
      Abilities.SelectSkill(skillId);
    }

    public void ResetSkill()
    {
      Abilities.ResetSelectedSkill();
    }
  }
}
