﻿using GameName1.Actors;
using GameName1.StatusEffects;

public class StatusEffectFabric
{
    private readonly CreatureActor _creature;

    public StatusEffectFabric(CreatureActor creature)
    {
        _creature = creature;
    }

    public StatusEffect CreateEffect(string effectId)
    {
        StatusEffect effect = null;
        switch (effectId)
        {
            case "Bleed":
                effect = new BleedStatusEffect(_creature, _creature.GetAttackDamage());
                effect.Duration = 3;
                break;
        }

        return effect;
    }
}
