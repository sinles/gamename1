﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using GameName1.Actors;

namespace GameName1.StatusEffects
{
    public abstract class StatusEffect:ISaveable
    {

        public StatusEffect(CreatureActor creature)
        {
            Creature = creature;
        }


        public void OnStepStarted()
        {
            ProcessOnStepStarted();
            Elapsed += 1;

            if (EffectEnded)
            {
                OnEffectEnded();
            }
        }

        protected virtual void ProcessOnStepStarted()
        {
            
        }

        protected virtual void OnEffectEnded()
        {
            
        }

        public int Elapsed { get; private set; }
        public int Duration { get; set; }
        public CreatureActor Creature { get; private set; }

        public bool EffectEnded 
        {
            get { return Elapsed > Duration; }
        }

        public void Save(XElement node)
        {
            XmlHelper.WriteSimple(node, "Duration", Duration);
            XmlHelper.WriteSimple(node, "Elapsed", Elapsed);
            XmlHelper.WriteClass(node, GetType());
        }

        public void Load(XElement node)
        {
            throw new NotImplementedException();
        }
    }
}
