﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameName1.Actors;
using UnityEngine;

namespace GameName1.StatusEffects
{
    public class BleedStatusEffect:StatusEffect
    {
        private readonly float _damage;

        public BleedStatusEffect(CreatureActor creature, float damage) 
            : base(creature)
        {
            _damage = damage;
        }

        protected override void ProcessOnStepStarted()
        {
            base.ProcessOnStepStarted();
            Creature.RecieveDamage(_damage);
        }
    }
}
