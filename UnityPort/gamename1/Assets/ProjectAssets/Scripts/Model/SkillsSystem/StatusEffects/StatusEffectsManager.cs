﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using UnityEngine;

namespace GameName1.StatusEffects
{
    public class StatusEffectsManager:ISaveable
    {
        private readonly List<StatusEffect> _effects;

        public StatusEffectsManager()
        {
            _effects = new List<StatusEffect>();
        }

        public void OnStepStarted()
        {
            foreach (var effect in _effects)
            {
                effect.OnStepStarted();
            }

            _effects.RemoveAll(p => p.EffectEnded);
        }

        public void Add(StatusEffect statusEffect)
        {
            _effects.Add(statusEffect);
        }

        public void Save(XElement node)
        {
            var root = XmlHelper.CreateChild(node, "StatusEffects");
            foreach (var statusEffect in _effects)
            {
                var statusNode = XmlHelper.CreateChild(root, "StatusEffect");
                statusEffect.Save(statusNode);
            }
        }

        public void Load(XElement node)
        {
            var root = XmlHelper.GetChild(node, "StatusEffects");
            foreach (var statusNode in XmlHelper.GetChildren(root, "StatusEffect"))
            {
                var status = XmlHelper.ReadClass<StatusEffect>(statusNode);
                status.Load(statusNode);
                _effects.Add(status);
            }
        }
    }
}
