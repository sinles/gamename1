﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using GameName1.Actors;
using GameName1.Data;
using GameName1.Lua;
using GameName1.Tiles;
using SLua;
using UnityEngine.Assertions;

namespace GameName1.Skills
{
  [CustomLuaClass]
  public class Abilities : ISaveable
  {
    private readonly List<SkillData> _loadedSkills = new List<SkillData>();

    private int _availableSkillSlots = 2;

    private SkillLuaLoader _loader;
    private TileMap _tileMap;
    private CreatureActor _creature;
    public SkillLuaWrapper SelectedSkill { get; private set; }
    public string SelectedSkillId
    {
      get
      {
        if (SelectedSkill == null)
        {
          return null;
        }

        return SelectedSkill.Name;
      }
    }

    public void InitSkills(SkillLuaLoader loader, TileMap tileMap, CreatureActor creature, List<string> skills)
    {
      _loader = loader;
      _tileMap = tileMap;
      _creature = creature;

      if (skills != null)
      {
        foreach (var skill in skills)
        {
          InitSkillFromId(skill);
        }
      }
    }

    private void InitSkillFromId(string id)
    {
      var skillData = new SkillData();
      skillData.SkillId = id;
      skillData.SkillWrapper = _loader.GetSkill(id, _tileMap, _creature);

      if (GetSkillWrapperById(id) != null)
      {
        Assert.IsTrue(false);
      }

      _loadedSkills.Add(skillData);
    }

    public SkillLuaWrapper GetSkillWrapperById(string id)
    {
      if (_loadedSkills == null)
      {
        return null;
      }

      var result = _loadedSkills.Find(s => s.SkillId == id);

      if (result == null)
      {
        return null;
      }

      return result.SkillWrapper;
    }


    public List<SkillData> GetAvailableSkills()
    {
      return _loadedSkills.ToList();
    }

    public int GetSkillSlotsCount()
    {
      return _availableSkillSlots;
    }

    public void Save(XElement node)
    {
    }

    public void Load(XElement node)
    {
    }

    public void SelectSkill(string skillId)
    {
      SelectedSkill = GetSkillWrapperById(skillId);
    }

    public void ResetSelectedSkill()
    {
      SelectedSkill = null;
    }
  }

  public enum AbilityType
  {
    Warrior,
    Archer,

    Last
  }
}
