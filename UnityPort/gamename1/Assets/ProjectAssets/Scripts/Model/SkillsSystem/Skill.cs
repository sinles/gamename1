﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameName1.Actors;
using GameName1.Data;
using GameName1.Extensions;
using GameName1.StatusEffects;
using UnityEngine;
using UnityEngine.Assertions;

namespace GameName1.Skills
{
  public class Skill
  {
    public Skill(SkillData skillInfo)
    {
      Assert.AreNotEqual(skillInfo, null);
    }


    /// <param name="creature">Creature who executes the skill</param>
    /// <param name="target">tile being targeted</param>
    public bool Execute(CreatureActor creature, Point? target)
    {
      return false;
    }
  }
}
