﻿using System.Collections.Generic;
using SLua;

namespace GameName1.Data
{
  [CustomLuaClass]
  public class EventChoice
  {
    public string Description { get; set; }

    //chance
    public Dictionary<EventOutcome, float> Outcomes { get; set; }

    public EventChoice()
    {
      Outcomes = new Dictionary<EventOutcome, float>();
    }

    public void AddOutcome(EventOutcome outcome, float chance = 1f)
    {
      Outcomes.Add(outcome, chance);
    }
  }
}
