﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SimpleJSON;

namespace GameName1.Data
{
    public static class Weapons
    {
        public static void Init()
        {
            string path = FileHelper.GetPathToGameData("Items/Weapons");
            string[] paths = FileHelper.GetFilePathsInFolder(path);

            LoadWeapons(paths);
        }

        private static void LoadWeapons(string[] paths)
        {
            _weapons = new List<WeaponData>();

            foreach (var path in paths)
            {
                JSONNode jsonNode = FileHelper.ParseJson(path);
                WeaponData weapon = new WeaponData(jsonNode);
                _weapons.Add(weapon);
            }
        }

        private static List<WeaponData> _weapons;
    }
}
