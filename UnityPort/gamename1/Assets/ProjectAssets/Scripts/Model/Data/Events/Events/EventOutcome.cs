﻿using System.Collections.Generic;
using GameName1.Actors;
using GameName1.Fight;
using SLua;

namespace GameName1.Data
{
  [CustomLuaClass]
  public class EventOutcomeReward
  {
    public int Gold;
    public int Exp;
    public List<string> Item;
    public List<UnitData> Units;
  }

  [CustomLuaClass]
  public class EventOutcome
  {
    public string EventId { get; set; }
    public string Description { get; set; }
    public float Chance { get; set; }

    public bool IsFight { get { return Fight != null; } }
    public EventOutcomeReward Reward { get; set; }
    public FightInfo Fight;

    public EventOutcome()
    {
      Reward = new EventOutcomeReward();
    }

    public void SetFight()
    {
      Fight = new FightInfo();
    }
  }
}
