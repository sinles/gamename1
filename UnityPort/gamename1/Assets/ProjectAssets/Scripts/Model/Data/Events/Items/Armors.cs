﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SimpleJSON;

namespace GameName1.Data
{
    public static class Armors
    {
        public static void Init()
        {
            string path = FileHelper.GetPathToGameData("Items/Armors");
            string[] paths = FileHelper.GetFilePathsInFolder(path);

            LoadWeapons(paths);
        }

        private static void LoadWeapons(string[] paths)
        {
            _armors = new List<ArmorData>();

            foreach (var path in paths)
            {
                JSONNode jsonNode = FileHelper.ParseJson(path);
                ArmorData armor = new ArmorData(jsonNode);
                _armors.Add(armor);
            }
        }

        private static List<ArmorData> _armors;
    }
}
