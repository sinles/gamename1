﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SimpleJSON;

namespace GameName1.Data
{
    public class WeaponData
    {
        public WeaponData(JSONNode node)
        {
            Id = node["Id"];
            Damage = node["Damage"].AsFloat;
        }

        public string Id { get; private set; }
        public float Damage { get; private set; }
    }
}
