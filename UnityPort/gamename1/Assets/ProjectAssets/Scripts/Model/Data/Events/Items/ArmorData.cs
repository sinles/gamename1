﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SimpleJSON;

namespace GameName1.Data
{
    public class ArmorData
    {
        public ArmorData(JSONNode node)
        {
            Id = node["Id"];
            Protection = node["Protection"].AsFloat;
        }

        public string Id { get; private set; }
        public float Protection { get; private set; }
    }
}
