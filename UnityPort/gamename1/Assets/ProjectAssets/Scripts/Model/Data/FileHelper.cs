﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using SimpleJSON;
using UnityEngine;
using UnityEngine.Rendering;

namespace GameName1.Data
{
    public static class FileHelper
    {
        public static string GetPathToGameData(string dataType)
        {
            return Path.Combine(Application.streamingAssetsPath, Path.Combine("GameData",dataType));
        }

        public static string[] GetFilePathsInFolder(string path)
        {
            return Directory.GetFiles(path, "*.json", SearchOption.AllDirectories);
        }

        public static JSONNode ParseJson(string path)
        {
            string jsonText = File.ReadAllText(path);
            try
            {
                return JSON.Parse(jsonText);
            }
            catch (Exception e)
            {
                Debug.LogError("COULD NOT READ JSON " + path + " EXCEPTION " + e.Message);
                return null;
            }
        }
    }
}
