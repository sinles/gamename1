﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameName1.Tiles;
using SimpleJSON;
using UnityEngine;

namespace GameName1.Data
{
    public class TileMapData
    {
        private string[] _map;
        private string _id;


        public TileMapData(JSONNode node)
        {
            _id = node["id"];

            var mapJson = node["map"].AsArray;
            _map = new string[mapJson.Count];

            for (int i = 0; i < mapJson.Count; i++)
            {
                _map[i] = mapJson[i];
            }
        }

        public TileMap CreateTileMap()
        {
            if (_map == null)
            {
                Debug.LogError("not loaded map " + _id);
                return null;
            }
            if (_map.Length == 0)
            {
                Debug.LogError("length 0 " + _id);
                return null;
            }
            if (_map[0].Length == 0)
            {
                Debug.LogError("First row length 0 " + _id);
                return null;
            }

            var tileMap = new TileMap(_map.Length, _map[0].Length);


            for (int i = 0; i < _map.Length; i++)
            {
                for (int j = 0; j < _map[i].Length; j++)
                {
                    tileMap.CreateTile(_map[i][j], i, j);
                }
            }



            return tileMap;
        }



        public string Id { get { return _id;} }
    }
}
