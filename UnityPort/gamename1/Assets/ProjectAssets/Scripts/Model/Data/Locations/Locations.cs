﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SimpleJSON;

namespace GameName1.Data
{
    public static class Locations
    {
        public static void Init()
        {
            string path = FileHelper.GetPathToGameData("Locations");
            string[] locationPaths = FileHelper.GetFilePathsInFolder(path);

            _locations = new List<Location>();
            foreach (var locationPath in locationPaths)
            {
                JSONNode jsonNode = FileHelper.ParseJson(locationPath);
                Location location = new Location(jsonNode);
                _locations.Add(location);
            }
        }




        public static Location Get(int index)
        {
            return _locations[index];
        }

        public static Location Get(string id)
        {
            return _locations.Find(p => p.Id == id);
        }

        private static List<Location> _locations;
    }
}
