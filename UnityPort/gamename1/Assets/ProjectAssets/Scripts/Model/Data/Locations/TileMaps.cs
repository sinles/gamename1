﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SimpleJSON;

namespace GameName1.Data
{
    public static class TileMaps
    {
        public static void Init()
        {
            string path = FileHelper.GetPathToGameData("Locations/TileMaps/");
            string[] tileMapsPaths = FileHelper.GetFilePathsInFolder(path);

            _tileMaps = new List<TileMapData>();
            foreach (var tileMapPath in tileMapsPaths)
            {
                JSONNode jsonNode = FileHelper.ParseJson(tileMapPath);
                TileMapData tileMapData = new TileMapData(jsonNode);
                _tileMaps.Add(tileMapData);
            }
        }


        public static TileMapData Get(string id)
        {
            return _tileMaps.Find(p => p.Id == id);
        }

        private static List<TileMapData> _tileMaps;
    }
}
