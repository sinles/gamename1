﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SimpleJSON;

namespace GameName1.Data
{
    public class Location
    {
        public Location(JSONNode node)
        {
            Id = node["Id"];

            Events = new List<LocationEventData>();

            JSONArray eventsJson = node["Events"].AsArray;

            foreach (JSONNode eventJson in eventsJson)
            {
                var locationEventData = new LocationEventData();
                locationEventData.Id = eventJson["Id"];
                locationEventData.Min = eventJson["Min"].AsInt;
                locationEventData.Max = eventJson["Max"].AsInt;
                Events.Add(locationEventData);
            }
        }


        public string Id { get; private set; }
        public List<LocationEventData> Events { get; private set; }

        public class LocationEventData
        {
            public string Id;

            public int Min;
            public int Max;
        }
    }
}
