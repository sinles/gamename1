﻿using System.Collections.Generic;
using SLua;

namespace GameName1.Data
{
  [CustomLuaClass]
  public class UnitData
  {
    public UnitData()
    {
      SkillIds = new List<string>();
    }

    public void AddSkill(string skillName)
    {
      SkillIds.Add(skillName);
    }

    public string Id { get; set; }

    public string Name { get; set; }
    public string Description { get; set; }
    public string Sprite { get; set; }

    public int MaxHealth { get; set; }
    public int MaxStamina { get; set; }
    public float StaminaRestoring { get; set; }

    public float CritChance { get; set; }
    public float Damage { get; set; }
    public int AttackRange { get; set; }

    public int MaxActionPoints { get; set; }
    public List<string> SkillIds { get; set; }
  }
}
