﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SimpleJSON;

namespace GameName1.Data
{
    public class UnitGroup
    {
        public UnitGroup(JSONNode node)
        {
            Id = node["Id"];
            var unitsNode = node["Units"].AsArray;

            Units = new List<UnitGroupUnitData>();
            foreach (JSONNode unitNode in unitsNode)
            {
                var unitGroupUnitData = new UnitGroupUnitData();
                unitGroupUnitData.UnitId = unitNode["Id"];
                unitGroupUnitData.Chance = unitNode["Chance"].AsFloat;
                Units.Add(unitGroupUnitData);
            }
        }

        public string Id { get; private set; }
        public List<UnitGroupUnitData> Units { get; private set; }

        public class UnitGroupUnitData
        {
            public string UnitId { get; set; }
            public float Chance { get; set; }
        }
    }
}
