﻿using GameName1.Lua;

namespace GameName1.Data
{
  public class SkillData
  {
    public string SkillId { get; set; }
    public SkillLuaWrapper SkillWrapper;
  }
}
