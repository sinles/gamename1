﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameName1.Common;

namespace GameName1.Travel
{
  public class PlayerProfileManager: Singleton<PlayerProfileManager>
  {
    public event Action<PlayerProfile> Added;

    private readonly List<PlayerProfile> _playerProfiles = new List<PlayerProfile>();

    public List<PlayerProfile> GetAll()
    {
      return _playerProfiles.OrderBy(p => p.Id).ToList();
     // _playerProfiles.Sort((p1, p2) => p1.Id > p2.Id ? 1 : -1);
      //return _playerProfiles.ToList();
    }

    public void Add(PlayerProfile profile)
    {
      _playerProfiles.Add(profile);

      if (Added != null)
      {
        Added(profile);
      }
    }

    public void Clear()
    {
      _playerProfiles.Clear();
    }
  }
}
