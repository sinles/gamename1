﻿using System.Collections.Generic;
using GameName1.Actors;
using GameName1.Items;

namespace GameName1.Travel
{
  public class PlayerProfile
  {
    public bool Local { get; set; }
    private readonly long _id;
    private readonly string _nickname;
    private readonly List<CreatureInfo> _creatures;

    public PlayerProfile(long id, string nickname, bool local)
    {
      Local = local;
      _id = id;
      _nickname = nickname;

      Inventory = new Inventory();
      _creatures = new List<CreatureInfo>();
    }

    public void AddCreature(CreatureInfo creature)
    {
      _creatures.Add(creature);
    }

    public List<CreatureInfo> Creatures
    {
      get { return _creatures; }
    }


    public Inventory Inventory { get; private set; }

    public long Id
    {
      get { return _id; }
    }
  }
}
