﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Xml.Linq;
using GameName1.Actors;
using GameName1.Data;
using GameName1.Fight;
using GameName1.Items;
using GameName1.LevelGeneration;
using GameName1.Lua;
using GameName1.Tiles;
using UnityEngine;
using UnityEngine.Assertions;

namespace GameName1.Levels
{
  public enum LevelType
  {
    Dungeon,
    City,
    Forest
  }

  public class Level : ISaveable
  {
    private readonly LevelLuaWrapper _luaLevel;
    private readonly string _location;
    private readonly List<TileActor> _tileObjects;
    private TileMap _tileMap;
    private LevelGeneratorFabric _levelGeneratorFabric;
    private LevelGenerator _generator;


    public Level()
    {
      _tileObjects = new List<TileActor>();
    }

    public Level(XElement root, TileMap tileMap)
      : this()
    {
      Load(root);
      _tileMap = tileMap;
    }

    public Level(LevelLuaWrapper luaLevel, string location)
      : this()
    {

      _luaLevel = luaLevel;
      _location = location;
    }

    private int[,] tempTiles = new int[,]
    {
      {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
      {0, 1, 1, 1, 0, 1, 0, 2, 2, 2},
      {0, 1, 1, 1, 0, 0, 0, 2, 0, 0},
      {0, 0, 0, 0, 1, 1, 2, 2, 2, 0},
      {0, 0, 0, 0, 1, 1, 0, 2, 0, 2},
      {0, 0, 0, 0, 1, 1, 0, 2, 0, 2},
      {0, 0, 0, 0, 0, 0, 0, 2, 2, 2}
    };

    public TileMap CreateTileMap()
    {
      _tileMap = _luaLevel.GenTileMap();
      _tileMap.Init(GetAtlas());

      return _tileMap;
    }

    public void Create()
    {
      if (_tileMap != null)
      {
        //    _tileMap.Init(GetAtlas());
      }
    }

    private string GetAtlas()
    {
      return _location;
    }

    public void Clean()
    {
      foreach (var tileObject in _tileObjects)
      {
        tileObject.Destroy();
      }
      _tileMap.Deinit();
    }

    public override string ToString()
    {
      return Name + " ";
    }

    private void AddObject(TileActor tileActor)
    {
      _tileObjects.Add(tileActor);
    }


    public void Save(XElement node)
    {
      var levelRoot = XmlHelper.CreateChild(node, "Level");

      XmlHelper.WriteSimple(levelRoot, "Name", Name);
    }

    public void Load(XElement node)
    {
      var levelRoot = XmlHelper.GetChild(node, "Level");

      Name = XmlHelper.ReadString(levelRoot, "Name");
    }

    public string Name { get; private set; }

    public TileMap TileMap
    {
      get { return _tileMap; }
    }
  }
}
