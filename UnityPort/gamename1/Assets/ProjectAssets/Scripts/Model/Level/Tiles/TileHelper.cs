﻿using SLua;
using UnityEngine;

namespace GameName1.Levels
{
  [CustomLuaClass]
  public static class TileHelper
  {
    public const float TileSize = 0.20f;

    public static Vector2 TileToPosition(Point tile)
    {
      return new Vector2(tile.X*TileSize + TileSize*0.5f, tile.Y*TileSize + TileSize*0.5f);
    }

    public static Point PositionToTile(Vector2 position)
    {
      return new Point((int) (position.x/TileSize), (int) (position.y/TileSize));
    }

    public static Point GetTileUnderMouse()
    {
      var pos = Input.mousePosition;
      var worldPos = CameraControl.I.Camera.ScreenToWorldPoint(pos);

      var selectedTile = PositionToTile(worldPos);
      return selectedTile;
    }
  }
}
