﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameName1.Tiles
{
    public class TileSpriteData
    {
        public string Generic;
        public string Middle;
        public string BottomMiddle;
        public string MiddleRight;
        public string MiddleLeft;
        public string TopMiddle;
        public string BottomLeft;
        public string BottomRight;
        public string TopRight;
        public string TopLeft;


        public void SetAll(string sprite)
        {
            Generic = sprite;
            Middle = sprite;
            BottomMiddle = sprite;
            MiddleRight = sprite;
            MiddleLeft = sprite;
            TopMiddle = sprite;
            BottomLeft = sprite;
            BottomRight = sprite;
            TopRight = sprite;
            TopLeft = sprite;
        }
    }
}
