﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameName1.Tiles
{
    public class TileRoad:Tile
    {
        protected override TileSpriteData GetSpriteData()
        {
            var spriteData = new TileSpriteData();

            spriteData.SetAll("grass");
            spriteData.Generic = "grass";
            spriteData.MiddleLeft = "road_horizontal";
            spriteData.MiddleRight = "road_horizontal";
            spriteData.TopMiddle = "road_vertical";
            spriteData.BottomMiddle = "road_vertical";
            spriteData.Middle = "crossroad";
            return spriteData;
        }

        protected override void OnMaskPostCreate(List<Mask> masks)
        {
            base.OnMaskPostCreate(masks);

            masks.Add(new Mask()
            {
                Sprite = "road_left_up",
                Array = new int[,]
                    {
                        {2, 1, 2},
                        {1, 1, 2},
                        {2, 2, 2}
                    },

            });

            masks.Add(new Mask
            {
                Sprite = "road_up_right",
                Array = new int[,]
                {
                    {2, 1, 2},
                    {2, 1, 1},
                    {2, 2, 2}
                },

            });
            masks.Add(new Mask
            {
                Sprite = "road_down_right",
                Array = new int[,]
                {
                    {2, 2, 2},
                    {2, 1, 1},
                    {2, 1, 2}
                },

            });
            masks.Add(new Mask
            {
                Sprite = "road_down_left",
                Array = new int[,]
                {
                    {2, 2, 2},
                    {1, 1, 2},
                    {2, 1, 2}
                },

            });


            masks.Add(new Mask
            {
                Sprite = "road_horizontal",
                Array = new int[,]
                {
                    {2, 2, 2},
                    {1, 1, 1},
                    {2, 2, 2}
                },

            });



            masks.Add(new Mask
            {
                Sprite = "road_vertical",
                Array = new int[,]
    {
                    {2, 1, 2},
                    {2, 1, 2},
                    {2, 1, 2}
    },

            });
        }

        public override Tile Copy()
        {
            return new TileRoad();
        }

        public override bool IsSolid {
            get { return false; }
        }
        public override int ProtectionLevel {
            get { return -1; }
        }
    }
}
