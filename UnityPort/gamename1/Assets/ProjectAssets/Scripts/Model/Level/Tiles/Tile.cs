﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameName1.Actors;
using GameName1.Data;
using GameObjectExtensions;
using Glide;
using UnityEngine;

namespace GameName1.Tiles
{
    public abstract class Tile
    {
        #region MASK

        private List<Mask> _masks;
        #endregion

        /// <summary>
        /// never acces this directly, use TileObjects property
        /// </summary>
        private List<TileActor> _tileObjects;


        protected Point Position;
        protected TileMap TileMap;

        private float _alpha;
        private bool _visible;
        private Tween _currentAnimation;

        private SpriteRenderer _sprite;

        private string _atlas;

        TileSpriteData _tileSpriteData;



        public Tile()
        {
        }

        public void Init(Point tile,TileMap tileMap, string atlas)
        {
            Position = tile;
            TileMap = tileMap;

            _tileSpriteData = GetSpriteData();
            _atlas = atlas;
            CreateMasks();
            OnMaskPostCreate(_masks);
            if (GameObject == null)
            {
                GameObject = Create();
                if (GameObject != null)
                {
                    _sprite = GameObject.GetComponentInChildren<SpriteRenderer>();
                    Alpha = 1.0f;
                }
            }
            SelectSprite();
            OnInit();
        }

        private void SelectSprite()
        {
            string sprite = _tileSpriteData.Generic;


            foreach (var mask in _masks)
            {

                if (CheckMask(Position, TileMap, mask))
                {
                    sprite = mask.Sprite;
                    break;
                }
            }

            SetSprite(sprite);
        }


        private bool CheckMask(Point tile, TileMap tileMap, Mask mask)
        {
            int[,] maskArray = RotateMatrixCounterClockwise(RotateMatrixCounterClockwise(RotateMatrixCounterClockwise(mask.Array)));

            for (int i = -1; i <= 1; i++)
            {
                for (int j = -1; j <= 1; j++)
                {
                    Point newTile = tile + new Point(i, j);
                    int maskAtPoint = maskArray[i + 1, j + 1];

                    if (tileMap.InRange(newTile))
                    {
                        Tile tileAtPoint = tileMap[newTile];

                        if (maskAtPoint == 1)
                        {
                            if (tileAtPoint.GetType() != GetType())
                            {
                                return false;
                            }
                        }
                        else if (maskAtPoint == 0)
                        {
                            if (tileAtPoint.GetType() == GetType())
                            {
                                return false;
                            }
                        }
                    }
                    else
                    {
                        if (maskAtPoint == 1)
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        static int[,] RotateMatrixCounterClockwise(int[,] oldMatrix)
        {
            int[,] newMatrix = new int[oldMatrix.GetLength(1), oldMatrix.GetLength(0)];
            int newColumn, newRow = 0;
            for (int oldColumn = oldMatrix.GetLength(1) - 1; oldColumn >= 0; oldColumn--)
            {
                newColumn = 0;
                for (int oldRow = 0; oldRow < oldMatrix.GetLength(0); oldRow++)
                {
                    newMatrix[newRow, newColumn] = oldMatrix[oldRow, oldColumn];
                    newColumn++;
                }
                newRow++;
            }
            return newMatrix;
        }

        private void CreateMasks()
        {
            _masks = new List<Mask>()
            {
                new Mask()
                {
                    Array = new int[,]
                    {
                        {2, 1, 2},
                        {1, 1, 1},
                        {2, 1, 2}
                    },
                    Sprite = _tileSpriteData.Middle
                },
                new Mask()
                {
                    Array = new int[,]
                    {
                        {2, 0, 2},
                        {0, 1, 0},
                        {2, 0, 2}
                    },
                    Sprite = _tileSpriteData.Generic
                },
                new Mask()
                {
                    Array = new int[,]
                    {
                        {2, 1, 2},
                        {1, 1, 1},
                        {2, 2, 2}
                    },
                    Sprite = _tileSpriteData.BottomMiddle
                },
                new Mask()
                {
                    Array = new int[,]
                    {
                        {2, 2, 2},
                        {1, 1, 1},
                        {2, 1, 2}
                    },
                    Sprite = _tileSpriteData.TopMiddle
                },
                new Mask()
                {
                    Array = new int[,]
                    {
                        {2, 1, 2},
                        {1, 1, 2},
                        {2, 1, 2}
                    },
                    Sprite = _tileSpriteData.MiddleRight
                },
                new Mask()
                {
                    Array = new int[,]
                    {
                        {2, 1, 2},
                        {2, 1, 1},
                        {2, 1, 2}
                    },
                    Sprite = _tileSpriteData.MiddleLeft
                },
                new Mask()
                {
                    Array = new int[,]
                    {
                        {2, 1, 1},
                        {0, 1, 1},
                        {2, 0, 2}
                    },
                    Sprite = _tileSpriteData.BottomLeft
                },
                new Mask()
                {
                    Array = new int[,]
                    {
                        {1, 1, 2},
                        {1, 1, 0},
                        {2, 0, 0}
                    },
                    Sprite = _tileSpriteData.BottomRight
                },
                new Mask()
                {
                    Array = new int[,]
                    {
                        {2, 0, 2},
                        {1, 1, 0},
                        {1, 1, 2}
                    },
                    Sprite = _tileSpriteData.TopRight
                },
                new Mask()
                {
                    Array = new int[,]
                    {
                        {2, 0, 2},
                        {0, 1, 1},
                        {2, 1, 1}
                    },
                    Sprite = _tileSpriteData.TopLeft
                }
            };

        }

        protected virtual void OnMaskPostCreate(List<Mask> masks)
        {

        }

        protected abstract TileSpriteData GetSpriteData();
    

        protected virtual void OnInit()
        {

        }

        protected void SetSprite(string name)
        {
            GameObject.GetComponentInChildren<SpriteRenderer>().sprite = Sprites.GetSprite(_atlas, name);
        }


        public void UpdatePosition(Vector3 position)
        {
            GameObject.transform.position = position;
        }

        public virtual GameObject Create()
        {
            return GameData.I.TileData.TilePrefab.Create();
        }

        public void SetVisible(bool visible)
        {
            if (_visible != visible)
            {
                _visible = visible;
                StopPreviousAnimation();
                _currentAnimation = Tweener.I.Tween(this, new {Alpha = _visible ? 1f : 0.5f}, 1f).Ease(Ease.CubeOut);
            }
        }

        private void StopPreviousAnimation()
        {
            if (_currentAnimation != null)
            {
                _currentAnimation.Cancel();
            }
        }

        public void AddTileObject(TileActor tileActor)
        {
            TileObjects.Add(tileActor);
        }

        public void RemoveTileObject(TileActor tileActor)
        {
            TileObjects.Remove(tileActor);
        }

        public bool HasTileObject(TileActor tileActor)
        {
            return TileObjects.Contains(tileActor);
        }

        public abstract Tile Copy();
        

        private List<TileActor> TileObjects
        {
            get
            {
                if (_tileObjects == null)
                {
                    _tileObjects = new List<TileActor>();
                }
                return _tileObjects;
            }
        }

        public List<TileActor> GetTileObjects()
        {
            if (_tileObjects == null)
            {
                return new List<TileActor>();
            }

            return TileObjects.ToList();
        }

        public bool HasAnyTileObjects()
        {
            if (_tileObjects == null)
            {
                return false;
            }

            return TileObjects.Count > 0;
        }

        public bool HasAnyTileObjects<T>(Predicate<T> predicate = null) where T:TileActor
        {
            if (_tileObjects == null)
            {
                return false;
            }

            for (int i = 0; i < TileObjects.Count; i++)
            {
                if (TileObjects[i].Active)
                {
                    if (TileObjects[i] is T)
                    {
                        if (predicate != null)
                        {
                            return predicate((T) TileObjects[i]);

                        }
                        return true;
                    }
                }
            }

            return false;
        }

        public bool IsVisible
        {
            get { return _visible; }
        }

        public GameObject GameObject { get; private set; }

        protected float Alpha
        {
            get { return _alpha; }
            set
            {
                _alpha = value;
                if (_sprite == null)
                {
                    return;
                }
                var c = _sprite.color;
                c.a = _alpha;
                _sprite.color = c;
            }
        }

        public abstract bool IsSolid { get; }
        public abstract int ProtectionLevel { get; }



        protected class Mask
        {
            public int[,] Array;
            public string Sprite;
        }
    }
}
