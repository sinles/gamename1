﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using GameName1.Actors;
using SLua;
using UnityEngine;
using UnityEngine.Assertions;

namespace GameName1.Tiles
{
  [CustomLuaClass]
  public class TileMap
  {
    public Rect Bounds { get; private set; }

    private Tile[,] _map;

    private Point _player;
    private HashSet<Point> _visiblePoints;
    readonly List<int> _visibleOctants = new List<int>() {1, 2, 3, 4, 5, 6, 7, 8};
    public int VisualRange;
    private Rectangle _visionBounds;
    private GameObject _root;


    private readonly Dictionary<Type, byte> _tileSaveTable;
    private readonly Dictionary<byte, Type> _tileLoadTable;

    public Tile this[int i, int j]
    {
      get { return _map[i, j]; }
      set
      {
        if (!InRange(i, j))
        {
          Debug.Log("Not in range " + i + " : " + j);
        }

        _map[i, j] = value;
      }
    }

    public Tile this[Point p]
    {
      get { return _map[p.X, p.Y]; }
      set { _map[p.X, p.Y] = value; }
    }

    public int Width;
    public int Height;

    public TileMap()
    {
      VisualRange = 6;

      _tileSaveTable = new Dictionary<Type, byte>()
      {
        {typeof (TileEmpty), 0},
        {typeof (TileWall), 1}
      };

      _tileLoadTable = new Dictionary<byte, Type>();
      foreach (var keyValue in _tileSaveTable)
      {
        _tileLoadTable[keyValue.Value] = keyValue.Key;
      }
    }

    public TileMap(int width, int height)
      : this()
    {
      Width = width;
      Height = height;
      _map = new Tile[Width, Height];

      Bounds = new Rect(0, 0, width*Levels.TileHelper.TileSize, height*Levels.TileHelper.TileSize);
    }

    public void Init(string atlas)
    {
      _root = new GameObject("Tilemap");

      for (var i = 0; i < Width; i++)
        for (var j = 0; j < Height; j++)
        {
          if (this[i, j] is TileWall)
          {
            this[i, j] = new TileForest();
          }
          if (this[i, j] is TileEmpty)
          {
            this[i, j] = new TileGrass();
          }


        }

      for (var i = 0; i < Width; i++)
      {
        for (var j = 0; j < Height; j++)
        {
          this[i, j].Init(new Point(i, j), this, atlas);
          this[i, j].UpdatePosition(new Vector3((i)*Levels.TileHelper.TileSize, (j + 1)*Levels.TileHelper.TileSize,
            0.03f));
          this[i, j].GameObject.transform.parent = _root.transform;
        }
      }
    }

    public void CreateTileRow(string tiles, int i)
    {
      for (int j = 0; j < tiles.Count(); j++)
      {
        CreateTile(tiles[j], j, i);
      }
    }

    //TODO:REmove this shit
    public void CreateTile(char tileType, int i, int j)
    {
      Tile tile = null;

      switch (tileType)
      {
        case 'g':
          {
            tile = new TileGrass();
            break;
          }
        case 'f':
          {
            tile = new TileForest();
            break;
          }
        case 'r':
          {
            tile = new TileRoad();
            break;
          }
      }

      if (tile == null)
      {
        Debug.LogError("unknown tile symbol " + tileType);
      }

      _map[i, j] = tile;
    }


    public void Deinit()
    {
      GameObject.Destroy(_root);
    }

    public void Save(Stream stream)
    {
      Assert.IsTrue(Width < 255);
      Assert.IsTrue(Height < 255);

      stream.WriteByte((byte) Width);
      stream.WriteByte((byte) Height);

      for (var i = 0; i < Width; i++)
      {
        for (var j = 0; j < Height; j++)
        {
          Type tile = this[i, j].GetType();
          byte value = _tileSaveTable[tile];

          stream.WriteByte(value);
        }
      }
    }

    public void Load(Stream stream)
    {
      Width = stream.ReadByte();
      Height = stream.ReadByte();

      _map = new Tile[Width, Height];

      for (var i = 0; i < Width; i++)
      {
        for (var j = 0; j < Height; j++)
        {
          byte value = (byte) stream.ReadByte();
          Type tileType = _tileLoadTable[value];

          var tile = Activator.CreateInstance(tileType) as Tile;

          _map[i, j] = tile;
        }
      }
    }


    public void UpdateVisibleTiles(Point heroPoint, Rectangle region)
    {
      int ifrom = Math.Max(0, region.Left);
      int ito = Math.Min(region.Right, Width);
      int jfrom = Math.Max(0, region.Top);
      int jto = Math.Min(region.Bottom, Height);


      _visionBounds = new Rectangle(ifrom, jfrom, ito - ifrom, jto - jfrom);
      _player = heroPoint;

      GetVisibleCells();

      for (int i = ifrom; i < ito; i++)
      {
        for (int j = jfrom; j < jto; j++)
        {
          bool visible = _visiblePoints.Contains(new Point(i, j));
          _map[i, j].SetVisible(visible);

          if (visible)
          {
            SetVisibleRect(new Rectangle(i - 1, j - 1, 3, 3), tile => tile.IsSolid);
          }
        }
      }

      _map[heroPoint.X, heroPoint.Y].SetVisible(true);
    }

    public void SetVisibleRect(Rectangle region, Func<Tile, bool> predicate = null)
    {
      if (predicate == null)
      {
        predicate = (t) => true;
      }

      for (int i = (int) region.X; i < region.Right; i++)
      {
        for (int j = (int) region.Y; j < region.Bottom; j++)
        {
          if (InRange(i, j))
          {
            if (predicate(_map[i, j]))
            {
              _map[i, j].SetVisible(true);
            }
          }
        }
      }
    }

    [DoNotToLua]
    public void FillWith(Tile cellType)
    {
      FillWith(_map, Width, Height, cellType);
    }

    [DoNotToLua]
    public static void FillWith(Tile[,] map, int width, int height, Tile cellType)
    {
      for (var i = 0; i < width; i++)
        for (var j = 0; j < height; j++)
          map[i, j] = cellType.Copy();
    }


    public Rect GetRect(int x, int y)
    {
      return new Rect(x*Levels.TileHelper.TileSize, y*Levels.TileHelper.TileSize, Levels.TileHelper.TileSize,
        Levels.TileHelper.TileSize);
    }

    public Rect GetRect(Point pos)
    {
      return new Rect(pos.Y*Levels.TileHelper.TileSize, pos.Y*Levels.TileHelper.TileSize, Levels.TileHelper.TileSize,
        Levels.TileHelper.TileSize);
    }

    public bool IsVisible(Point currentTile)
    {
      return this[currentTile].IsVisible;
    }

    public bool IsSolid(int x, int y)
    {
      if (!InRange(x, y))
      {
        return false;
      }

      return _map[x, y].IsSolid;
    }

    public bool IsFree(Point p)
    {
      if (!InRange(p))
      {
        return false;
      }
      return !_map[p.X, p.Y].IsSolid;
    }

    public bool IsFree(int x, int y)
    {
      if (!InRange(x, y))
      {
        return false;
      }
      return !_map[x, y].IsSolid;
    }

    public Point GetBottomFreeCell()
    {
      for (int i = Height - 1; i >= 0; i--)
        for (int j = 1; j < Width; j++)
          if (_map[j, i] == null)
            return new Point(j, i);
      return Point.Zero;
    }

    public Point GetTopFreeCell()
    {
      for (int i = 1; i < Height; i++)
        for (int j = Width - 1; j >= 0; j--)
          if (_map[j, i] is TileEmpty)
            return new Point(j, i);
      return Point.Zero;
    }

    public Point GetLeftCell(int y, Type cellType)
    {
      for (int i = 0; i < Width; i++)
        if (_map[i, y].GetType() == cellType)
          return new Point(i, y);
      return Point.Zero;
    }

    public Point GetRightCell(int y, Type cellType)
    {
      for (int i = Width - 1; i >= 0; i--)
        if (_map[i, y].GetType() == cellType)
          return new Point(i, y);
      return Point.Zero;
    }

    public Point GetRightCell(Type cellType)
    {
      Point max = Point.Zero;
      for (int i = 0; i < Height; i++)
      {
        Point p = GetRightCell(i, cellType);
        if (p.X > max.X)
          max = p;
      }
      return max;
    }


    public Point GetFreeRandomPoint()
    {
      Point p;
      do
      {
        p = new Point(RandomTool.D.NextInt(0, Width), RandomTool.D.NextInt(0, Height));
      } while (!IsFree(p));

      return p;
    }

    public Point GetRandomStandablePoint()
    {
      Point p;
      do
      {
        p = new Point(RandomTool.D.NextInt(0, Width), RandomTool.D.NextInt(0, Height - 1));
      } while (!IsFree(p) || _map[p.X, p.Y + 1] == null);

      return p;
    }

    public Point GetFreeGroundCell()
    {
      while (true)
      {
        var p = new Point(RandomTool.D.NextInt(1, Width - 1),
          RandomTool.D.NextInt(1, Height - 1));

        if (IsFree(p) && !IsFree(p.X, p.Y + 1))
          return p;
      }
    }

    public bool InRange(Point cell)
    {
      return InRange(cell.X, cell.Y);
    }

    public bool InRange(int x, int y)
    {
      return x >= 0 && x < Width && y >= 0 && y < Height;
    }

    public CreatureActor GetCreatureFromTile(Point tile)
    {
      if (!InRange(tile))
        return null;

      var tileObjects = _map[tile.X, tile.Y].GetTileObjects();
      for (int i = 0; i < tileObjects.Count; i++)
      {
        var p = tileObjects[i];
        if (!(p is CreatureActor)) continue;

        var creature = p as CreatureActor;
        if (creature.Active)
        {
          return creature;
        }
      }
      return null;
    }

    public List<TileActor> GetTileObjects(Point tile)
    {
      return this[tile].GetTileObjects().Where(p => p.Active).ToList();
    }

    public void AddActor(TileActor tileActor)
    {
      SetObjectTile(tileActor, new Point(-1, -1), tileActor.Tile);
      tileActor.TileChanged += TileActorOnTileChanged;
    }

    public void RemoveActor(TileActor tileActor)
    {
      var oldTile = this[tileActor.Tile];
      oldTile.RemoveTileObject(tileActor);

      tileActor.TileChanged -= TileActorOnTileChanged;
    }

    private void TileActorOnTileChanged(TileActor actor, Point newTile)
    {
      SetObjectTile(actor, actor.Tile, newTile);
    }

    public void SetObjectTile(TileActor tileActor, Point oldTile, Point newTile)
    {
      if (oldTile == newTile) return;

      if (InRange(oldTile))
        this[oldTile].RemoveTileObject(tileActor);

      if (InRange(newTile))
        this[newTile].AddTileObject(tileActor);
      else
        throw new ArgumentOutOfRangeException("newTilePosition " + newTile);
    }

    public List<TileActor> CheckCircleForTileObjects(Point start, int radius)
    {
      var tileObjects = new List<TileActor>();

      var begin = new Point(start.X - radius, start.Y - radius);
      var end = new Point(start.X + radius, start.Y + radius);

      for (int i = begin.X; i < end.X; i++)
      {
        for (int j = begin.Y; j < end.Y; j++)
        {
          CheckTileAt(i, j, tileObjects);
        }
      }

      return tileObjects;
    }


    private void CheckTileAt(int x, int y, List<TileActor> tileObjects)
    {
      if (InRange(x, y))
      {
        if (_map[x, y].HasAnyTileObjects())
        {
          tileObjects.AddRange(_map[x, y].GetTileObjects().Where(p => p.Active));
        }
      }
    }

    #region FOV algorithm

    //  Octant data
    //
    //    \ 1 | 2 /
    //   8 \  |  / 3
    //   -----+-----
    //   7 /  |  \ 4
    //    / 6 | 5 \
    //
    //  1 = NNW, 2 =NNE, 3=ENE, 4=ESE, 5=SSE, 6=SSW, 7=WSW, 8 = WNW

    /// <summary>
    /// Start here: go through all the octants which surround the player to
    /// determine which open cells are visible
    /// </summary>
    public void GetVisibleCells()
    {
      _visiblePoints = new HashSet<Point>();
      foreach (int o in _visibleOctants)
        ScanOctant(1, o, 1.0, 0.0);

    }

    /// <summary>
    /// Examine the provided octant and calculate the visible cells within it.
    /// </summary>
    /// <param name="pDepth">Depth of the scan</param>
    /// <param name="pOctant">Octant being examined</param>
    /// <param name="pStartSlope">Start slope of the octant</param>
    /// <param name="pEndSlope">End slope of the octance</param>
    protected void ScanOctant(int pDepth, int pOctant, double pStartSlope, double pEndSlope)
    {

      int visrange2 = VisualRange*VisualRange;
      int x = 0;
      int y = 0;

      switch (pOctant)
      {

        case 1: //nnw
          y = _player.Y - pDepth;
          if (y < 0) return;

          x = _player.X - Convert.ToInt32((pStartSlope*Convert.ToDouble(pDepth)));
          if (x < 0) x = 0;

          while (GetSlope(x, y, _player.X, _player.Y, false) >= pEndSlope)
          {
            if (GetVisDistance(x, y, _player.X, _player.Y) <= visrange2)
            {
              if (IsSolid(x, y))
              {
                if (IsFree(x - 1, y)) //prior cell within range AND open...
                  //...incremenet the depth, adjust the endslope and recurse
                  ScanOctant(pDepth + 1, pOctant, pStartSlope, GetSlope(x - 0.5, y + 0.5, _player.X, _player.Y, false));
              }
              else
              {

                if (x - 1 >= 0 && IsSolid(x - 1, y)) //prior cell within range AND open...
                  //..adjust the startslope
                  pStartSlope = GetSlope(x - 0.5, y - 0.5, _player.X, _player.Y, false);

                _visiblePoints.Add(new Point(x, y));
              }
            }
            x++;
          }
          x--;
          break;

        case 2: //nne

          y = _player.Y - pDepth;
          if (y < 0) return;

          x = _player.X + Convert.ToInt32((pStartSlope*Convert.ToDouble(pDepth)));
          if (x >= _visionBounds.Right - 1) x = _visionBounds.Right - 1;

          while (GetSlope(x, y, _player.X, _player.Y, false) <= pEndSlope)
          {
            if (GetVisDistance(x, y, _player.X, _player.Y) <= visrange2)
            {
              if (IsSolid(x, y))
              {
                if (x + 1 < _visionBounds.Right && IsSolid(x + 1, y))
                  ScanOctant(pDepth + 1, pOctant, pStartSlope, GetSlope(x + 0.5, y + 0.5, _player.X, _player.Y, false));
              }
              else
              {
                if (x + 1 < _visionBounds.Right && IsSolid(x + 1, y))
                  pStartSlope = -GetSlope(x + 0.5, y - 0.5, _player.X, _player.Y, false);

                _visiblePoints.Add(new Point(x, y));
              }
            }
            x--;
          }
          x++;
          break;

        case 3:

          x = _player.X + pDepth;
          if (x >= _visionBounds.Right) return;

          y = _player.Y - Convert.ToInt32((pStartSlope*Convert.ToDouble(pDepth)));
          if (y < 0) y = 0;

          while (GetSlope(x, y, _player.X, _player.Y, true) <= pEndSlope)
          {

            if (GetVisDistance(x, y, _player.X, _player.Y) <= visrange2)
            {

              if (IsSolid(x, y))
              {
                if (y - 1 >= 0 && IsFree(x, y - 1))
                  ScanOctant(pDepth + 1, pOctant, pStartSlope, GetSlope(x - 0.5, y - 0.5, _player.X, _player.Y, true));
              }
              else
              {
                if (y - 1 >= 0 && IsSolid(x, y - 1))
                  pStartSlope = -GetSlope(x + 0.5, y - 0.5, _player.X, _player.Y, true);

                _visiblePoints.Add(new Point(x, y));
              }
            }
            y++;
          }
          y--;
          break;

        case 4:

          x = _player.X + pDepth;
          if (x >= _visionBounds.Right) return;

          y = _player.Y + Convert.ToInt32((pStartSlope*Convert.ToDouble(pDepth)));
          if (y >= _visionBounds.Bottom) y = _visionBounds.Bottom - 1;

          while (GetSlope(x, y, _player.X, _player.Y, true) >= pEndSlope)
          {

            if (GetVisDistance(x, y, _player.X, _player.Y) <= visrange2)
            {

              if (IsSolid(x, y))
              {
                if (y + 1 < _visionBounds.Bottom && IsFree(x, y + 1))
                  ScanOctant(pDepth + 1, pOctant, pStartSlope, GetSlope(x - 0.5, y + 0.5, _player.X, _player.Y, true));
              }
              else
              {
                if (y + 1 < _visionBounds.Bottom && IsSolid(x, y + 1))
                  pStartSlope = GetSlope(x + 0.5, y + 0.5, _player.X, _player.Y, true);

                _visiblePoints.Add(new Point(x, y));
              }
            }
            y--;
          }
          y++;
          break;

        case 5:

          y = _player.Y + pDepth;
          if (y >= _visionBounds.Bottom) return;

          x = _player.X + Convert.ToInt32((pStartSlope*Convert.ToDouble(pDepth)));
          if (x >= _visionBounds.Right) x = _visionBounds.Right - 1;

          while (GetSlope(x, y, _player.X, _player.Y, false) >= pEndSlope)
          {
            if (GetVisDistance(x, y, _player.X, _player.Y) <= visrange2)
            {

              if (IsSolid(x, y))
              {
                if (x + 1 < _visionBounds.Bottom && IsFree(x + 1, y))
                  ScanOctant(pDepth + 1, pOctant, pStartSlope, GetSlope(x + 0.5, y - 0.5, _player.X, _player.Y, false));
              }
              else
              {
                if (x + 1 < _visionBounds.Bottom
                    && IsSolid(x + 1, y))
                  pStartSlope = GetSlope(x + 0.5, y + 0.5, _player.X, _player.Y, false);

                _visiblePoints.Add(new Point(x, y));
              }
            }
            x--;
          }
          x++;
          break;

        case 6:

          y = _player.Y + pDepth;
          if (y >= _visionBounds.Bottom) return;

          x = _player.X - Convert.ToInt32((pStartSlope*Convert.ToDouble(pDepth)));
          if (x < 0) x = 0;

          while (GetSlope(x, y, _player.X, _player.Y, false) <= pEndSlope)
          {
            if (GetVisDistance(x, y, _player.X, _player.Y) <= visrange2)
            {

              if (IsSolid(x, y))
              {
                if (x - 1 >= 0 && IsFree(x - 1, y))
                  ScanOctant(pDepth + 1, pOctant, pStartSlope, GetSlope(x - 0.5, y - 0.5, _player.X, _player.Y, false));
              }
              else
              {
                if (x - 1 >= 0
                    && IsSolid(x - 1, y))
                  pStartSlope = -GetSlope(x - 0.5, y + 0.5, _player.X, _player.Y, false);

                _visiblePoints.Add(new Point(x, y));
              }
            }
            x++;
          }
          x--;
          break;

        case 7:

          x = _player.X - pDepth;
          if (x < 0) return;

          y = _player.Y + Convert.ToInt32((pStartSlope*Convert.ToDouble(pDepth)));
          if (y >= _visionBounds.Bottom) y = _visionBounds.Bottom - 1;

          while (GetSlope(x, y, _player.X, _player.Y, true) <= pEndSlope)
          {

            if (GetVisDistance(x, y, _player.X, _player.Y) <= visrange2)
            {

              if (IsSolid(x, y))
              {
                if (y + 1 < _visionBounds.Bottom && IsFree(x, y + 1))
                  ScanOctant(pDepth + 1, pOctant, pStartSlope, GetSlope(x + 0.5, y + 0.5, _player.X, _player.Y, true));
              }
              else
              {
                if (y + 1 < _visionBounds.Bottom && IsSolid(x, y + 1))
                  pStartSlope = -GetSlope(x - 0.5, y + 0.5, _player.X, _player.Y, true);

                _visiblePoints.Add(new Point(x, y));
              }
            }
            y--;
          }
          y++;
          break;

        case 8: //wnw

          x = _player.X - pDepth;
          if (x < 0) return;

          y = _player.Y - Convert.ToInt32((pStartSlope*Convert.ToDouble(pDepth)));
          if (y < 0) y = 0;

          while (GetSlope(x, y, _player.X, _player.Y, true) >= pEndSlope)
          {

            if (GetVisDistance(x, y, _player.X, _player.Y) <= visrange2)
            {

              if (IsSolid(x, y))
              {
                if (y - 1 >= 0 && IsFree(x, y - 1))
                  ScanOctant(pDepth + 1, pOctant, pStartSlope, GetSlope(x + 0.5, y - 0.5, _player.X, _player.Y, true));

              }
              else
              {
                if (y - 1 >= 0 && IsSolid(x, y - 1))
                  pStartSlope = GetSlope(x - 0.5, y - 0.5, _player.X, _player.Y, true);

                _visiblePoints.Add(new Point(x, y));
              }
            }
            y++;
          }
          y--;
          break;
      }


      if (x < 0)
        x = 0;
      else if (x >= _visionBounds.Right)
        x = _visionBounds.Right - 1;

      if (y < 0)
        y = 0;
      else if (y >= _visionBounds.Bottom)
        y = _visionBounds.Bottom - 1;

      if (pDepth < VisualRange & IsFree(x, y))
        ScanOctant(pDepth + 1, pOctant, pStartSlope, pEndSlope);

    }

    /// <summary>
    /// Get the gradient of the slope formed by the two points
    /// </summary>
    /// <param name="pInvert">Invert slope</param>
    /// <returns></returns>
    private double GetSlope(double pX1, double pY1, double pX2, double pY2, bool pInvert)
    {
      if (pInvert)
        return (pY1 - pY2)/(pX1 - pX2);
      else
        return (pX1 - pX2)/(pY1 - pY2);
    }


    /// <summary>
    /// Calculate the distance between the two points
    /// </summary>
    /// <returns>Distance</returns>
    private int GetVisDistance(int pX1, int pY1, int pX2, int pY2)
    {
      return ((pX1 - pX2)*(pX1 - pX2)) + ((pY1 - pY2)*(pY1 - pY2));
    }

    #endregion
  }
}