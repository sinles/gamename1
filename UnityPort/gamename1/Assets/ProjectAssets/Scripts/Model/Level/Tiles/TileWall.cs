﻿namespace GameName1.Tiles
{
    public class TileWall:Tile
    {
        protected override TileSpriteData GetSpriteData()
        {
            return new TileSpriteData();
        }

        public override Tile Copy()
        {
            return new TileWall();
        }

        public override bool IsSolid
        {
            get { return true; }
        }

        public override int ProtectionLevel {
            get { return 0; }
        }
    }
}
