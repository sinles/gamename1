﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameObjectExtensions;
using UnityEngine;

namespace GameName1.Tiles
{
    public class TileForest: Tile
    {
        protected override TileSpriteData GetSpriteData()
        {
            var spriteData = new TileSpriteData();

            spriteData.Generic = "forest";
            spriteData.Middle = "forest_middle";
            spriteData.BottomMiddle = "forest_bottom_middle";
            spriteData.TopMiddle = "forest_top_middle";
            spriteData.MiddleRight = "forest_middle_right";
            spriteData.MiddleLeft = "forest_middle_left";
            spriteData.BottomLeft = "forest_bottom_left";
            spriteData.BottomRight = "forest_bottom_right";
            spriteData.TopRight = "forest_top_right";
            spriteData.TopLeft = "forest_top_left";

            return spriteData;
        }

        protected override void OnInit()
        {
            base.OnInit();
        }


        public override Tile Copy()
        {
            return new TileForest();
        }

        public override bool IsSolid
        {
            get { return false; }
        }

        public override int ProtectionLevel
        {
            get { return 2; }
        }


    }
}
