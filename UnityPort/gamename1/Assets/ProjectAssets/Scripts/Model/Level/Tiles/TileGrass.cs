﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameObjectExtensions;
using UnityEngine;

namespace GameName1.Tiles
{
    public class TileGrass:Tile
    {
        protected override void OnInit()
        {
            base.OnInit();
        }

        protected override TileSpriteData GetSpriteData()
        {
            var spriteData = new TileSpriteData();
            spriteData.SetAll("grass");
            return spriteData;
        }

        public override Tile Copy()
        {
            return new TileGrass();
        }

        public override bool IsSolid
        {
            get { return false; }
        }

        public override int ProtectionLevel
        {
            get { return 0; }
        }
    }
}
