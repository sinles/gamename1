﻿using GameName1.Extensions;
using GameObjectExtensions;
using UnityEngine;

namespace GameName1.Tiles
{
    public class TileEmpty:Tile
    {
        protected override TileSpriteData GetSpriteData()
        {
            return new TileSpriteData();
        }

        public override Tile Copy()
        {
            return new TileEmpty();
        }

        public override bool IsSolid
        {
            get { return false; }
        }

        public override int ProtectionLevel
        {
            get { return 0; }
        }
    }
}
