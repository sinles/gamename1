﻿using System.Collections.Generic;
using System;
using GameName1.Actors;
using GameName1.Tiles;

namespace GameName1.LevelGeneration
{
    public enum Stage:sbyte
    {
        Nothing = -1,
        Miners = 0,
        Nature = 1,
        Adventurer = 2,
        WayChecker = 3,
        Final = 4
    }


    public class DungeonGenerator : LevelGenerator
    {
        private readonly List<ILevelComponent> _components; 

        public Stage Stage { get;private set; }

        public int RoomCount;
        public int RoomsMaxWidth;
        public int RoomsMaxHeight;

        /// <summary>
        /// percantage of chance to smooth walls, 0 - 100
        /// </summary>
        public float WallSmoothing = 50;

        internal ResourseMap ResourseMap;
        internal int MaxAllowedWidth { get; set; }

        /// <summary>
        /// Points which has to be reached 
        /// </summary>
        internal byte[,] ImportantPointsMap;
        internal int ImportantsPointsCount;
        private readonly List<Miner> _miners;



        public bool Fast;

        public DungeonGenerator(bool fast = false)
            :base()
        {
            _components = new List<ILevelComponent>();
            _miners = new List<Miner>();
            Fast = fast;
        }

        protected override void GenerateTileMap()
        {
            ImportantPointsMap = new byte[Width, Height];
            MaxAllowedWidth = TileMap.Width;// - TileMap.Width/3;

            Stage = Stage.Nothing;
            _miners.Clear();

            ResourseMap = new ResourseMap(Width, Height);
            do
            {
                UpdateMiners();
                if (_miners.Count == 0)
                {
                    Stage += 1;
                    SetCurrentStage();
                    if (Stage == Stage.Final)
                    {
                        _miners.Clear();
                        FinishGeneration();
                        break;
                    }
                }
            } while (true);
        }

        protected override List<TileActor> GenerateTileObjectsInternal()
        {
            var tileActors = new List<TileActor>();



//
//            for (int i = 0; i < (TileMap.Width + TileMap.Height) / 20; i++)
//            {
//                var tileSplat = new TileSplat("Blood1");
//
//                tileSplat.Tile = TileMap.GetFreeRandomPoint();
//            }

            return tileActors;  
        }

        public void UpdateComponents()
        {
            foreach (var levelGenerationComponent in _components)
                levelGenerationComponent.Init();
        }

        public void AddComponent(ILevelComponent component)
        {
            _components.Add(component);
        }
        
        private void SetCurrentStage()
        {
            switch (Stage)
            {
                case Stage.Miners:
                    ResourseMap = new ResourseMap(Width, Height);

                    TileMap.FillWith(new TileWall());
                    var m = new Miner(this, 4, Height - 2);
                    m.Init();
                    _miners.Add(m);

                    m = new Miner(this, MaxAllowedWidth - 4, Height - 2);
                    _miners.Add(m);
                    m.Init();

                    m = new Miner(this, MaxAllowedWidth/2, Height - 2);
                    _miners.Add(m);
                    m.Init();
                    break;
                case Stage.Nature:
                    
                    for (int i = 0; i < RoomCount; i++)
                        AddRoomAtRandomPosition();

                    int height = RandomTool.D.NextInt(2, Height - 2);

                    MakeConnectionX(TileMap.GetLeftCell(height, typeof(TileEmpty)),
                                   TileMap.GetRightCell(height, typeof(TileEmpty)));

                    SmoothWalls();

                    UpdateComponents();
                    DetermineImportantPoints();

                    break;
                case Stage.Adventurer:
//                    p = GetFreeGroundCell();
//                    var adv = new Adventurer(this, p.X, p.Y);
//                    _miners.Add(adv);
                    break;
                case Stage.WayChecker:
                    if (Fast)
                        break;
                    break;
            }
        }


        public bool InRange(int x, int y)
        {
            return x > 0 && y > 0 && x < Width - 1 && y < Height - 1 && x < MaxAllowedWidth;
        }

        private void SmoothWalls()
        {
            for (int i = 2; i < TileMap.Width - 2; i++)
                for (int j = 2; j < TileMap.Height - 2; j++)

                    if (!TileMap.IsFree(i, j))
                    {
                        int wallCount = GetCellCountAround(i, j, typeof (TileWall));
                        if (wallCount == 4)
                            if (RandomTool.D.NextBool(WallSmoothing))
                                TileMap[i, j] = new TileEmpty();
                    }
        }



        private void UpdateMiners()
        {
            foreach (var miner in _miners)
                miner.Step();

            for (int i = 0; i < _miners.Count; i++)
                if (_miners[i].Cell.Y <= 0)
                {
                    _miners[i].Remove();
                    _miners.Remove(_miners[i]);
                    i--;
                }
        }

        private void DetermineImportantPoints()
        {
            for (int i = 1; i < MaxAllowedWidth - 1; i++)
                for (int j = 1; j < TileMap.Height - 1; j++)
                    if (TileMap.IsFree(i, j) && !TileMap.IsFree(i, j + 1))
                    {
                        ImportantPointsMap[i, j] = 1;
                        ImportantsPointsCount++;
                    }
        }


        private int GetCellCountAround(int x , int y, Type type)
        {
            int count = 0;

            for (int i = x - 1; i <= x + 1; i++)
                for (int j = y - 1; j <= y + 1; j++)
                    if (TileMap[i, j].GetType() == type)
                        count++;
            return count;
        }

        private void AddRoomAtRandomPosition()
        {
            int x, y, width, height;

            do
            {
                width = RandomTool.D.NextInt(0, RoomsMaxWidth);
                height = RandomTool.D.NextInt(0, RoomsMaxHeight);
                x = RandomTool.D.NextInt(2, Width - width - 2);
                y = RandomTool.D.NextInt(2, Height - height - 2);
            } while (!TileMap.IsFree(x,y));
            AddRoom(x, y, width, height);
        }

        public void PrintDebug()
        {
            for (int j = 0; j < Height; j++)
            {
                Console.WriteLine();
                for (int i = 0; i < Width; i++)
                {
                    Console.Write(TileMap[i, j].IsSolid ? "1" : "0");
                }
            }
        }
    }
}
