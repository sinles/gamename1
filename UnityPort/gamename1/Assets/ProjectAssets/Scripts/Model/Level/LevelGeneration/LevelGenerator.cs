﻿using System.Collections.Generic;
using GameName1.Actors;
using GameName1.Tiles;

namespace GameName1.LevelGeneration
{
    public delegate void FinishGenerationEventHandler();


    public abstract class LevelGenerator
    {
        public event FinishGenerationEventHandler GenerationFinishedEvent;

        public LevelGenerator()
        {
        }

        private void CreateTileMap()
        {
            var size = GetTileMapSize();

            Width = size.X;
            Height = size.Y;
            TileMap = new TileMap(Width, Height);
        }


        protected virtual Point GetTileMapSize()
        {
            return new Point(20, 20);
        }

        public virtual void Generate()
        {
            CreateTileMap();
            GenerateTileMap();
        }

        protected abstract void GenerateTileMap();

        protected abstract List<TileActor> GenerateTileObjectsInternal();

        protected void FinishGeneration()
        {
            if (GenerationFinishedEvent != null)
            {
                GenerationFinishedEvent();
            }
        }

        protected void AddRoom(int x, int y, int width, int height)
        {
            for (int i = x; i <= x + width; i++)
                for (int j = y; j <= y + height; j++)
                    TileMap[i, j] = new TileEmpty();
        }

        protected void AddBoundedRoom(int x, int y, int width, int height)
        {
            for (int i = x; i <= x + width; i++)
                for (int j = y; j <= y + height; j++)
                {
                    if (TileMap.InRange(i, j))
                        TileMap[i, j] = new TileEmpty();
                }

            Point pos = new Point(0, 0);
            Point dir = new Point(1, 0);
            int P = width * 2 + height * 2;
            do
            {
                TileMap[pos.X + x, pos.Y + y] = new TileWall();
                if (pos.X + dir.X > width)
                {
                    dir.Y = 1;
                    dir.X = 0;
                }
                else if (pos.X + dir.X < 0)
                {
                    dir.Y = -1;
                    dir.X = 0;
                }
                if (pos.Y + dir.Y > height)
                {
                    dir.X = -1;
                    dir.Y = 0;
                }
                pos.X += dir.X;
                pos.Y += dir.Y;
                //else if (pos.Y + dir.Y < 0)
                //{
                //    dir.X = 1;
                //    dir.Y = 0;
                //}

            } while (P-- > 0);
        }

        protected void MakeConnectionX(Point p1, Point p2)
        {
            for (int i = p1.X; i < p2.X; i++)
            {
                TileMap[i, p1.Y] = new TileEmpty();
            }
        }


        protected void MakeConnectionY(Point p1, Point p2)
        {
            for (int i = p1.Y; i < p2.Y; i++)
            {
                TileMap[p1.X, i] = new TileEmpty();
            }
        }

        public int Width { get; private set; }
        public int Height { get; private set; }
        internal TileMap TileMap { get; private set; }
    }
}
