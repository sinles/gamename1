﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameName1.Levels;
using GameName1.Tiles;

namespace GameName1.LevelGeneration
{
    public class LevelGeneratorFabric
    {
        public LevelGeneratorFabric()
        {
        }

        public LevelGenerator CreateFromLevelType(LevelType levelType)
        {
            switch (levelType)
            {
                case LevelType.Forest:
                    return new DungeonGenerator();
                default:
                    throw new ArgumentOutOfRangeException("levelType");
            }
        }
    }
}
