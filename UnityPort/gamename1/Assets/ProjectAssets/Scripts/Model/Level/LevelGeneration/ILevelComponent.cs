﻿namespace GameName1.LevelGeneration
{
    public interface ILevelComponent
    {
        void Init();
        void Update(float dt);
    }
}
