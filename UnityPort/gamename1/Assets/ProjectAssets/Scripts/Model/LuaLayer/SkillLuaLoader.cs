﻿using System.Collections.Generic;
using System.Linq;
using GameName1.Actors;
using GameName1.Tiles;
using SLua;
using UnityEngine;

namespace GameName1.Lua
{
  public class SkillLuaWrapper: LuaWrapperBase
  {
    public TileMap TileMap { get; private set; }
    public CreatureActor CreatureActor { get; private set; }

    public string SpriteName
    {
      get { return Object["SpriteName"] as string; }
    }

    public SkillLuaWrapper(string name, TileMap tileMap, CreatureActor actor)
      :base(name)
    {
      TileMap = tileMap;
      CreatureActor = actor;
    }

    public bool Apply(LuaTable param)
    {
      var result = Object.invoke("Apply", Object, param);
      if(result == null) return true;

      return (bool)result;
    }

    public void OnDone()
    {
      Object.invoke("OnDone", Object);
    }

    public bool IsDone()
    {
      return (bool)Object.invoke("IsDone", Object);
    }

    public void Update()
    {
      Object.invoke("Update", Object);
    }

    public bool IsInRange(Point target)
    {
      return (bool)Object.invoke("IsInRange", Object, target);
    }

    public List<Point> GetTargetTiles(LuaTable param)
    {
      var table = Object.invoke("GetTargetTiles", Object, param) as LuaTable;

      if (table == null) return new List<Point>();

      return table.Select(p => (Point)p.value).ToList();
    }
  }

  public class SkillLuaLoader: LuaLoader<SkillLuaWrapper>
  {
    public SkillLuaLoader() 
      : base("skillsTable")
    {
    }

    public virtual SkillLuaWrapper GetSkill(string name, TileMap tileMap, CreatureActor actor)
    {
      var skillWrapper = new SkillLuaWrapper(name, tileMap, actor);
      ReloadWrapper(skillWrapper);
      return skillWrapper;
    }

    protected override LuaTable CreateLuaObject(SkillLuaWrapper wrapper)
    {
      return Table.invoke("Create", wrapper.Name, wrapper.TileMap, wrapper.CreatureActor) as LuaTable;
    }
  }
}
