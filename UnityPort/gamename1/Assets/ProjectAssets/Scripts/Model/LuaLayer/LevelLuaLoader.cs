﻿using System.Collections.Generic;
using System.Linq;
using GameName1.Actors;
using GameName1.Fight;
using GameName1.Tiles;
using SLua;

namespace GameName1.Lua
{
  public class LevelLuaWrapper:LuaWrapperBase
  {
    public LevelLuaWrapper(string name) 
      : base(name)
    {
    }

    public void OnStepStart()
    {
      Object.invoke("OnStepStart", Object);
    }
    public TileMap GenTileMap()
    {
      var table = Object.invoke("GenTileMap", Object);



      return table as TileMap;
    }
    public List<CreatureInfo> GenCreatures()
    {
      var table = Object.invoke("GenCreatures", Object) as LuaTable;
      var result = table.Select(p => p.value as CreatureInfo).ToList();
      return result;
    }
  }

  public class LevelLuaLoader:LuaLoader<LevelLuaWrapper>
  {
    public LevelLuaLoader() 
      : base("levelTable")
    {
      
    }

    public LevelLuaWrapper GetLevel(string name)
    {
      var level = new LevelLuaWrapper(name);
      ReloadWrapper(level);
      return level;
    }

    protected override LuaTable CreateLuaObject(LevelLuaWrapper wrapper)
    {
      return Table.invoke("Create", wrapper.Name) as LuaTable;
    }
  }
}
