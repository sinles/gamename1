﻿using System;
using System.Collections.Generic;
using SLua;

namespace GameName1.Lua
{
  public class LuaRootLoader
  {
    private readonly bool _createGo;
    public event Action LoadFinished;
    public AILuaLoader AiLoader { get; private set; }
    public SkillLuaLoader SkillLoader { get; private set; }
    public LevelLuaLoader LevelLoader { get; private set; }
    public EventsLuaLoader EventsLoader { get; private set; }
    public LuaState State{
      get { return svr.luaState; }
    }

    private readonly List<ILuaLoader> _loaders;

    private LuaSvr svr;
    private LuaTable self;

    public LuaRootLoader(bool createGo = true)
    {
      _createGo = createGo;
      _loaders = new List<ILuaLoader>();
    }

    public void Init()
    {
      svr = new LuaSvr(_createGo);
      svr.init(null, () =>
      {
        self = (LuaTable)svr.start("main");

        AiLoader = new AILuaLoader();
        _loaders.Add(AiLoader);

        SkillLoader = new SkillLuaLoader();
        _loaders.Add(SkillLoader);

        LevelLoader = new LevelLuaLoader();
        _loaders.Add(LevelLoader);

        EventsLoader = new EventsLuaLoader();
        _loaders.Add(EventsLoader);

        foreach (var luaLoader in _loaders)
        {
          LoadLoader(luaLoader);
        }

        if (LoadFinished !=null)
          LoadFinished();
      });
    }

    public void Reload()
    {
      if(svr != null)
        svr.luaState.Dispose();

      svr = new LuaSvr(_createGo);
      svr.init(null, () =>
      {
        self = (LuaTable)svr.start("main");
        foreach (var luaLoader in _loaders)
        {
          LoadLoader(luaLoader);
        }
      });
    }

    public void LoadLoader(ILuaLoader loader)
    {
      loader.Load(svr.luaState);
    }

    public void CleanCache()
    {
      foreach (var luaLoader in _loaders)
      {
        luaLoader.Clean();
      }
    }
  }
}
