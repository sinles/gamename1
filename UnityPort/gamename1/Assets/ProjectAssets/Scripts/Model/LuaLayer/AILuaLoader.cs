﻿using GameName1.Actors;
using GameName1.Fight;
using SLua;

namespace GameName1.Lua
{
  public class AILuaBehaviourWrapper:LuaWrapperBase
  {
    public CreatureActor Actor { get; private set; }
    public FightManager Manager { get; private set; }
    public FightPlayer Player { get; private set; }

    public AILuaBehaviourWrapper(string name, FightManager fightManager, FightPlayer fightPlayer, CreatureActor actor) 
      : base(name)
    {
      Manager = fightManager;
      Player = fightPlayer;
      Actor = actor;
    }

    public void Update()
    {
      Object.invoke("Update", Object);
    }
  }

  public class AILuaLoader:LuaLoader<AILuaBehaviourWrapper>
  {
    public AILuaLoader() 
      : base("aiTable")
    {
      
    }

    public AILuaBehaviourWrapper GetBehaviour(string name, FightManager fightManager, FightPlayer fightPlayer, CreatureActor actor)
    {
      var behaviour = new AILuaBehaviourWrapper(name, fightManager, fightPlayer, actor);
      ReloadWrapper(behaviour);
      return behaviour;
    }

    protected override LuaTable CreateLuaObject(AILuaBehaviourWrapper wrapper)
    {
      return Table.invoke("Create", wrapper.Name, wrapper.Manager, wrapper.Player, wrapper.Actor) as LuaTable;
    }
  }
}
