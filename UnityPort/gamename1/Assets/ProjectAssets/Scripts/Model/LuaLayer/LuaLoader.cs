﻿using System.Collections.Generic;
using SLua;
using UnityEngine;

namespace GameName1.Lua
{
  public class LuaWrapperBase
  {
    public LuaState State;
    public string Name;
    public LuaTable Object;

    public LuaWrapperBase(string name)
    {
      Name = name;
    }

    public void OnReload(LuaState state, LuaTable obj)
    {
      Object = obj;
      State = state;
    }
  }

  public interface ILuaLoader
  {
    void Load(LuaState luaState);
    void Clean();
  }

  public abstract class LuaLoader<T>: ILuaLoader where T:LuaWrapperBase
  {
    public LuaState LuaState { get; private set; }
    protected LuaTable Table { get; private set; }

    private readonly HashSet<T> _cache;

    private readonly string _tableName;

    protected LuaLoader(string tableName)
    {
      _cache = new HashSet<T>();
      _tableName = tableName;
    }

    public void Load(LuaState luaState)
    {
      LuaState = luaState;
      LoadTable(luaState);
      OnReload();
    }

    public void Clean()
    {
      _cache.Clear();
    }

    private void LoadTable(LuaState luaState)
    {
      Table = luaState[_tableName] as LuaTable;
    }

    private void OnReload()
    {
      foreach (var wrapper in _cache)
      {
        ReloadWrapper(wrapper);
      }
    }

    protected abstract LuaTable CreateLuaObject(T wrapper);

    protected virtual void ReloadWrapper(T wrapper)
    {
      if (wrapper == null)
      {
        Debug.LogError("Wrapper null! " + GetType().Name);
        return;
      }

      var luaObject = CreateLuaObject(wrapper);

      if (luaObject == null)
      {
        Debug.LogError("Not reloaded " + wrapper.Name);
        return;
      }

      _cache.Add(wrapper);
      wrapper.OnReload(LuaState, luaObject);
    }
  }
}