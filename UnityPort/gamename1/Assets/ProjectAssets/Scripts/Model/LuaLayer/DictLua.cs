﻿using System.Collections.Generic;
using SLua;

namespace GameName1.Lua
{
  public class DictLua: Dictionary<string, object>
  {
    public LuaTable ToLua(LuaState state)
    {
      var  table = new LuaTable(state);

      foreach (var kvp in this)
      {
        table[kvp.Key] = kvp.Value;
      }

      return table;
    }
  }
}
