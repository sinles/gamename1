﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameName1.Skills;

namespace GameName1.Items
{
    public class ArmorFabric:ItemFabric
    {
        public override Item Create()
        {
            var armorParams = new ArmorParams
            {
                Protection = 15,
                Slot = EEQeuipmentSlot.Torso,
                Type = ItemType.LightArmor
            };

            return new Armor(armorParams);
        }
    }
}
