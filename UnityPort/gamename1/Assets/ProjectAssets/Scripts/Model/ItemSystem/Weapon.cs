﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using GameName1.Skills;

namespace GameName1.Items
{
    public class WeaponParams : ItemParams
    {
        public float Damage;
    }

    public class Weapon:Item
    {
        public Weapon()
            : base()
        {
            
        }

        public Weapon(WeaponParams weaponParams)
            : base(weaponParams)
        {
            Damage = weaponParams.Damage;
        }

        public override string StatsText()
        {
            return "Damage: " + Damage;
        }

        public override void Save(XElement node)
        {
            base.Save(node);
            XmlHelper.WriteSimple(node, "Damage", Damage);
        }

        public override void Load(XElement node)
        {
            base.Load(node);

            Damage = XmlHelper.ReadFloat(node, "Damage");

        }

        public float Damage { get; private set; }
    }
}
