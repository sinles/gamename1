﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using GameName1.Skills;

namespace GameName1.Items
{
    public class ArmorParams : ItemParams
    {
        public float Protection;
    }

    public class Armor:Item
    {
        public Armor()
            :base()
        {
            
        }

        public Armor(ArmorParams armorParams)
            : base(armorParams)
        {
            Protection = armorParams.Protection;
        }

        public override string StatsText()
        {
            return "Armor: " + Protection;
        }

        public override void Save(XElement node)
        {
            base.Save(node);

            XmlHelper.WriteSimple(node, "Protection", Protection);
            XmlHelper.WriteType(node, "ArmorType", (int)ArmorType);
        }

        public override void Load(XElement node)
        {
            base.Load(node);

            Protection = XmlHelper.ReadFloat(node, "Protection");
            ArmorType = (EArmorType)XmlHelper.ReadType(node, "ArmorType");
        }

        public float Protection { get; private set; }
        public EArmorType ArmorType { get; private set; }

        public enum EArmorType
        {
            Heavy,
            Light
        }
    }
}
