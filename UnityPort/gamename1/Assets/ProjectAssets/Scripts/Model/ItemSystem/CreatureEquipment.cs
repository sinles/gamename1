﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Xml.Linq;
using GameName1.Actors;

namespace GameName1.Items
{
    public enum EEQeuipmentSlot
    {
        Torso,
        Head,
        Legs,
        RightHand
    }

    public class CreatureEquipment:ISaveable
    {
        public CreatureEquipment()
        {
        }

        public float GetArmorBonus()
        {
            return ItemSafe(Torso, i => i.Protection) + ItemSafe(Head, i => i.Protection);
        }

        public float GetDamageBonus()
        {
            return ItemSafe(RightHand, i => i.Damage);
        }

        private T1 ItemSafe<T, T1>(T item, Func<T, T1> itemAction)
        {
            if (item != null)
            {
                return itemAction(item);
            }

            return default(T1);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        /// <returns>Old item</returns>
        public Item Equip(Item item)
        {
            Item oldItem = null;
            switch (item.Slot)
            {
                case EEQeuipmentSlot.Torso:
                    oldItem = Torso;
                    EquipTorso(item as Armor);
                    break;
                case EEQeuipmentSlot.Head:
                    oldItem = Head;
                    EquipHead(item as Armor);
                    break;
                case EEQeuipmentSlot.Legs:
                    break;
                case EEQeuipmentSlot.RightHand:
                    oldItem = RightHand;
                    EquipWeapon(item as Weapon);
                    break;
                default:
                    throw new ArgumentOutOfRangeException("slot");
            }

            return oldItem;
        }

        public void EquipTorso(Armor armor)
        {
            Torso = armor;
        }

        public void EquipHead(Armor armor)
        {
            Head = armor;
        }

        public void EquipWeapon(Weapon weapon)
        {
            RightHand = weapon;
        }

        public void Save(XElement node)
        {
            var root = XmlHelper.CreateChild(node, "Equipment");

            if (Torso != null)
            {
                var torso = XmlHelper.CreateChild(root, "Torso");
                Torso.Save(torso);
            }
            if (Head != null)
            {
                var head = XmlHelper.CreateChild(root, "Head");
                Head.Save(head);
            }
            if (RightHand != null)
            {
                var rightHand = XmlHelper.CreateChild(root, "RightHand");
                RightHand.Save(rightHand);
            }
        }

        public void Load(XElement node)
        {
            var root = XmlHelper.GetChild(node, "Equipment");


            var torso = XmlHelper.GetChild(root, "Torso");
            if (torso != null)
            {
                Torso = new Armor();
                Torso.Load(torso);
            }

            var head = XmlHelper.GetChild(root, "Head");
            if (head != null)
            {
                Head = new Armor();
                Head.Load(head);
            }

            var rightHand = XmlHelper.GetChild(root, "RightHand");
            if (rightHand != null)
            {
                RightHand = new Weapon();
                RightHand.Load(rightHand);
            }
        }

        public Armor Torso { get; private set; }
        public Armor Head { get; private set; }
        public Weapon RightHand { get; private set; }
        public List<Armor> Armors {
            get
            {
                return new List<Armor>()
                {
                    Torso, Head
                }.Where(p => p != null).ToList();
            }
        }
    }
}
