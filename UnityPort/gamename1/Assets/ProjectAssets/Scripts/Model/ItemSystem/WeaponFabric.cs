﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameName1.Skills;

namespace GameName1.Items
{
    public class WeaponFabric:ItemFabric
    {
        public override Item Create()
        {
            var weaponParams = new WeaponParams
            {
                Type = ItemType.SteelSword, 
                Slot = EEQeuipmentSlot.RightHand,
                Damage = 3
            };
            return new Weapon(weaponParams);
        }
    }
}
