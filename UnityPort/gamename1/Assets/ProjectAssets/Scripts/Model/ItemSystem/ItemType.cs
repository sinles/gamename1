﻿namespace GameName1.Items
{
    public enum ItemType
    {
        //////////////////////////////Armors
        LightArmor = 0,
        LightCape = 1,


        //////////////////////////////Weapons
        SteelSword = 2,
    }

    public enum WeaponType
    {
        Sword,
        Axe
    }
}
