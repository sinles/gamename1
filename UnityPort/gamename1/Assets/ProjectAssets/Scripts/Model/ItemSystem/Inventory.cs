﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace GameName1.Items
{
    public class Inventory:ISaveable
    {
        private readonly List<Item> _items;

        public Inventory()
        {
            _items = new List<Item>();
            Gold = 1200;
        }

        public void AddGold(int amount)
        {
            Gold += amount;
        }

        public bool UseGold(int amount)
        {
            if (!HasEnoughGold(amount))
            {
                return false;
            }

            Gold -= amount;

            return true;
        }
        public bool HasEnoughGold(int price)
        {
            return Gold > price;
        }

        public void AddItem(Item item)
        {
            if (item == null)
            {
                return;
            }
            _items.Add(item);
        }

        public void RemoveItem(Item item)
        {
            _items.Remove(item);
        }

        public void Save(XElement node)
        {
            XElement root = XmlHelper.CreateChild(node, "Inventory");


            XmlHelper.WriteSimple(root, "Gold", Gold);

            foreach (var item in Items)
            {
                var child = XmlHelper.CreateChild(root, "Item");
                item.Save(child);
            }
        }

        public void Load(XElement node)
        {
            XElement root = XmlHelper.GetChild(node, "Inventory");

            Gold = XmlHelper.ReadInt(root, "Gold");

            List<XElement> items = XmlHelper.GetChildren(root, "Item");

            foreach (var itemNode in items)
            {
                var item = XmlHelper.ReadClass<Item>(itemNode);
                item.Load(itemNode);
                AddItem(item);
            }
        }

        public List<Item> Items
        {
            get { return _items.ToList(); }
        }

        public int Gold { get; private set; }

    }
}
