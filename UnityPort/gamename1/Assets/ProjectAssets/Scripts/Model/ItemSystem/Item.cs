﻿using System.Xml.Linq;
using GameName1.Skills;

namespace GameName1.Items
{
    public class ItemParams
    {
        public ItemType Type;
        public EEQeuipmentSlot Slot;
    }

    public abstract class Item:ISaveable
    {
        public Item()
        {
            
        }

        public Item(ItemParams itemParams)
        {
            Slot = itemParams.Slot;
            Type = itemParams.Type;
        }

        public virtual string StatsText()
        {
            return "empty stats";
        }

        public virtual void Save(XElement node)
        {
            XElement slot = new XElement("Slot");
            slot.SetValue((int)Slot);

            XElement ability = new XElement("Ability");
            ability.SetValue((int)Ability);

            XElement type = new XElement("ItemType");
            type.SetValue((int)Type);

            node.Add(slot);
            node.Add(ability);
            node.Add(type);


            XmlHelper.WriteClass(node, GetType());
        }

        public virtual void Load(XElement node)
        {
            Slot = (EEQeuipmentSlot)XmlHelper.ReadType(node, "Slot");
            Ability = (AbilityType)XmlHelper.ReadType(node, "Ability");
            Type = (ItemType)XmlHelper.ReadType(node, "ItemType");
        }

        public EEQeuipmentSlot Slot { get; set; }
        public AbilityType Ability { get; set; }
        public ItemType Type { get; set; }
    }
}
