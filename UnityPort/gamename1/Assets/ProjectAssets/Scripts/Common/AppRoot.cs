﻿using GameName1.Data;
using GameName1.Lua;
using GameName1.Network;
using Glide;
using UnityEngine;

public partial class AppRoot : MonoBehaviour
{
  public LuaRootLoader LuaLoader;

  private static AppRoot _instance;

  void Awake()
  {
    _instance = this;

    LuaLoader = new LuaRootLoader();
    LuaLoader.Init();
  }

  public void Start()
  {
    SetState(new StateMainMenu());
    Sprites.Init();
  }



  public void Update()
  {
    Dispatcher.InvokePending();
    Tweener.I.Update(Time.deltaTime);
    UpdateStates();

    if (Input.GetKeyDown(KeyCode.R))
    {
      LuaLoader.Reload();
    }
  }

  public void LateUpdate()
  {
  }

  void OnDestroy()
  {
    NetworkManager.I.Stop();
  }

  public void OnLevelWasLoaded(int level)
  {
    OnSceneLoadedStates();
  }

  public static AppRoot I
  {
    get { return _instance; }
  }
}
