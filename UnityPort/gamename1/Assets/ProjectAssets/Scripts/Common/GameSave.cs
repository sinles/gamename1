﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using GameName1.Actors;
using GameName1.Fight;
using GameName1.Levels;
using GameName1.Tiles;

namespace GameName1
{
    public class GameSave
    {
        public void Save(Level level, List<Actor> actors, List<FightPlayer> players, List<FightPlayer> fightPlayers = null)
        {
            XElement root = new XElement("Save");


            SaveGameObjects(root, actors);
            SavePlayers(root, players, fightPlayers);

            level.Save(root);

            XDocument document = new XDocument(root);

            XmlWriterSettings settings = new XmlWriterSettings
            {
                Indent = true
            };
            using (XmlWriter writer = XmlWriter.Create("savedata.xml", settings))
            {
                document.Save(writer);
            }
           

            SaveTileMap(level);
        }
        
        private void SavePlayers(XElement root, List<FightPlayer> players, List<FightPlayer> fightPlayers)
        {
            var playersRoot = XmlHelper.CreateChild(root, "Players");

            foreach (var player in players.Where(p => p.HasCreatures))
            {
                var playerNode = XmlHelper.CreateChild(playersRoot, "Player");
                player.Save(playerNode);
            }
        }

        private void SaveGameObjects(XElement root, List<Actor> actors)
        {

            //var gameObjectsRoot = XmlHelper.CreateChild(root, "GameObjects");
            //foreach (var creature in actors.Where(p => !(p is CreatureActor) && p.Saveable))
            //{
            //    var creatureNode = XmlHelper.CreateChild(gameObjectsRoot, "GameObject");
            //    creature.Save(creatureNode);
            //}

        }


        private void SaveTileMap(Level level)
        {
            var tileMap = level.TileMap;

            using (var stream = new FileStream("map.data", FileMode.Create))
            {
                tileMap.Save(stream);
            }
        }

        public void Load()
        {
            XElement root = XElement.Load("savedata.xml");

            LoadLevel(root);
            LoadGameObjects(root);
            LoadPlayers(root);
        }

        private void LoadPlayers(XElement root)
        {
            {
                var playersRoot = XmlHelper.GetChild(root, "Players");
                var players = XmlHelper.GetChildren(playersRoot, "Player");
                foreach (var playerNode in players)
                {
                    var player = XmlHelper.ReadClass<FightPlayer>(playerNode);
                    player.Load(playerNode);

//                    World.I.TravelManager.AddPlayer(player);
//                    if (player is HumanFightPlayer)
//                    {
//                        World.I.TravelManager.SetMainPlayer(player);
//                    }
                }
            }
        }

        private void LoadGameObjects(XElement root)
        {
            //var gameObjectsRoot = XmlHelper.GetChild(root, "GameObjects");
            //foreach (var actorNode in XmlHelper.GetChildren(gameObjectsRoot, "GameObject"))
            //{
            //    var creature = XmlHelper.ReadClass<Actor>(actorNode);
            //    creature.Load(actorNode);
            //}
        }

        private void LoadLevel(XElement root)
        {
            var tileMap = new TileMap();

            using (var stream = new FileStream("map.data", FileMode.Open))
            {
                tileMap.Load(stream);
            }

            Level level = new Level(root, tileMap);
            //World.I.GoTo(level);
        }
    }

    public interface ISaveable
    {
        void Save(XElement node);
        void Load(XElement node);
    }
}
