﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameName1.Common
{
  public class Singleton<T> where T:class, new()
  {
    public static T I {
      get
      {
        if (_i == null)
        {
          _i = new T();
        }
        return _i;
      }
    }
    private static T _i;
  }
}
