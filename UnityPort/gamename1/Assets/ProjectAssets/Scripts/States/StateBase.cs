﻿using System.Collections.Generic;
using UnityEngine;

public abstract class StateBase
{
    private readonly string _stateSceneName;

    public StateBase(string stateSceneName = "")
    {
        _stateSceneName = stateSceneName;
    }

    public void Activate()
    {
        OnActivate();
    }

    public void Deactivate()
    {
        OnDeactivate();
    }

    protected virtual void OnActivate()
    {
    }

    protected virtual void OnDeactivate()
    {
    }

    public virtual void Update()
    {
    }

    public virtual void OnSceneLoaded()
    {

    }


    public virtual void OnApplicationPause(bool pauseStatus)
    {
        Debug.Log("Pause status: " + pauseStatus);
    }

    public virtual void OnApplicationQuit()
    {
    }

    public virtual string StateSceneName
    {
        get { return _stateSceneName; }
    }
}