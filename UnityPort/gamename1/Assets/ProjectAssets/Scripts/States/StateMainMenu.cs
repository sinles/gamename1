﻿using GameName1.UI;
using UnityEngine;

public class StateMainMenu : StateBase
{
    public StateMainMenu()
        : base("MainMenu")
    {
    }


    protected override void OnActivate()
    {
        base.OnActivate();
    }

    public override void Update()
    {
        base.Update();
    }
}
