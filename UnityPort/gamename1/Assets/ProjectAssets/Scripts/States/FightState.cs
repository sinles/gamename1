﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using GameName1.Actors;
using GameName1.Data;
using GameName1.Extensions;
using GameName1.Fight;
using GameName1.Levels;
using GameName1.Lua;
using GameName1.Travel;
using GameName1.UI;
using Glide;
using UnityEngine;

public class FightState : StateBase
{
  public event Action PlayerStepStarted;
  public FightManager FightManager { get { return _fightManager; } }
  public World World{ get { return _world; } }
  public List<FightPlayer> HumanFightPlayers { get { return _humanFightPlayers; } }

  private World _world;

  private readonly FightInfo _fightInfo;
  private FightPlayer _otherBattleFightPlayer;
  private FightManager _fightManager;
  private List<FightPlayer> _humanFightPlayers;

  private bool _gameFinished;

  public FightState(FightInfo fightInfo)
    : base("fightScene")
  {
    _fightInfo = fightInfo;
  }

  protected override void OnActivate()
  {
    base.OnActivate();

    var level = AppRoot.I.LuaLoader.LevelLoader.GetLevel(_fightInfo.LevelName);
    Sprites.LoadSpriteAtlas("forestTileMap");

    _fightManager = new FightManager();

    _world = new World();
    _world.GoTo(new Level(level, "forestTileMap"));

    InitFightManager();
    InitViews();

    _fightManager.Start();
  }

  private void InitViews()
  {
//todo
    GameObject
      .FindObjectOfType<FightStateView>()
      .SetState(this);
    GameObject
      .FindObjectOfType<SkillView>()
      .SetState(this);
  }

  private void CreateOtherPlayer()
  {
    _otherBattleFightPlayer = new LuaAIPlayer(AppRoot.I.LuaLoader.AiLoader, _fightManager, "aiSimple");
    _otherBattleFightPlayer.Side = 1;

    var creaturesActors = ConvertCreatureInfosToActors(_fightInfo.Creatures.Select(unitData => new CreatureInfo(unitData)).ToList());
    _otherBattleFightPlayer.SetCreatures(creaturesActors);
    SetCreaturesStartPositions(_otherBattleFightPlayer);
    _otherBattleFightPlayer.ActivateCreatures(true);
  }

  private List<CreatureActor> ConvertCreatureInfosToActors(List<CreatureInfo> units)
  {
    var actors=  units.Select(unit => new CreatureActor(_world.TileMap, unit, 0)).ToList();

    foreach (var creatureActor in actors)
    {
      creatureActor.InitSkills(AppRoot.I.LuaLoader.SkillLoader);
      _world.AddTileActor(creatureActor);
    }

    return actors;
  }

  private void CreatePlayers()
  {
    _humanFightPlayers = new List<FightPlayer>();

    foreach (var profile in PlayerProfileManager.I.GetAll())
    {
      var aiType = profile.Local ? "aiHumanPlayer" : "aiHumanDummy";

      var humanPlayer = new LuaAIPlayer(AppRoot.I.LuaLoader.AiLoader, _fightManager, aiType);

      if (profile.Local)
      {
        humanPlayer.MarkLocal();
      }

      humanPlayer.StepStarted += OnPlayerStepStarted;

      var creaturesActors = ConvertCreatureInfosToActors(profile.Creatures);
      humanPlayer.SetCreatures(creaturesActors);

      SetCreaturesStartPositions(humanPlayer);

      humanPlayer.ActivateCreatures(true);

      _humanFightPlayers.Add(humanPlayer);
    }
  }

  private void SetCreaturesStartPositions(FightPlayer player)
  {
    Point centerTile = _world.TileMap.GetFreeRandomPoint();
    Point tile = centerTile;
    Vector2 tileVec = tile.ToVector();

    foreach (var creature in player.AliveCreatures)
    {
      while (!_world.TileMap.IsFree(tile) || _world.TileMap.GetCreatureFromTile(tile) != null)
      {
        tileVec += RandomTool.D.NextUnitVector2()*RandomTool.D.NextSingle(0, 3f);
        tile = tileVec.ToSizeRound();

        if (tile.X > _world.TileMap.Width)
        {
          tile.X = _world.TileMap.Width;
          tileVec.x = tile.X;
        }

        if (tile.Y > _world.TileMap.Height)
        {
          tile.Y = _world.TileMap.Height;
          tileVec.y = tile.Y;
        }

        if (tile.X < 0)
        {
          tile.X = 0;
          tileVec.x = tile.X;
        }
        if (tile.Y < 0)
        {
          tile.Y = 0;
          tileVec.y = tile.Y;
        }
      }

      creature.SetTile(tile, true);
    }
  }

  private void InitFightManager()
  {
    CreatePlayers();
    CreateOtherPlayer();


    var players = new List<FightPlayer>();
    players.AddRange(_humanFightPlayers);
    players.Add(_otherBattleFightPlayer);

    _fightManager.SetPlayers(players);
  }

  public override void Update()
  {
    if (PopupTextThrower.I != null)
      PopupTextThrower.I.Update();

    if (_gameFinished)
      return;

    base.Update();



    if (_fightManager == null)
    {
      return;
    }


    _fightManager.Update();
    _world.Update();


    if (_fightManager.FightEnded)
    {
      _fightManager.Deinit();

      if (_fightManager.MainPlayerLost)
      {
        //AppRoot.I.SetState(new LostState(StateManager));
      }
      else
      {
        AppRoot.I.StartCoroutine(GoToEventChoice());
      }
      _gameFinished = true;

     // AppRoot.I.LuaLoader.CleanCache();
    }

  }

  private static IEnumerator GoToEventChoice()
  {
    yield return new WaitForSeconds(1.5f);
    AppRoot.I.SetState(new EventChoiceState());
  }

  private void OnPlayerStepStarted()
  {
    if (PlayerStepStarted != null)
      PlayerStepStarted();
  }
}