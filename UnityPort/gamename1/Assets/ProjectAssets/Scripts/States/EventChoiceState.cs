﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameName1.Data;
using GameName1.UI;
using UnityEngine;
using Event = GameName1.Data.Event;

public class EventChoiceState:StateBase
{
  private Event _travelEvent;

  public Event TravelEvent
  {
    get { return _travelEvent; }
    private set
    {
      _travelEvent = value;

      if (TravelEventChanged != null)
        TravelEventChanged();
    }
  }

  public event Action TravelEventChanged;

  public EventChoiceState(Event travelEvent = null)
    : base("eventChoice")
  {
    TravelEvent = travelEvent;
    if (TravelEvent == null)
    {
      GetRandomEvent();
    }
  }

  protected override void OnActivate()
  {
    base.OnActivate();
    InitViews();
  }

  private void InitViews()
  {
    //todo
    GameObject
      .FindObjectOfType<EventStartedUIControl>()
      .SetState(this);
  }

  public void MakeChoice(EventChoice eventChoice)
  {
    var outcome = RandomTool.D.NextWeightedChoice(
      eventChoice.Outcomes.Keys.ToList(),
      eventChoice.Outcomes.Values.ToList());

    if(!string.IsNullOrEmpty(outcome.EventId))
    {
      TravelEvent = GetTravelEvent(outcome.EventId);
      return;
    }

    if (outcome.IsFight)
    {
      AppRoot.I.SetState(new FightState(outcome.Fight));
      return;
    }

    GetRandomEvent();
  }

  private Event GetTravelEvent(string name)
  {
    return AppRoot.I.LuaLoader.EventsLoader.GetEvent(name);
  }

  private void GetRandomEvent()
  {
    var eventsLoader = AppRoot.I.LuaLoader.EventsLoader;
    var available = eventsLoader.SelectAvailable(2);
    var resultEvent = RandomTool.D.NextChoice(available);

    TravelEvent = GetTravelEvent(resultEvent);
  }
}
