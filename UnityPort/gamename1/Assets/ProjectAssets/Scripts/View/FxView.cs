﻿using GameName1.Actors;
using GameName1.Levels;
using GameObjectExtensions;
using UnityEngine;

namespace GameName1.View
{
  public class FxView: ActorView<FxActor>
  {
    private GameObject _subview;

    protected override void OnAdded(FxActor actor)
    {
      _subview = Resources.Load<GameObject>("prefabs/" + actor.View).Create();
      _subview.gameObject.transform.SetParent(gameObject.transform, false);
    }
  }
}
