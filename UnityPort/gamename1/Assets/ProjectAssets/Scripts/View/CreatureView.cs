﻿using GameName1;
using UnityEngine;
using GameName1.Actors;
using GameName1.Items;
using GameName1.UI;
using UnityEngine.Assertions;

namespace GameName1.View
{
  public class CreatureView : ActorView<CreatureActor>
  {
    [SerializeField] private SpriteRenderer _sprite;

    [SerializeField] private HealthBar _healthBar;

    private CreatureActor _actor;

    protected override void OnAdded(CreatureActor actor)
    {
      Assert.IsNotNull(actor);
      Assert.IsNotNull(actor.Info.Data.Sprite);
      Assert.IsNotNull(_sprite);
      Assert.IsNotNull(_healthBar);

      _actor = actor;
      _actor.HealthChanged += ActorOnHealthChanged;

      var sprite = ResourceManager.LoadSprite(actor.Info.Data.Sprite);
      Assert.IsNotNull(sprite);

      _sprite.sprite = sprite;

      ActorOnHealthChanged(0);
    }

    private void ActorOnHealthChanged(float change)
    {
      _healthBar.SetValue(_actor.Health, _actor.Info.Data.MaxHealth);

      if (change < 0)
      {
        PopupTextThrower.I.CreatePopUp(change.ToString("F0"), new Color(1, 0, 0, 1),
          new Color(1, 0, 0, 0), _actor.Position, RandomTool.U.NextUnitVector2(), 0.01f,
          0.0001f, 3f, 1f);

        CameraControl.I.RumbleAdditive(0.1f, 0.02f, 0.001f, 10);
        AudioManager.I.PlayWeaponHit(WeaponType.Sword);
      }
    }
  }
}
