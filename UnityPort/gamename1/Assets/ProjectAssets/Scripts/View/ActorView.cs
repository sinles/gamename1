﻿using GameName1.Actors;
using UnityEngine;

namespace GameName1.View
{
  public interface IActorView
  {
    void Remove();
  }

  public abstract class ActorView<T>:MonoBehaviour, IActorView where T:Actor
  {
    private T _actor;

    public void Set(T actor)
    {
      _actor = actor;
      _actor.PositionChanged += ActorOnPositionChanged;
      ActorOnPositionChanged();

      OnAdded(actor);
    }

    public void Remove()
    {
      OnRemoved();
      Destroy(gameObject);
      _actor = null;
    }

    protected virtual void OnAdded(T actor)
    {
      
    }

    protected virtual void OnRemoved()
    {

    }

    protected virtual void ActorOnPositionChanged()
    {
      transform.position = _actor.Position;
    }

    protected T Actor { get { return _actor; } }
  }
}
