﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameName1.Actors;
using GameObjectExtensions;
using UnityEngine;

namespace GameName1.View
{
  public class ActorBinder : MonoBehaviour
  {
    [SerializeField]
    private CreatureView _creaturePrefab;

    [SerializeField]
    private ProjectileView _projectile;

    [SerializeField]
    private FxView _fx;

    private readonly Dictionary<Type, Action<Actor>> _bindings = new Dictionary<Type, Action<Actor>>();
    private readonly Dictionary<Actor, IActorView> _actorViews = new Dictionary<Actor, IActorView>();

    private World _world;

    void Awake()
    {
      Bind<CreatureActor, CreatureView>(_creaturePrefab);
      Bind<ProjectileActor, ProjectileView>(_projectile);
      Bind<FxActor, FxView>(_fx);
    }

    public void SetWorld(World world)
    {
      _world = world;
      _world.ActorManager.ActorAdded += ActorAdded;
      _world.ActorManager.ActorRemoved += ActorRemoved;

      AddExistingActors(world);
    }

    private void ActorRemoved(Actor obj)
    {
      _actorViews[obj].Remove();
      _actorViews.Remove(obj);
    }

    private void Bind<T, T2>(T2 view) 
      where T : Actor 
      where T2 : ActorView<T> 
    {
      if (view == null)
      {
        Debug.LogError("NULL VIEW " + typeof(T2).Name);
      }

      _bindings[typeof (T)] = (a) =>
      {
        var resultView = view.gameObject.Create<T2>();
        resultView.Set(a as T);
        _actorViews[a] = resultView;
      };
    }

    private void AddExistingActors(World world)
    {
      foreach (var actor in world.ActorManager.Actors)
      {
        ActorAdded(actor);
      }
    }

    private void ActorAdded(Actor actor)
    {
      _bindings[actor.GetType()](actor);
    }
  }
}
