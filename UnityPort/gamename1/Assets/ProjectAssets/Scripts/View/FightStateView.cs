﻿using System.Collections.Generic;
using System.Linq;
using GameName1.Actors;
using GameName1.Fight;
using GameName1.Levels;
using GameName1.UI;
using GameName1.View;
using Glide;
using SLua;
using UnityEngine;

public class FightStateView : UIView
{
  private MovementGrid _movementGrid;

  private FightState _state;

  private CreatureActor _currentCreature;
  private SelectionCursor _cursor;

  private readonly List<SelectionCursor> _skillCursors = new List<SelectionCursor>();

  public void SetState(FightState state)
  {
    _state = state;
    _state.PlayerStepStarted += OnPlayerStepStarted;

    foreach (var player in state.FightManager.Players)
    {
      player.CreatureStepStarted += PlayerOnCreatureStepStarted;
    }

    GameObject
      .FindObjectOfType<RTSCameraControl>()
      .ClampRect = new Rect(
        0,
        0,
        TileHelper.TileSize*_state.World.TileMap.Width*1f,
        TileHelper.TileSize*_state.World.TileMap.Height*1f);

    PopupTextThrower.Init();

    GameObject
      .FindObjectOfType<ActorBinder>()
      .SetWorld(state.World);

    _movementGrid = new MovementGrid(state.World.TileMap);

    _cursor= new SelectionCursor();
    _cursor.Show();
  }

  void Update()
  {
    UpdateMovementGrid();
    UpdateCursor();
    UpdateCursorSkills();
  }

  private void UpdateMovementGrid()
  {
    if (_movementGrid == null) return;
    if (_currentCreature == null || !IsCurrentCreatureLocal())
    {
      _movementGrid.HideMovementGrid();
      return;
    }

    if (_currentCreature.ActionRunning)
    {
      _movementGrid.HideMovementGrid();
    }
    else
    {
      _movementGrid.Update();
    }
  }

  private void UpdateCursor()
  {
    var tileUnderMouse = TileHelper.GetTileUnderMouse();
    var tile = TileHelper.TileToPosition(tileUnderMouse);
    _cursor.Update(tile);
  }

  private void UpdateCursorSkills()
  {
    if (_currentCreature == null || 
      _currentCreature.Abilities.SelectedSkill == null ||
      !IsCurrentCreatureLocal())
    {
      foreach (var cursor in _skillCursors)
      {
        cursor.Hide();
      }
      return;
    }

    var tileUnderMouse = TileHelper.GetTileUnderMouse();
    var param = new LuaTable(AppRoot.I.LuaLoader.State);
    param["targetTile"] = tileUnderMouse;

    bool inRange = _currentCreature.Abilities.SelectedSkill.IsInRange(tileUnderMouse);
    var color = inRange ? Color.green : Color.red;

    var tiles = _currentCreature.Abilities.SelectedSkill.GetTargetTiles(param);

    for (var i = 0; i < tiles.Count; i++)
    {
      var point = tiles[i];

      if (_skillCursors.Count == i)
      {
        _skillCursors.Add(new SelectionCursor());
      }

      var skillCursor = _skillCursors[i];
      skillCursor.Show();

      var tile = TileHelper.TileToPosition(point);
      skillCursor.Update(tile, color);
    }
    for (var i = tiles.Count; i < _skillCursors.Count; i++)
    {
      _skillCursors[i].Hide();
    }
  }

  private void PlayerOnCreatureStepStarted(CreatureActor creature)
  {
    _currentCreature = creature;
    if (_movementGrid != null)
      _movementGrid.Show(creature);

    RTSCameraControl.I.enabled = false;
    CameraControl.I.Target = RTSCameraControl.I.transform;

    Tweener.I
      .Tween(RTSCameraControl.I.transform, new { position = (Vector3)_state.FightManager.CurrentCreatureActor.Position }, 0.5f)
      .Ease(Ease.CubeOut)
      .OnComplete(() =>
      {
        RTSCameraControl.I.enabled = true;
        RTSCameraControl.I.transform.position = RTSCameraControl.I.transform.position;
        CameraControl.I.Target = RTSCameraControl.I.transform;
      });
  }

  private void OnPlayerStepStarted()
  {
  }

  private bool IsCurrentCreatureLocal()
  {
    return _state.HumanFightPlayers.Any(p => 
    p.IsLocal &&
    p.AliveCreatures.Contains(_currentCreature));
  }
}
