﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GameName1.Items;
using GameName1.Skills;
using GameName1.StatusEffects;




public class GameData : MonoBehaviour
{
    public static GameData I { get; private set; }

    public void Awake()
    {
        I = this;
    }

    public TileData TileData;
}


[Serializable]
public class TileData
{
    public GameObject TilePrefab;
    public GameObject AvailableTile;
}