﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GameName1.Network
{
  public class Dispatcher
  {
    private static readonly List<Action> _pending = new List<Action>();

    public static void Invoke(Action action)
    {
      lock (_pending)
      {
        _pending.Add(action);
      }
    }

    public static void InvokePending()
    {
      List<Action> pending;

      lock (_pending)
      {
        pending = _pending.ToList();
        _pending.Clear();
      }

      foreach (var action in pending)
      {
        action();
      }
    }

    public static void ClearPending()
    {
      lock (_pending)
      {
        _pending.Clear();
      }
    }
  }
}
