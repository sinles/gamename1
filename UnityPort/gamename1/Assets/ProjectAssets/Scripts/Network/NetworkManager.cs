﻿using System;
using GameName1.Common;
using GameName1.Network.Commands;
using UnityEngine;

namespace GameName1.Network
{
  public class NetworkManager:Singleton<NetworkManager>
  {
    public event Action Connected;

    public bool IsServer { get { return _current is NetworkServer; } }
    public bool IsClient { get { return _current is NetworkClient; } }

    private INetworkBase _current;

    public void Create()
    {
      _current = new NetworkServer();
      Launch();
    }

    public void Join(string host)
    {
      _current = new NetworkClient(host);
      Launch();
    }

    public void SendCmd(GameCmd cmd)
    {
      if (_current != null)
      {
        _current.SendCmd(cmd);
      }
      else
      {
        Debug.LogError("NOT STARTED");
      }
    }

    public void Stop()
    {
      if (_current != null)
      {
        _current.Stop();
      }
    }

    private void Launch()
    {
      _current.Connected += CurrentOnConnected;
      _current.Launch();
    }

    private void CurrentOnConnected()
    {
      _current.Connected -= CurrentOnConnected;

      if (Connected != null)
      {
        Connected();
      }
    }
  }
}
