﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace GameName1.Network
{
  public class Serializer
  {
    public static string SerializeObject<T>(T o)
    {
      if (!o.GetType().IsSerializable)
      {
        Debug.LogError("Can not serialize, because it is not marked serializable! "+ o.GetType().Name);
        return null;
      }

      using (MemoryStream stream = new MemoryStream())
      {
        new BinaryFormatter().Serialize(stream, o);
        return Convert.ToBase64String(stream.ToArray());
      }
    }

    public static T DeserializeObject<T>(string str) where T:class
    {
      byte[] bytes = Convert.FromBase64String(str);

      using (MemoryStream stream = new MemoryStream(bytes))
      {
        var bf = new BinaryFormatter();
        var result = bf.Deserialize(stream);
        return result as T;
      }
    }
  }
}
