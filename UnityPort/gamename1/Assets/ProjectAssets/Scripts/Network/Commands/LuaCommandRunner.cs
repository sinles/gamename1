﻿using Assets.ProjectAssets.Scripts.Network.Commands;
using SLua;

namespace GameName1.Network.Commands
{
  [CustomLuaClass]
  public static class LuaCommandRunner
  {
    [StaticExport]
    public static void MoveCmd(MoveCreatureCmd.Input input)
    {
      new MoveCreatureCmd(input).Execute();
    }

    [StaticExport]
    public static void UseSkillCmd(UseSkillCmd.Input input)
    {
      new UseSkillCmd(input).Execute();
    }
  }
}
