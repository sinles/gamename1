﻿using System;
using GameName1.Actors;
using GameName1.Travel;
using UnityEngine;

namespace GameName1.Network.Commands
{
  public class CreatePlayerProfileCmd:GameCmd<CreatePlayerProfileCmd.Input>
  {
    private readonly bool _local;

    [Serializable]
    public class Input
    {
      public string Nickname;
      public long Id;
    }

    public CreatePlayerProfileCmd(Input data) 
      : base(data)
    {
    }

    public CreatePlayerProfileCmd(Input data, bool local)
      : base(data)
    {
      _local = local;
    }

    public override void Apply()
    {
      var profile = new PlayerProfile(Data.Id, Data.Nickname, _local);

      var creature = new CreatureInfo
      {
        Data =
        {
          MaxHealth = 15,
          MaxStamina = 100,
          StaminaRestoring = 10,
          AttackRange = 1,
          MaxActionPoints = 4,
          Damage = 2,
          CritChance = 0.05f,
          Sprite = "HumanWarrior"
        }
      };
      creature.Data.SkillIds.Add("fireball");
      creature.Data.SkillIds.Add("slash");

      //var skills = creature.Abilities.GetAvailableSkills();
      //creature.Abilities.SelectSkills(skills.FirstOrDefault(), 0);
      profile.AddCreature(creature);

      creature = new CreatureInfo
      {
        Data =
        {
          MaxHealth = 10,
          MaxStamina = 80,
          StaminaRestoring = 20,
          AttackRange = 3,
          Damage = 3,
          CritChance = 0.3f,
          MaxActionPoints = 5,
          Sprite = "HumanArcher"
        }
      };
      creature.Data.SkillIds.Add("shootArrow");
      profile.AddCreature(creature);

      PlayerProfileManager.I.Add(profile);
    }
  }
}
