﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameName1.Network.Commands;
using UnityEngine;

namespace Assets.ProjectAssets.Scripts.Network.Commands
{
  class HelloWorldCmd:GameCmd<HelloWorldCmd.Input>
  {
    [Serializable]
    public class Input
    {
      public string Hello;
    }

    public HelloWorldCmd(Input data) 
      : base(data)
    {
    }

    public override void Apply()
    {
      Debug.LogError("HELLOE + " + Data.Hello);
    }
  }
}
