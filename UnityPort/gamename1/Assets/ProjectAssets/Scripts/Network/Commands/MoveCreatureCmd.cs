﻿using System;
using GameName1.Actors;
using SLua;
using UnityEngine;

namespace GameName1.Network.Commands
{
  [CustomLuaClass]
  public class MoveCreatureCmd:GameCmd<MoveCreatureCmd.Input>
  {
    [Serializable]
    [CustomLuaClass]
    public class Input
    {
      public int CreatureId;
      public Point Target;
    }

    public MoveCreatureCmd(Input data) 
      : base(data)
    {
    }

    public override void Apply()
    {
      CreatureActor creature = ActorManager.I.GetById<CreatureActor>(Data.CreatureId);
      if (creature != null)
      {
        if (creature.Move(Data.Target.X, Data.Target.Y))
        {

        }
      }
    }
  }
}
