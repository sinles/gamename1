﻿using System;
using GameName1.Actors;
using GameName1.Network.Commands;
using SLua;
using UnityEngine;

namespace GameName1.Network.Commands
{
  public class UseSkillCmd :GameCmd<UseSkillCmd.Input>
  {
    [Serializable]
    [CustomLuaClass]
    public class Input
    {
      public int CreatureId;
      public string SkillId;
      private Point? _target;//slua problem with compilation try later

      public void SetTarget(Point target)
      {
        _target = target;
      }

      public Point? GetTarget()
      {
        return _target;
      }
    }

    public UseSkillCmd(Input data) 
      : base(data)
    {
    }

    public override void Apply()
    {
      if (Data.SkillId == null)
      {
        Debug.LogError("No skill id");
      }

      var creature = ActorManager.I.GetById<CreatureActor>(Data.CreatureId);
      if (creature != null)
      {
        var data = AppRoot.I.GetLuaTable();

        if(Data.GetTarget() != null)
          data["targetTile"] = Data.GetTarget().Value;

        if (creature.UseSkill(Data.SkillId, data))
        {

        }
      }
    }
  }
}
