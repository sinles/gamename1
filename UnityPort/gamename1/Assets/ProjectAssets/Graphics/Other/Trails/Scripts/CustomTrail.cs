using UnityEngine;
using System.Collections;
using System;

public class CustomTrail : MonoBehaviour {

    private Vector3[] prevPosBuffer;        // pos
    private Vector3[] nextPosBuffer;        // normal
    private Vector4[] currPosBuffer;        // tan
    private Vector2[] offsetAndTimeBuffer;  // uv

    private int[] indices;
    public int bufferSize = 100;

    public Texture2D texture;

    public float startWidth;
    public float endWidth;
    public float minDistance;
    public bool strictDistance;
    public bool keepCloseToEmiter;
    public float lifetime;
    public Gradient colorOverLifetime;

    int headPointer;
    int activeCount;
    int textureTilingOffset;
    Vector3 lastPosition;

    private GameObject trailObject;
    private Material material;
    private Mesh mesh;

	// Use this for initialization
	void Start () {
        int vertices = bufferSize * 2; // each trail point is 2 vertices in buffer
        prevPosBuffer = new Vector3[vertices];
        nextPosBuffer = new Vector3[vertices];
        currPosBuffer = new Vector4[vertices];
        offsetAndTimeBuffer = new Vector2[vertices];

        CreateObject();
        lastPosition = transform.position;
	}

    void CreateObject()
    {
        if (!mesh)
        {
            mesh = new Mesh();
        }
        if (!trailObject)
        {
            trailObject = new GameObject("Trail");
            trailObject.transform.parent = null;
            trailObject.transform.position = Vector3.zero;
            trailObject.transform.rotation = Quaternion.identity;
            trailObject.transform.localScale = Vector3.one;
            MeshFilter meshFilter = trailObject.AddComponent<MeshFilter>();
            meshFilter.mesh = mesh;
            trailObject.AddComponent<MeshRenderer>();
            material = CheckShaderAndCreateMaterial(Shader.Find("Custom/TrailShader"), material);
            trailObject.GetComponent<Renderer>().material = material;
        }
    }

    protected Material CheckShaderAndCreateMaterial(Shader s, Material m2Create)
    {
        if (!s)
        {
            Debug.Log("Missing shader in " + ToString());
            enabled = false;
            return null;
        }

        if (s.isSupported && m2Create && m2Create.shader == s)
            return m2Create;

        if (!s.isSupported)
        {
            enabled = false;
            Debug.Log("The shader " + s.ToString() + " on trail " + ToString() + " is not supported on this platform!");
            return null;
        }
        else
        {
            m2Create = new Material(s);
            m2Create.hideFlags = HideFlags.DontSave;
            if (m2Create)
                return m2Create;
            else return null;
        }
    }
	
    void UpdateParams()
    {
        if (colorOverLifetime.colorKeys.Length != 2)
        {
            Debug.LogError("Trail support only 2 color keys in gradient. Its important!");
        }
        if (colorOverLifetime.alphaKeys.Length != 2)
        {
            Debug.LogError("Trail support only 2 alpha keys in gradient. Its important!");
        }
        material.SetTexture("_Texture", texture);

        material.SetFloat("_FadeTime", lifetime);
        material.SetFloat("_StartWidth", startWidth);
        material.SetFloat("_EndWidth", endWidth);
        GradientColorKey startColorKey = colorOverLifetime.colorKeys[0].time < colorOverLifetime.colorKeys[1].time ? colorOverLifetime.colorKeys[0] : colorOverLifetime.colorKeys[1];
        GradientColorKey endColorKey = colorOverLifetime.colorKeys[0].time >= colorOverLifetime.colorKeys[1].time ? colorOverLifetime.colorKeys[0] : colorOverLifetime.colorKeys[1];
        GradientAlphaKey startAlphaKey = colorOverLifetime.alphaKeys[0].time < colorOverLifetime.alphaKeys[1].time ? colorOverLifetime.alphaKeys[0] : colorOverLifetime.alphaKeys[1];
        GradientAlphaKey endAlphaKey = colorOverLifetime.alphaKeys[0].time >= colorOverLifetime.alphaKeys[1].time ? colorOverLifetime.alphaKeys[0] : colorOverLifetime.alphaKeys[1];
        
        material.SetColor("_startColor", startColorKey.color);
        material.SetFloat("_startColorTime", startColorKey.time);
        material.SetColor("_endColor", endColorKey.color);
        material.SetFloat("_endColorTime", endColorKey.time);

        material.SetFloat("_startAlpha", startAlphaKey.alpha);
        material.SetFloat("_startAlphaTime", startAlphaKey.time);
        material.SetFloat("_endAlpha", endAlphaKey.alpha);
        material.SetFloat("_endAlphaTime", endAlphaKey.time);
    }

	// Update is called once per frame
	void Update () {

        if(!trailObject)
        {
            CreateObject();
        }
        
        UpdateParams();

        // if we have moved enough, create a new vertex and make sure we rebuild the mesh
        float theDistance = (lastPosition - transform.position).magnitude;
        Vector3 direction = (transform.position - lastPosition).normalized;
	    if( theDistance >= minDistance)
        {
            if (strictDistance)
            {
                int steps = (int)Math.Floor(theDistance / minDistance);
                for (int i = 0; i < steps; i++)
                {
                    Step(lastPosition + direction * minDistance, direction);
                }
                
            }
            else
            {
                Step(transform.position, direction);
            }
        }
        // if we moved, update last position and rebuild mesh
        else if( theDistance > 0 )
        {
            Keep(transform.position, direction);
        }

        UpdateLifetime();
        RebuildVertices();
        RebuildIndices();
	}

    void OnDestroy()
    {
        if (trailObject)
        {
            Destroy(trailObject);
        }
        if( mesh )
        {
            DestroyImmediate(mesh);
        }
        if(material)
        {
            DestroyImmediate(material);
        }
    }

    void RebuildVertices()
    {
        mesh.Clear();
        mesh.vertices = prevPosBuffer;
        mesh.normals = nextPosBuffer;
        mesh.tangents = currPosBuffer;
        mesh.uv = offsetAndTimeBuffer;
        
    }

    void RebuildIndices()
    {
        indices = new int[Math.Max(activeCount - 1, 0) * 6];
        int idx = 0;
        if (activeCount > 1)
        {
            int tailVertex = ((headPointer - (activeCount-1)) + bufferSize) % bufferSize;
            for (int i = tailVertex; i != headPointer; i = (i + 1) % bufferSize)
            {
                int leftVertexIdx = i * 2;
                int rightVertexIdx = i * 2 + 1;
                int nextLeftVertexIdx = ((i + 1) % bufferSize) * 2;
                int nextRightVertexIdx = ((i + 1) % bufferSize) * 2 + 1;
                indices[idx++] = leftVertexIdx;
                indices[idx++] = nextLeftVertexIdx;
                indices[idx++] = rightVertexIdx;

                indices[idx++] = rightVertexIdx;
                indices[idx++] = nextLeftVertexIdx;
                indices[idx++] = nextRightVertexIdx;
            }
        }
        mesh.triangles = indices;
    }

    void UpdateLifetime()
    {
        int tailVertex = ((headPointer - (activeCount-1)) + bufferSize)%bufferSize;

        bool aliveFound = false;
        do
        {
            if(activeCount == 0)
            {
                break;
            }
            if ((Time.timeSinceLevelLoad - offsetAndTimeBuffer[tailVertex * 2].y) > lifetime)
            {
                activeCount -= 1;
                tailVertex = (tailVertex + 1) % bufferSize;
            }
            else
            {
                aliveFound = true;
            }
        } while (!aliveFound);
    }

    void Keep(Vector3 position, Vector3 direction)
    {
        if (keepCloseToEmiter)
        {
            if (activeCount > 1)
            {
                AddVertex(headPointer, position, direction, false);
                LinkVertices((bufferSize + headPointer - 1) % bufferSize, headPointer);
            }
        }
    }

    bool Step(Vector3 position, Vector3 direction)
    {
        bool result = false;
        if( lastPosition != position )
        {
            lastPosition = position;
            if(!IsFull())
            {
                AddVertex(MoveNextFree(), position, direction, true);
                result = true;
                if( activeCount > 1 )
                {
                    LinkVertices((bufferSize + headPointer - 1) % bufferSize, headPointer);
                }
            }
        }
        return result;
    }

    bool IsFull()
    {
        return activeCount >= bufferSize;
    }

    bool IsEmpty()
    {
        return activeCount > 0;
    }

    int NextFree()
    {
        if( activeCount == 0)
        {
            return 0;
        }
        else if( activeCount < bufferSize)
        {
            return (headPointer + 1) % bufferSize;
        }
        return -1;
    }

    int MoveNextFree()
    {
        int next = NextFree();
        if(next != -1)
        {
            headPointer = next;
            activeCount += 1;
        }
        return next;
    }

    void AddVertex(int index, Vector3 pos, Vector3 dir, bool updateTimeAndOffset)
    {
        int indexLeft = index * 2;
        int indexRight = index * 2 + 1;

        prevPosBuffer[indexLeft] = prevPosBuffer[indexRight] = (pos - dir);
        
        currPosBuffer[indexLeft] = new Vector4(pos.x, pos.y, pos.z, -1);
        currPosBuffer[indexRight] = new Vector4(pos.x, pos.y, pos.z, 1);

        nextPosBuffer[indexLeft] = nextPosBuffer[indexRight] = (pos + dir);
        
        if (updateTimeAndOffset)
        {
            offsetAndTimeBuffer[indexLeft] = new Vector2(textureTilingOffset, Time.timeSinceLevelLoad);
            offsetAndTimeBuffer[indexRight] = new Vector2(textureTilingOffset, Time.timeSinceLevelLoad);

            textureTilingOffset++;
        }
    }

    void LinkVertices(int prev, int next)
    {
        int prevIndex = prev * 2;
        int nextIndex = next * 2;
        int i = 1;
        do
        {
            prevPosBuffer[nextIndex] = currPosBuffer[prevIndex];
            nextPosBuffer[prevIndex] = currPosBuffer[nextIndex];
            prevIndex++;
            nextIndex++;

        } while(i++ < 2);
    }
}
