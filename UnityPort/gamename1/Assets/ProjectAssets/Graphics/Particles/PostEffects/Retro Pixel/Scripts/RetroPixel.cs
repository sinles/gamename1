﻿using UnityEngine;
using System.Collections;

namespace AlpacaSound
{
	[ExecuteInEditMode]
	[RequireComponent (typeof(Camera))]
	[AddComponentMenu("Image Effects/Custom/Retro Pixel")]
	public class RetroPixel : MonoBehaviour
	{
		public static readonly int MAX_NUM_COLORS = 8;

		public int horizontalResolution = 160;
		public int verticalResolution = 200;



		Shader[] shaders = new Shader[MAX_NUM_COLORS];

		Material m_material;
		Material material
		{
			get
			{
				if (m_material == null)
				{
					for (int i = 1; i < MAX_NUM_COLORS; ++i)
					{
						string shaderName = "AlpacaSound/RetroPixel" + (i+1);
						Shader shader = Shader.Find (shaderName);

						if (shader == null)
						{
							Debug.LogError ("Shader \'" + shaderName + "\' not found. Was it deleted?");
							enabled = false;
							return null;
						}
						
						shaders[i] = shader;
					}
					
					m_material = new Material (shaders[1]);
					m_material.hideFlags = HideFlags.DontSave;
				}
				return m_material;
			} 
		}


	    public Material FinalMaterial;



		void Start ()
		{
			if (!SystemInfo.supportsImageEffects)
			{
				enabled = false;
				return;
			}
		}
		
		public void OnRenderImage (RenderTexture src, RenderTexture dest)
		{
			horizontalResolution = Mathf.Clamp(horizontalResolution, 1, 2048);
			verticalResolution = Mathf.Clamp(verticalResolution, 1, 2048);

			if (material)
			{
					material.shader = shaders[1];
				
				
				RenderTexture scaled = RenderTexture.GetTemporary (horizontalResolution, verticalResolution);
				scaled.filterMode = FilterMode.Point;
				Graphics.Blit (src, scaled, material);
                FinalMaterial.SetFloat("_ResolutionX", horizontalResolution);
                FinalMaterial.SetFloat("_ResolutionY", verticalResolution);
                Graphics.Blit(scaled, dest, FinalMaterial);
				RenderTexture.ReleaseTemporary (scaled);
				
			}
			else
			{
				Graphics.Blit (src, dest);
			}
		}

		void OnDisable ()
		{
			if (m_material)
			{
				Material.DestroyImmediate (m_material);
			}
		}
	}
}



