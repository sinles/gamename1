﻿#region Using Statements
using System;
using System.Collections.Generic;
using GameName1.Extensions;
using GameName1.States;
using Glide;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
#endregion

namespace GameName1
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        private StateManager _stateManager;
        public static float DT { get; private set; }

        public Game()
            : base()
        {
            graphics = new GraphicsDeviceManager(this);


            Content.RootDirectory = "Content";
            IsMouseVisible = true;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            _stateManager = new StateManager();
            Globals.Viewport = GraphicsDevice.Viewport;

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            new SB(GraphicsDevice);
            new CM(Content);
            new Tweener();

            _stateManager.SetState(new LoadGameState(_stateManager));
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            UpdateDeltaTime(gameTime);
            Input.Update();
            _stateManager.Update();

            Tweener.I.Update(DT);

            base.Update(gameTime);
        }

        private static void UpdateDeltaTime(GameTime gameTime)
        {
            TimeSpan deltaTime = gameTime.ElapsedGameTime;
            float delta = deltaTime.Milliseconds;
            delta = delta/1000;
            DT = delta;

            DT = MathHelper.Clamp(DT, 0, 0.1f);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            _stateManager.Draw();


            base.Draw(gameTime);
        }
    }
}
