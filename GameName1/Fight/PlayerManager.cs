﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameName1.Fight
{
    public class PlayerManager
    {
        protected List<Player> Players { get; private set; }

        private Player _currentPlayer;
        private int _currentPlayerIndex;


        public PlayerManager()
        {
        }

        public void SetPlayers(List<Player> players)
        {
            Players = players;
            _currentPlayer = Players.FirstOrDefault();
        }

        public void AddPlayer(Player player)
        {
            Players.Add(player);
        }

        public void Start()
        {
            if (_currentPlayer != null)
            {
                _currentPlayer.StartStep();
            }
        }

        public void Deinit()
        {
            foreach (var player in Players)
            {
                player.Deinit();
            }
        }

        public virtual void Update()
        {
            if (_currentPlayer != null)
            {
                _currentPlayer.Update();
                if (_currentPlayer.StepEnded)
                {
                    EndStep();  
                }
            }
        }

        public virtual void Draw()
        {
            if (_currentPlayer != null)
            {
                _currentPlayer.Draw();
            }
        }

        public void EndStep()
        {
            _currentPlayerIndex++;
            if (_currentPlayerIndex >= Players.Count)
            {
                _currentPlayerIndex = 0;
            }
            if (_currentPlayerIndex >= Players.Count)
            {
                return;
            }

            _currentPlayer = Players[_currentPlayerIndex];
            if (!_currentPlayer.HasCreatures)
            {
                _currentPlayer = null;
                Players.Remove(_currentPlayer);
                EndStep();
                return;
            }

            if (_currentPlayer != null)
            {
                _currentPlayer.StartStep();
                PlayerStepStarted();
            }
        }


        protected virtual void PlayerStepStarted()
        {

        }

        protected Player CurrentPlayer 
        {
            get { return _currentPlayer; }
        }
    }
}
