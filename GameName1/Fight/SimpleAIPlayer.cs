﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameName1.GameObjects;
using Microsoft.Xna.Framework;

namespace GameName1.Fight
{
    public class SimpleAIPlayer:Player
    {
        private readonly Player _otherPlayer;

        public SimpleAIPlayer(Player otherPlayer)
            :base()
        {
            _otherPlayer = otherPlayer;
        }

        public override void Update()
        {
            base.Update();

            var direction = GetDirection();

            if (direction.X != 0 && direction.Y != 0)
            {
                if (RandomTool.NextBool(0.5f))
                {
                    direction.X = 0;
                }
                else
                {
                    direction.Y = 0;
                }
            }

            if (!SelectedCreature.Move(direction.X, direction.Y))
            {
                SelectedCreature.Skip();
            }
        }

        private Point GetDirection()
        {
//            if (RandomTool.NextBool(0.1f))
//            {
//                var result = new Point(RandomTool.NextInt(-1, 2), RandomTool.NextInt(-1, 2));
//                return result;
//            }

            var closest = GetClosestEnemyCreature();

            return new Point((closest.CurrentTile.X - SelectedCreature.CurrentTile.X) > 0 ? 1 : -1,
                             (closest.CurrentTile.Y - SelectedCreature.CurrentTile.Y) > 0 ? 1 : -1);


            return new Point();
        }

        private Creature GetClosestEnemyCreature()
        {
            return _otherPlayer.AliveCreatures.FirstOrDefault();
        }
    }
}
