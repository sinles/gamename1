﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using GameName1.GameObjects;
using GameName1.PathFinding;
using GameName1.States;
using GameName1.Tiles;
using Microsoft.Xna.Framework;

namespace GameName1.Fight
{
    public abstract class Player
    {
        protected List<Creature> Creatures;
        private int _currentCreature;
        private Arrow _arrow;

        public Player()
        {
            _currentCreature = 0;

            _arrow = new Arrow {Active = false};
        }

        public void SetCreatures(List<Creature> creatures)
        {
            Creatures = creatures;
            foreach (Creature creature in Creatures)
            {
                creature.SetPlayer(this);
            }
        }

        public void ActivateCreatures(bool active)
        {
            foreach (var creature in Creatures)
            {
                creature.Active = active;
            }
        }

        public void PackCreatures()
        {
            _arrow.Active = true;
            CreatureGroup.Pack();
        }
        public List<Creature> UnpackCreatures(Rectangle battlefieldArea)
        {
            _arrow.Active = false;
            return CreatureGroup.Unpack(battlefieldArea);
        }

        public virtual void StartStep()
        {
            StepEnded = false;
            _arrow.Active = true;

            if (SelectedCreature == null || SelectedCreature.IsDead)
            {
                Creatures.Remove(SelectedCreature);
                if (Creatures.Count == 0)
                {
                    StepEnded = true;
                    return;
                }
                GetNextCreature();
            }
        }

        public virtual void EndStep()
        {
            if (!StepEnded)
            {
                _arrow.Active = false;
                GetNextCreature();
            }
        }

        public virtual void Update()
        {
            if (SelectedCreature != null)
            {
                if (!SelectedCreature.HasActionPoints())
                {
                    GetNextCreature();
                    //EndStep();
                }
                _arrow.Position = SelectedCreature.Position;
            }
        }

        public virtual void Deinit()
        {
            _arrow.Active = false;
        }

        public virtual void Draw()
        {
        }

        private void GetNextCreature()
        {
            if (SelectedCreature != null)
            {
                SelectedCreature.ResetActionPoints();
            }

            _currentCreature++;
            if (_currentCreature >= Creatures.Count)
            {
                _currentCreature = 0;
                _arrow.Active = false;

                StepEnded = true;
            }
        }



        public bool StepEnded { get; private set; }

        public Creature SelectedCreature
        {
            get
            {
                if (_currentCreature >= Creatures.Count)
                {
                    return null;
                }

                return Creatures[_currentCreature];
            }
        }

        public IEnumerable<Creature> AliveCreatures {
            get { return Creatures; }
        }

        public bool HasCreatures
        {
            get { return Creatures.Any(p => !p.IsDead); }
        }


        public Creature MainCreature
        {
            get { return Creatures.FirstOrDefault(); }
        }

        public CreatureGroup CreatureGroup
        {
            get { return MainCreature as CreatureGroup; }
        }




    }
    
}
