﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameName1.GameObjects;
using GameName1.Tiles;
using Microsoft.Xna.Framework;

namespace GameName1.Fight
{
    public abstract class PlayerAttackState:PlayerState
    {
        protected Creature AttackedCreature;
        private readonly TileMap _tileMap;


        public PlayerAttackState(Player player, TileMap tileMap) 
            : base(player)
        {
            _tileMap = tileMap;
        }

        protected void AttackTile(Point targetTile)
        {
            AttackedCreature = GameObjectManager.Instance.GetCreatureFromTile(targetTile);
            if (AttackedCreature != null)
            {
                AttackedCreature.RecieveDamage(5);
            }

            End();
        }
    }
}
