﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameName1.GameObjects;

namespace GameName1.Fight
{
    public class DummyPlayer:Player
    {
        public DummyPlayer() 
        {
        }

        public override void Update()
        {
            base.Update();
            EndStep();
        }
    }
}
