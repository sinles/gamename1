﻿using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Linq;
using GameName1.GameObjects;

namespace GameName1.Fight
{
    public class FightManager:PlayerManager
    {
        private Player _mainPlayer;
        private Arrow _arrow;

        public void SetMainPlayer(Player player)
        {
            _mainPlayer = player;
        }



        public bool FightEnded 
        {
            get { return Players.Any(p => !p.HasCreatures); }
        }

        public bool MainPlayerLost
        {
            get { return !_mainPlayer.HasCreatures; }
        }
         
    }
}

