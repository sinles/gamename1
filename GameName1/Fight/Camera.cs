﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameName1.Fight
{
    public class Camera
    {
        private Vector2 _position;
        private float _rotation;
        private float _zoom;

        public Camera()
        {
            SmoothPosition = 3;
            Zoom = 1;
        }

        public void Update()
        {
            float spd = MathHelper.Clamp(SmoothPosition*Game.DT, 0, 1);
            _position.X = MathHelper.Lerp(_position.X, TargetPosition.X, spd);
            _position.Y = MathHelper.Lerp(_position.Y, TargetPosition.Y, spd);

            _rotation = MathHelper.Lerp(_rotation, TargetRotation, MathHelper.Clamp(SmoothRotation*Game.DT, 0, 1));
            _zoom = MathHelper.Lerp(_zoom, TargetZoom, MathHelper.Clamp(SmoothZoom*Game.DT, 0, 1));
        }

        public Vector2 ToScreenSpace(Vector2 screenSpacePosition)
        {
            var transformed = Vector3.Transform(new Vector3(screenSpacePosition.X, screenSpacePosition.Y, 1),
                GetMatrix());
            return new Vector2(transformed.X, transformed.Y);
        }

        public Vector2 ToWorldSpace(Vector2 worldSpacePosition)
        {
            var transformed = Vector3.Transform(new Vector3(worldSpacePosition.X, worldSpacePosition.Y, 1),
                Matrix.Invert(GetMatrix()));
            return new Vector2(transformed.X, transformed.Y);
        }

        public Vector2 ScreenToViewportPoint(Vector2 screenSpacePosition)
        {
            return new Vector2(screenSpacePosition.X/Globals.Viewport.Width,
                screenSpacePosition.Y/Globals.Viewport.Height);
        }

        public Matrix GetMatrix()
        {
            Viewport vp = Globals.Viewport;

            Vector2 cameraWorldPosition = (Position);
            Vector2 screenCentre = new Vector2(vp.Width/(2f*Zoom), vp.Height/(2f*Zoom));

            Vector2 translation = -cameraWorldPosition + screenCentre;

            Matrix cameraMatrix = Matrix.CreateTranslation((int) Math.Round(translation.X),
                (int) Math.Round(translation.Y), 0)
                                  *Matrix.CreateRotationZ(Rotation)
                                  *Matrix.CreateScale(new Vector3(Zoom, Zoom, 1));
            return cameraMatrix;
        }

        public float SmoothPosition { get; set; }
        public float SmoothRotation { get; set; }
        public float SmoothZoom { get; set; }

        public Vector2 TargetPosition { get; set; }

        public Vector2 Position
        {
            get { return _position; }
            set
            {
                _position = value;
                TargetPosition = _position;
            }
        }

        public float TargetRotation { get; set; }

        public float Rotation
        {
            get { return _rotation; }
            set
            {
                _rotation = value;
                TargetRotation = _rotation;
            }
        }

        public float TargetZoom { get; set; }

        public float Zoom
        {
            get { return _zoom; }
            set
            {
                _zoom = value;
                TargetZoom = _zoom;
            }
        }

        public Rectangle Bounds
        {
            get
            {
                Rectangle bounds = new Rectangle();

                float scrWidth = Globals.Viewport.Width;
                float scrHeight = Globals.Viewport.Height;

                bounds.X = (int) ((Position.X - scrWidth/2));
                bounds.Y = (int) ((Position.Y - scrHeight/2));
                bounds.Width = (int) (scrWidth);
                bounds.Height = (int) (scrHeight);

                bounds.X += (int) ((bounds.Width - (bounds.Width/Zoom))/2);
                bounds.Y += (int) ((bounds.Height - (bounds.Height/Zoom))/2);
                bounds.Width = (int) (bounds.Width/Zoom);
                bounds.Height = (int) (bounds.Height/Zoom);

                if (_rotation%MathHelper.TwoPi != 0)
                {
                    Vector2 lt = new Vector2(-bounds.Width/2, -bounds.Height/2);
                    Vector2 rt = new Vector2(bounds.Width/2, -bounds.Height/2);
                    Vector2 rb = new Vector2(bounds.Width/2, bounds.Height/2);
                    Vector2 lb = new Vector2(-bounds.Width/2, bounds.Height/2);
                    Quaternion rot = Quaternion.CreateFromAxisAngle(new Vector3(0, 0, 1), _rotation);

                    Vector2.Transform(ref lt, ref rot, out lt);
                    Vector2.Transform(ref rt, ref rot, out rt);
                    Vector2.Transform(ref rb, ref rot, out rb);
                    Vector2.Transform(ref lb, ref rot, out lb);
                    lt.X += bounds.X + bounds.Width/2;
                    lt.Y += bounds.Y + bounds.Height/2;
                    rt.X += bounds.X + bounds.Width/2;
                    rt.Y += bounds.Y + bounds.Height/2;
                    rb.X += bounds.X + bounds.Width/2;
                    rb.Y += bounds.Y + bounds.Height/2;
                    lb.X += bounds.X + bounds.Width/2;
                    lb.Y += bounds.Y + bounds.Height/2;
                    bounds.X = (int) Math.Round(Math.Min(Math.Min(lt.X, rt.X), Math.Min(rb.X, lb.X)));
                    bounds.Y = (int) Math.Round(Math.Min(Math.Min(lt.Y, rt.Y), Math.Min(rb.Y, lb.Y)));
                    bounds.Width = (int) Math.Round(Math.Max(Math.Max(lt.X, rt.X), Math.Max(rb.X, lb.X))) - bounds.X;
                    bounds.Height = (int) Math.Round(Math.Max(Math.Max(lt.Y, rt.Y), Math.Max(rb.Y, lb.Y))) - bounds.Y;
                }

                return bounds;
            }
        }

        public Rectangle TileBounds
        {
            get
            {
                var bounds = Bounds;

                bounds.X = bounds.X/Globals.TileSize;
                bounds.Y = bounds.Y/Globals.TileSize;
                bounds.Width = (bounds.Width/Globals.TileSize) + 2;
                bounds.Height = (bounds.Height/Globals.TileSize) + 2;

                return bounds;
            }
        }
    }
}
