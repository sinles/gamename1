﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameName1.GameObjects;

namespace GameName1.Fight
{
    public class TravelManager:PlayerManager
    {
        private readonly Player _mainPlayer;

        public TravelManager() 
        {
            _mainPlayer = new HumanPlayer();
        }

        public void ActivateTravel()
        {
            _mainPlayer.PackCreatures();
        }

        public void CleanOtherPlayers()
        {
            foreach (var player in Players.Where(p => p != _mainPlayer))
            {
                player.CreatureGroup.Destroy();
            }

            Players.RemoveAll(p => p != _mainPlayer);
        }

        protected override void PlayerStepStarted()
        {
            base.PlayerStepStarted();

            if (CurrentPlayer == _mainPlayer)
            {
                var mainCreature = CurrentPlayer.MainCreature;


                var gos = World.I.TileMap.CheckCircleForTileObjects(mainCreature.CurrentTile, 8);

                var creatures = gos.Where(p => p is Creature).Cast<Creature>().ToList();

                if (creatures.Any())
                {
                    if (BattleStarted != null)
                    {
                        foreach (var c in creatures)
                        {
                            var otherPlayer = c.Player;

                            if (otherPlayer != _mainPlayer)
                            {
                                BattleStarted(otherPlayer);
                            }
                        }
                    }
                }
            }
        }

        public event Action<Player> BattleStarted;
        public Player MainPlayer {
            get { return _mainPlayer; }
        }


    }
}
