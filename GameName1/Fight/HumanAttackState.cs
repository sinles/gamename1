﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameName1.Tiles;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace GameName1.Fight
{
    public class HumanAttackState:PlayerAttackState
    {
        public HumanAttackState(Player player, TileMap tileMap)
            : base(player, tileMap)
        {
        }

        public override void Update()
        {
            base.Update();

            var state = Mouse.GetState();

            if (state.LeftButton == ButtonState.Pressed)
            {
                var target = Input.GetTileFromMousePosition();
                AttackTile(target);
            }
        }
    }
}
