﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace GameName1.Fight
{
    public class CameraControler
    {
        public float MouseBorder = 0.2f;
        public float Sensetivity;
        public float SmoothSpeed;

        public float ZoomSensetivity;
        public float ZoomSmooth;

        public float MinZoom= 0.5f;
        public float MaxZoom = 5f;

        public Rectangle Borders;


        public float TargetZoom { get; set; }

        private float _prevScrollWheel;

        public CameraControler()
        {
            Camera = new Camera();
            

            Camera.SmoothPosition = 10;
            Camera.SmoothZoom = 10;
            Camera.SmoothPosition = 10;
            TargetZoom = 1;
        }

        public void Update()
        {
            var mouseState = Mouse.GetState();


           // UpdatePositionControl(mouseState);
            float scrollDelta = _prevScrollWheel - mouseState.ScrollWheelValue;
            _prevScrollWheel = mouseState.ScrollWheelValue;
            float zoomDelta = scrollDelta * ZoomSensetivity;
            Camera.TargetZoom = MathHelper.Clamp(TargetZoom - zoomDelta, MinZoom, MaxZoom);

            Camera.Update();
        }

        private void UpdatePositionControl(MouseState mouseState)
        {
            Vector2 mousePos = Camera.ScreenToViewportPoint(new Vector2(mouseState.Position.X, mouseState.Position.Y));
            mousePos.X = MathHelper.Clamp(mousePos.X, 0, 1);
            mousePos.Y = MathHelper.Clamp(mousePos.Y, 0, 1);


            Vector2 movement = new Vector2();

            if (mousePos.X < MouseBorder)
            {
                movement.X = mousePos.X - MouseBorder;
            }
            else if (mousePos.X > 1f - MouseBorder)
            {
                movement.X = 1f - mousePos.X - MouseBorder;
                movement.X *= -1;
            }

            if (mousePos.Y < MouseBorder)
            {
                movement.Y = mousePos.Y - MouseBorder;
            }
            else if (mousePos.Y > 1f - MouseBorder)
            {
                movement.Y = 1f - mousePos.Y - MouseBorder;
                movement.Y *= -1;
            }

            float spd = movement.Length();
            if (movement != Vector2.Zero)
            {
                movement.Normalize();
                movement = movement*MathHelper.Clamp(spd, -Sensetivity, Sensetivity);


                Camera.TargetPosition = Camera.TargetPosition + movement * Sensetivity * Game.DT;
            }
        }

        public Camera Camera { get; private set; }
    }
}
