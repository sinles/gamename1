﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameName1.GameObjects;
using GameName1.Tiles;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace GameName1.Fight
{
    public class HumanPlayer:Player
    {
        public HumanPlayer() 
            : base()
        {
        }

        public override void StartStep()
        {
            base.StartStep();
        }

        public override void Update()
        {
            if (SelectedCreature != null)
            {
                if (GetMovementInput(Keys.Left))
                {
                    Move(-1, 0);
                }
                else if (GetMovementInput(Keys.Right))
                {
                    Move(1, 0);
                }
                else if (GetMovementInput(Keys.Up))
                {
                    Move(0, -1);
                }
                else if (GetMovementInput(Keys.Down))
                {
                    Move(0, 1);
                }
                else if (GetMovementInput(Keys.S))
                {
                    SelectedCreature.Skip();
                }
                else if(Input.IsKeyDown(Keys.E))
                {
                    SelectedCreature.Use();
                }
            }
            base.Update();
        }

        private bool GetMovementInput(Keys key)
        {
            return SelectedCreature is CreatureGroup ? Input.IsKeyPressed(key) : Input.IsKeyDown(key);
        }

        private void Move(int x, int y)
        {
            if (SelectedCreature.Move(x, y))
            {
                Globals.Camera.TargetPosition = SelectedCreature.Position + Globals.TileToPosition(new Point(x, y));
            }
        }

        public override void Draw()
        {
            base.Draw();
        }
    }
}
