﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameName1.Extensions
{
    public class PackedTexture
    {

        public PackedTexture(Texture2D texture, Rectangle source)
        {
            Texture = texture;
            Source = source;
        }

        public Texture2D Texture { get; private set; }
        public Rectangle Source { get; private set; }
    }
}
