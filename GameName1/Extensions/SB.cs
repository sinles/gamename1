﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Dynamic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameName1.Extensions
{
    public class SB:SpriteBatch
    {
        private static SB _instance;

        public SB(GraphicsDevice graphicsDevice) 
            : base(graphicsDevice)
        {
            _instance = this;
        }

        public void DrawCentered(Texture2D texture, Vector2 position, Color color, float scale = 1.0f)
        {
            Draw(texture, position, null, Color.White, 0, new Vector2(texture.Width / 2f, texture.Height / 2f), scale, SpriteEffects.None, 0);
        }

        public static SB I
        {
            get
            {
                return _instance;
            }
        }
    }
}
