﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace GameName1.Extensions
{
    public static class Vector2Extensions
    {
        public static Size ToSize(this Vector2 v)
        {
            return new Size((int)v.X,(int)v.Y);
        }
    }
}
