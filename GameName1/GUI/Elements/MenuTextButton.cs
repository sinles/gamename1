﻿using GameName1.Extensions;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameName1.GUI.Elements
{
    class MenuTextButton:UIObject
    {
        public string Text { get;protected set; }
    
        protected SpriteFont Font;
        private float _size = 0.8f;

        public MenuTextButton(string text, SpriteFont font)
            :base()
        {
            Text = text;
            Size = font.MeasureString(Text).ToSize();
            Font = font;

            MouseMoveIn += OnMouseMoveIn;
            MouseMoveOut += OnMouseMoveOut;
        }

        private void OnMouseMoveOut(object sender, MouseMoveEventArgs mouseMoveEventArgs)
        {
            _size = 0.8f;
        }

        private void OnMouseMoveIn(object sender, MouseMoveEventArgs mouseMoveEventArgs)
        {
            _size = 1.0f;
        }

        internal override void Draw(double dt)
        {
            base.Draw(dt);
            SB.I.DrawString(Font, Text, new Vector2(X, Y), Color.Red, 0, new Vector2(), _size, 0, 0);
        }
    }
}
