//using System;

using System;
using System.Drawing;
using GameName1.GUI;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Color = System.Drawing.Color;
using Rectangle = System.Drawing.Rectangle;

namespace GameName1.GUI.Elements
{
    public delegate void MouseEventHandler(object sender, MouseButtonEventArgs e);
    public delegate void MouseMoveEventHandler(object sender, MouseMoveEventArgs e);
    public delegate void WheelEventHandler(object sender, MouseWheelEventArgs e);
    public delegate void KeyPressEventHandler(object sender, KeyboardKeyEventArgs e);
    public delegate void ElementDrawEventHandler();

    public abstract class UIObject
    {
        //protected Rectangle Rect;

        #region variables

        internal GUIManager GUIManager;

        private float _x, _y;

        protected float BaseX, BaseY, BaseW, BaseH;

        public Size Size;

        private Container _parent;

        protected State State;

        protected double DT = 0.0;
        protected bool wasClicked = false;
        public double TimeOfDoubleClick = 0.2;

        public string HelpText = "";

        /// <summary>
        /// обозначает что элемент может перехватывать мышь
        /// </summary>
        public bool MouseInterception = true;

        public bool Clickable = true;//??
        public bool Clicked;
        public bool Active = true;
        public bool Visible = true;
        public Color color = Color.White;

        /// <summary>
        /// доступна отовсюду
        /// </summary>
        public Keys GlobalKey;
        /// <summary>
        /// доступна лишь если контейнер в фокусе
        /// </summary>
        public Keys LocalKey;

        #endregion variables

        #region properties

        public int Depth { get; set; }

        public Container Parent
        {
            get { return _parent; }

            set { _parent = value; }
        }

        public Vector2 Position
        {
            get { return new Vector2(X, Y); }
            set
            {
                _x = value.X;
                _y = value.Y;
            }
        }

        public float X
        {
            get
            {
                if (Parent != null)
                    return _x + Parent.X;
                return _x;
            }
            set { _x = value; }
        }

        public float Y
        {
            get
            {
                if (Parent != null)
                    return _y + Parent.Y;
                return _y;
            }
            set { _y = value; }
        }

        //public float CenterX
        //{
        //    get
        //    {
        //        return X + Width / 2;
        //    }
        //    set
        //    {
        //        Position.X = value - Size.Width / 2;
        //    }
        //}

        //public float centerY
        //{
        //    get
        //    {
        //        return Position.Y + Size.Height;
        //    }
        //    set
        //    {
        //        Position.Y = value - Size.Height / 2;
        //    }
        //}

        public int Width
        {
            get { return Size.Width; }
            set
            {
                //if (value != 0)
                //    if (this is Container)
                //    {
                //        Container temp = this as Container;
                //        float r = (float)Size.Width / value;

                //        foreach (UIObject uiObject in temp.Elements)
                //        {
                //            uiObject.Width = (int)(uiObject.Width / r);
                //            if (uiObject.Width < 0)
                //                uiObject.Width = 0;
                //            //uiObject.X -= Math.Abs(r);
                //        }
                //        Size.Width = value;
                //    }
                Size.Width = value;
            }
        }

        public int Height
        {
            get { return Size.Height; }
            set { Size.Height = value; }
        }

        public Rectangle Bounds
        {
            get
            {
                return new Rectangle((int)X, (int)Y, Width, Height);
            }
            set
            {
                _x = value.X;
                _x = value.Y;
                Size.Width = value.Width;
                Size.Height = value.Height;
            }
        }

        #endregion properties

        #region Events

        public event EventHandler Action;

        public event KeyPressEventHandler KeyPress;

        public event KeyPressEventHandler GlobalKeyPress;

        public event MouseEventHandler DoubleClick;

        public event MouseMoveEventHandler MouseMoveIn;

        public event MouseMoveEventHandler MouseMoveOut;

        public event MouseEventHandler Click;

        public event MouseEventHandler ReleaseIn;

        public event MouseEventHandler ReleaseOut;

        public event WheelEventHandler MouseWheelChanged;

        #endregion Events

        #region public methods

        internal virtual void Initialize()
        {
            BaseX = _x;
            BaseY = _y;
            BaseW = Width;
            BaseH = Height;
        }

        protected virtual void StartClosing()
        {
        }

        public void GlobalKeyPressed(KeyboardKeyEventArgs e)
        {
            if (GlobalKeyPress != null)
                GlobalKeyPress(this, e);
        }

        public void KeyPressed(KeyboardKeyEventArgs e)
        {
            if (KeyPress != null)
                KeyPress(this, e);
        }

        public void WheelChanged(MouseWheelEventArgs e)
        {
            if (MouseWheelChanged != null)
                MouseWheelChanged(this, e);
        }

        public void OnMouseMoveIn(MouseMoveEventArgs e)
        {
            if (MouseMoveIn != null)
                MouseMoveIn(this, e);
        }
        public void OnMouseMoveOut(MouseMoveEventArgs e)
        {
            if (MouseMoveIn != null)
                MouseMoveOut(this, e);
        }


        public void OnLeftMouseButtonDown(MouseButtonEventArgs e)
        {
            if (Clickable)
                if (Click != null)
                    Click(this, e);
            if (wasClicked)
            {
                if (DoubleClick != null)
                {
                    DoubleClick(this, e);
                    wasClicked = false;
                    DT = 0.0;
                }
            }
            else
                wasClicked = true;
        }

        public void OnLeftMouseButtonUp( MouseButtonEventArgs e)
        {
            if (Clickable)
            {
                if (MouseInteserect(e.X, e.Y))
                {
                    if (ReleaseIn != null)
                        ReleaseIn(this, e);
                    if (Action != null)
                        Action(this, e);
                }
                else if (ReleaseOut != null)
                    ReleaseOut(this, e);
            }
        }

        public virtual void Close()
        {
            State = State.Closing;
        }

        internal virtual void Update(double dt)
        {
            if (!Active) return;

            if (wasClicked)
            {
                this.DT += dt;

                if (TimeOfDoubleClick < this.DT)
                {
                    this.DT = 0.0;
                    wasClicked = false;
                }
            }

            //if (State == State.Closing)
            //{
            //    Width -= (int)(dt * 2000);
            //    X += (int)(dt * 1000);
            //    Y += (float)(dt * 500);
            //    double alpha = color.A - dt * 1000;
            //    if (alpha > 255 || alpha < 0)
            //        alpha = 0;
            //    color = Color.FromArgb((byte)alpha, color.R, color.G, color.B);
            //    if (Width <= 0 || (byte)alpha == 0)
            //    {
            //        GUIManager.Remove(this);
            //    }
            //}
        }

        internal virtual void Draw(double dt)
        {
            if (!Visible) return;
        }

        /// <summary>
        /// Проверяет нахождение мыши над элементом
        /// </summary>
        internal bool MouseInteserect(int x, int y)
        {
            return MouseInteserect(new Vector2(x, y));
        }

        internal bool MouseInteserect(Vector2 mousePosition)
        {
            return (Bounds.Contains((int)mousePosition.X, (int)mousePosition.Y));
        }

        #endregion public methods
    }
}