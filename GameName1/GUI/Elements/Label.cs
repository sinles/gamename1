﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameName1.Extensions;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameName1.GUI.Elements
{
    public class Label:UIObject
    {
        private readonly SpriteFont _font;

        public Label(SpriteFont font)
        {
            _font = font;
            Color = Color.White;
        }

        internal override void Draw(double dt)
        {
            base.Draw(dt);
            SB.I.DrawString(_font, Text, Position, Color);
        }

        public string Text { get; set; }
        public Color Color { get; set; }
    
    }
}
