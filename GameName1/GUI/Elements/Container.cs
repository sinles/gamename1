using System.Collections.Generic;
using System.Linq;
using GameName1.GUI.Elements;
using Microsoft.Xna.Framework.Input;

namespace GameName1.GUI.Elements
{
    public class Container : UIObject
    {
        public List<UIObject> Elements { get; set; }

        public UIObject this[int index]
        {
            get
            {
                return Elements[index];
            }
        }

        public Container()
            : base()
        {
            Elements = new List<UIObject>();
        }

        public void Add(UIObject uiObject)
        {
            Elements.Add(uiObject);
            uiObject.Parent = this;
            uiObject.GUIManager = GUIManager;
            uiObject.Initialize();
            if (uiObject.GlobalKey != Keys.None)
                GUIManager.CanBePressed.Add(uiObject);
        }

        public void Remove(UIObject uiObject)
        {
            Elements.Remove(uiObject);
        }

        public virtual void Clear()
        {
            Elements.Clear();
        }

        public bool Exist(UIObject uiObject)
        {
            return Elements.Any(u => u == uiObject);
        }

        public UIObject FoundFocus(int mouseX, int mouseY, bool setToFront = true)
        {
            if (Elements.Count != 0)
            {
                int min = 0;
                while (!Elements[min].MouseInteserect(mouseX, mouseY) || !Elements[min].Visible)
                {
                    min++;
                    if (min == Elements.Count) break;
                }
                if (min != Elements.Count)
                {
                    int k = min;
                    if (setToFront)
                    {
                        SetToFront(Elements[min]);
                        k = 0;
                    }
                    if (Elements[k] is Container)
                    {
                        return (Elements[k] as Container).FoundFocus(mouseX, mouseY, setToFront) ?? Elements[k];
                    }
                    return Elements[k];
                }
            }
            return null;
        }

        public void SetToFront(UIObject uiobject)
        {
            Elements.Remove(uiobject);
            Elements.Insert(0, uiobject);
            if (Parent != null)
                Parent.SetToFront(this);
        }

        internal override void Initialize() { }

        internal override void Update(double dt)
        {
            base.Update(dt);
            for (int i = Elements.Count - 1; i >= 0; i--)
                Elements[i].Update(dt);
        }

        internal override void Draw(double dt)
        {
            base.Draw(dt);
            for (int i = Elements.Count - 1; i >= 0; i--)
                Elements[i].Draw(dt);
        }
    }
}