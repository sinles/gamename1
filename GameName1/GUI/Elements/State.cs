﻿using Microsoft.Xna.Framework;

namespace GameName1.GUI.Elements
{
    public enum State
    {
        Open, 
        Closed,
        Closing,
        Opening
    }
}