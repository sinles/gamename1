﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Microsoft.Xna.Framework.Graphics;

namespace GameName1.GUI.Elements
{
    public delegate void ItemSelectedEventHandler(object sender, int index);

    internal class TextMenu : Container
    {
        public event ItemSelectedEventHandler ItemSelected;

        private int SelectedItem { get; set; }

        private SpriteFont _font;
        private List<string> _items;

        private int _count;

        public TextMenu(SpriteFont font)
        {
            SelectedItem = 0;
            _items = new List<string>();
            _font = font;
            State = State.Opening;
        }

        public void AddRecords(params string[] items)
        {
            foreach (var item in items)
            {
                AddRecord(item);
            }
        }

        public void AddRecord(string item)
        {
            _items.Add(item);
            MenuTextButton button = new MenuTextButton(item, _font);
            button.Click += ButtonOnClick;
            // button.Size = new Size(Width, (int)_font.Measure(item).Height);
            button.Y = button.Height * _count;
            _count++;
            Add(button);
        }

        public override void Clear()
        {
            base.Clear();
            _count = 0;
        }

        private void ButtonOnClick(object sender, MouseButtonEventArgs mouseButtonEventArgs)
        {
            var button = sender as MenuTextButton;
            if (button == null) return;

            SelectedItem = _items.FindIndex(0, s => s == button.Text) + 1;
            ItemSelected(this, SelectedItem);
        }
    }
}