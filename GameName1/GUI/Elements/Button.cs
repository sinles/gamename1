using System;
using System.Drawing;
using GameName1.GUI.Elements;
using Microsoft.Xna.Framework.Graphics;

namespace GameName1.GUI.Elements
{
    public class Button : UIObject
    {
        protected State state = State.Idle;
        protected Texture IdleTexture;
        protected Texture PressedTexture;

        public Button()
            : base()
        {
            Click += OnClick;
            ReleaseIn += OnRelease;
            ReleaseOut += OnRelease;
        }

        internal override void Initialize()
        {
        }

        internal override void Draw(double dt)
        {
            base.Draw(dt);
        }

        private void OnClick(object sender, MouseButtonEventArgs mouseButtonEventArgs)
        {
            state = State.Pressed;
        }

        private void OnRelease(object sender, MouseButtonEventArgs mouseButtonEventArgs)
        {
            state = State.Idle;
        }

        public enum State
        {
            Idle = 0,
            Hover = 1,
            Pressed = 2
        }
    }
}