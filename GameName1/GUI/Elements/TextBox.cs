﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using GameName1.Extensions;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace GameName1.GUI.Elements
{
    class TextBox:UIObject
    {
        public string Mask;

        private List<string> Lines; 
        private string _text;

        public string Text
        {
            get { return _text; }
            set
            {
                _text = value;
                //SizeF fontSize = _font.Measure(_text);
                Lines.Clear();
                string curLine = "";
                for (int i = 0; i < _text.Count() &&  Lines.Count < RowsCount; i++)
                {
                    curLine += _text[i];
                    if (_font.MeasureString(curLine).X>= Width)
                    {
                        Lines.Add(curLine);
                        curLine = "";
                    }
                }

                if(curLine != "")
                    Lines.Add(curLine);
            //    Lines.Reverse();
            }
        }


        private int _cursorLine;
        private int _cursorRow;
        private int CursorLine
        {
            get { return _cursorLine; }
            set
            {
                _cursorLine = value;
                var maxSymbols = SymbolsPerRow;
                if(_cursorLine > Text.Count())
                {
                    _cursorLine = Text.Count();
                }
                else if (_cursorLine > maxSymbols)
                {
                    _cursorLine = 1;//dirty...
                    CursorRow++;
                }
                else if (_cursorLine < 0)
                {
                    if (CursorRow == 0)
                        _cursorLine = 0;
                    else
                    {
                        _cursorLine = SymbolsPerRow;
                        CursorRow--;
                    }

                }
            }
        }

        private int CursorRow
        {
            get 
            {
                return _cursorRow;
            }
            set 
            { 
                _cursorRow = value;

                if (_cursorRow < 0)
                    _cursorRow = 0;
                else if (_cursorRow > Lines.Count || _cursorRow >= RowsCount )
                    _cursorRow = Lines.Count - 1;
            }
        }
        
        private int CursorPosition
        {
            get { return CursorLine + (CursorRow*SymbolsPerRow); }
        }

        public float CursorBlinkTime = 1.0f;
        public float CursorBlinkLenght = 0.2f;
        private float cursorBlinkElapsed = 0.0f;

        private bool CursorVisible
        {
            get { return cursorBlinkElapsed > CursorBlinkLenght; }
        }

        private SizeF fontSize;
        private SpriteFont _font;

        private int TotalMaxNumberOfSymbols
        {
            get { return SymbolsPerRow*RowsCount; }
        }

        private int SymbolsPerRow
        {
            get { return (int)Math.Round(Width/fontSize.Width); }
        }

        private int RowsCount
        {
            get { return (int) Math.Round(Height/fontSize.Height); }
        }


        public TextBox(SpriteFont font):base()
        {
            Lines = new List<string>();
            _font = font;
            fontSize = _font.MeasureString(" ").ToSize();
            Text = "";
            KeyPress += OnKeyPress;
            

        }

        private void OnKeyPress(object sender, KeyboardKeyEventArgs e)
        {
            //int i = 1;
            //i += ++i+i++ + ++i+i++;
            //Console.WriteLine(i);
            switch (e.Key)
            {
                case Keys.Back:
                    if (CursorLine > 0 && Text.Any())
                        Text = Text.Remove(CursorPosition - 1, 1);
                    CursorLine--;

                    break;
                case Keys.Delete:
                    if (CursorLine < Text.Count() && Text.Any())
                        Text = Text.Remove(CursorLine, 1);
                    break;
                case Keys.Right:
                        CursorLine++;
                    break;
                case Keys.Left:
                        CursorLine--;
                    break;
                default:
                    var str = GetKeyByInput(e.Key) ?? e.Key.ToString();
                    if (IsInMask(str) && TotalMaxNumberOfSymbols > Text.Count())
                    {
                        Text = Text.Insert(CursorPosition, str);
                        CursorLine++;
                    }
                    break;
            }
        }

        private bool IsInMask(string str)
        {
            return Mask == null || Mask.Contains(str);
        }

        private string GetKeyByInput(Keys key)
        {
//            switch (key)
            {
//                case Keys.:
//                    return "1";
//                case Key.Number2:
//                    return "2";
//                case Key.Number3:
//                    return "3";
//                case Key.Number4:
//                    return "4";
//                case Key.Number5:
//                    return "5";
//                case Key.Number6:
//                    return "6";
//                case Key.Number7:
//                    return "7";
//                case Key.Number8:
//                    return "8";
//                case Key.Number9:
//                    return "9";
//                case Key.Number0:
//                    return "0";
//                case Key.Period:
//                    return ".";
//                case Key.Space:
//                    return " ";
            }
            return null;
        }

        internal override void Update(double dt)
        {
            base.Update(dt);
            cursorBlinkElapsed -= (float)dt;
            if(cursorBlinkElapsed < 0)
            {
                cursorBlinkElapsed = CursorBlinkTime;
            }
        }

        internal override void Draw(double dt)
        {
            base.Draw(dt);
            //SB.I.OutlineRectangle(X, Y, Width, Height, color, 1.0f, 0.0f, 0.0f, 0.0f);
            if(Lines.Count() == 0 && GUIManager.FocusedElement != this)
            {
                Color clr = Color.FromArgb(color.A / 2, color);
//                SpriteBatch.Instance.PrintText(_font, HelpText, X, Y, clr,
//                               0.0f, 1.0f, 0.0f, 0.0f,
//                               false, false);
            }
            for (int index = 0; index < Lines.Count; index++)
          //  for (int index = Lines.Count; index >= 0; index)
            {
                string line = Lines[index];
//                SpriteBatch.Instance.PrintText(_font, line, X, Y + fontSize.Height * index, color,
//                                               0.0f, 1.0f, 0.0f, 0.0f,
//                                               false, false);
            }


            if(CursorVisible && this.GUIManager.FocusedElement == this)
            {
                var x = CursorLine*fontSize.Width + X;
                var y = CursorRow * fontSize.Height + Y;
                //SB.I.DrawLine(x, y, x, y + fontSize.Height, color, 2.0f);
            }
        }
    }
}
