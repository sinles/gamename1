﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Rectangle = System.Drawing.Rectangle;

namespace GameName1.GUI.Elements
{
    public class Window : Container
    {
        private string Title;
        public bool Bordered;
        public byte DownPartAlpha = 255;
        private Button closeButton;
        private Texture2D texture;
        private Rectangle MovingRect;

        private Vector2 offset;
        protected bool StartMove;

        public Window()
            : base()
        {
            Click += MousePress;
            ReleaseIn += MouseRelease;
            ReleaseOut += MouseRelease;
        }

        internal override void Draw(double dt)
        {
            //SpriteBatch.Instance.DrawTexture(texture, Bounds, Rectangle.Empty, color, 0, Vector2.Zero, false, true, 0);
            base.Draw(dt);
        }

        internal override void Update(double dt)
        {
            base.Update(dt);
            if (StartMove)
                if (GUIManager.MousePosition.X > 0 && GUIManager.MousePosition.X < GUIManager.ClientSize.Width
                    && GUIManager.MousePosition.Y > 0 && GUIManager.MousePosition.Y < GUIManager.ClientSize.Height)
                    Position = GUIManager.MousePosition - offset;
        }

        internal override void Initialize()
        {
            //MovingRect = new Rectangle((int)X, (int)Y, Width,
            //                           DrawHelper.Instance[TexturesType.CloseButton].Size.Height);
            //CreateTextures();
            //closeButton = new Button
            //                  {
            //                      Size = DrawHelper.Instance[TexturesType.CloseButton].Size,
            //                      X = Width - DrawHelper.Instance[TexturesType.CloseButton].Size.Width,
            //                      IsDrawBorders = false,
            //                      color = Color.Gray,
            //                      Image = DrawHelper.Instance[TexturesType.CloseButton],
            //                  };
            closeButton.Action += CloseButtonReleaseIn;
            Add(closeButton);
            if (Parent != null)
                Parent.SetToFront(this);
            GUIManager.FocusedElement = this;
        }

        protected void MousePress(object sender, MouseButtonEventArgs e)
        {
            if (MovingRect.Contains(e.X, e.Y))
            {
                StartMove = true;
                offset = new Vector2(e.X - X, e.Y - Y);
            }
        }

        protected virtual void MouseRelease(object sender, MouseButtonEventArgs e)
        {
            //StartMove = false;
            //MovingRect = new Rectangle((int)X, (int)Y, Width,
            //                           DrawHelper.Instance[TexturesType.CloseButton].Size.Height);
            //Bounds = new Rectangle(X, Y, Width, Height);
        }

        private void CloseButtonReleaseIn(object sender, EventArgs eventArgs)
        {
            GUIManager.Remove(this);
        }
    }
}