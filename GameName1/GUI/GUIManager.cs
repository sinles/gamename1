﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using GameName1.Extensions;
using GameName1.GUI.Elements;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Game = GameName1.Game;

namespace GameName1.GUI
{
    public class GUIManager : IDisposable
    {
        public bool MouseIntercepted
        {
            get { return HoveredElement != null; }
        }

        private UIObject _focusedElement;

        internal UIObject FocusedElement
        {
            get
            {
                return _focusedElement;
            }
            set
            {
                _focusedElement = value;
            }
        }

        internal UIObject HoveredElement;

        private Container Elements;
        internal List<UIObject> CanBePressed;
        internal Size ClientSize;
        internal Vector2 MousePosition;

        internal double Dt;

        public GUIManager()
        {
            Elements = new Container();
            Elements.GUIManager = this;

            CanBePressed = new List<UIObject>();

            //todo: Create input class
//            ClientSize = gameWindow.ClientSize;
//
            Input.KeyDown += OnKeyDown;
            Input.MouseDown += OnMouseButtonDown;
            Input.MouseUp += OnMouseButtonUp;
            Input.MouseMove += OnMouseMove;
            Input.WheelChanged += Mouse_WheelChanged;
//
//            gameWindow.GamepadState.OnButtonPress += new GamepadButtonHandler(GamepadState_OnButtonPress);
//            gameWindow.GamepadState.OnButtonUp += new GamepadButtonHandler(GamepadState_OnButtonUp);
//            gameWindow.GamepadState.OnLeftStick += new GamepadStickHandler(GamepadState_OnLeftStick);
        }

//        private void GamepadState_OnButtonUp(object sender, SlimDX.XInput.GamepadButtonFlags e)
//        {
//            if ((e & SlimDX.XInput.GamepadButtonFlags.A) == 0)
//            {
//                OnMouseButtonUp(this, new MouseButtonEventArgs(GameWindow.Mouse.X, GameWindow.Mouse.Y, MouseButton.Left, false));
//            }
//        }
//
//        private void GamepadState_OnLeftStick(object sender, GamepadState.ThumbstickState e, Vector2 delta)
//        {
//        }
//
//        private void GamepadState_OnButtonPress(object sender, SlimDX.XInput.GamepadButtonFlags e)
//        {
//            if ((e & SlimDX.XInput.GamepadButtonFlags.A) != 0)
//            {
//                OnMouseButtonDown(this, new MouseButtonEventArgs(GameWindow.Mouse.X, GameWindow.Mouse.Y, MouseButton.Left, true));
//            }
//        }

        public List<UIObject> GetElements()
        {
            return Elements.Elements;
        }

        public void Add(UIObject uiobject)
        {
            Elements.Add(uiobject);
        }

        public void Remove(UIObject uiObject)
        {
            Elements.Remove(uiObject);
            CanBePressed.Remove(uiObject);
            FocusedElement = Elements[0];
        }

        public bool Exist(UIObject uiObject)
        {
            return Elements.Exist(uiObject);
        }

        public void Update(double dt)
        {
            Dt = dt;
            Elements.Update(dt);
        }

        public void Draw(double dt)
        {
            Elements.Draw(dt);
        }

        #region Private methods

        private void OnKeyDown(object sender, KeyboardKeyEventArgs e)
        {
            foreach (var uiObject in CanBePressed.Where(uiObject => uiObject.GlobalKey == e.Key))
            {
                uiObject.GlobalKeyPressed(e);
                break;
            }
            if (FocusedElement != null)
            {
                FocusedElement.KeyPressed(e);
            }
        }

        private void OnMouseButtonDown(object sender, MouseButtonEventArgs e)
        {
            FocusedElement = Elements.FoundFocus(e.X, e.Y);
            if (FocusedElement != null)
                if (FocusedElement.MouseInterception)
                    if (e.Button == MouseButton.Left)
                    {
                        FocusedElement.OnLeftMouseButtonDown(e);
                    }
        }

        private void OnMouseButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (FocusedElement != null) // if button was pressed
            {
                FocusedElement.OnLeftMouseButtonUp(e);
            }
        }

        private void Mouse_WheelChanged(object sender, MouseWheelEventArgs e)
        {
            if (FocusedElement != null && HoveredElement != null)
            {
                FocusedElement.WheelChanged(e);
            }
        }

        private void OnMouseMove(object sender, MouseMoveEventArgs e)
        {
            MousePosition = new Vector2(e.X, e.Y);

            UIObject previousCovered = HoveredElement;

            HoveredElement = Elements.FoundFocus(e.X, e.Y, false);

            if (previousCovered != null)
                if (previousCovered != HoveredElement)
                    previousCovered.OnMouseMoveOut(e);

            if (HoveredElement != null)
            {
                HoveredElement.OnMouseMoveIn(e);
            }
        }

        #endregion Private methods

        public void Dispose()
        {
            Input.KeyDown -= OnKeyDown;
            Input.MouseDown -= OnMouseButtonDown;
            Input.MouseUp -= OnMouseButtonUp;
            Input.MouseMove -= OnMouseMove;
            Input.WheelChanged -= Mouse_WheelChanged;
        }
    }
}