﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using GameName1.Extensions;
using GameName1.GameObjects;
using Glide;
using Microsoft.Xna.Framework;

namespace GameName1.Tiles
{
    public abstract class Tile
    {
        /// <summary>
        /// never acces this directly, use TileObjects property
        /// </summary>
        private List<TileObject> _tileObjects;

        protected float _alpha;
        private bool _visible;
        private Tween _currentAnimation;

        public void SetVisible(bool visible)
        {
            if (_visible != visible)
            {
                _visible = visible;
                StopPreviousAnimation();
                _currentAnimation = Tweener.I.Tween(this, new {_alpha = _visible ? 1f : 0.5f}, 1f).Ease(Ease.CubeOut);
            }
        }

        private void StopPreviousAnimation()
        {
            if (_currentAnimation != null)
            {
                _currentAnimation.Cancel();
            }
        }

        public void AddTileObject(TileObject tileObject)
        {
            Debug.Assert(!TileObjects.Contains(tileObject));

            TileObjects.Add(tileObject);
        }

        public void RemoveTileObject(TileObject tileObject)
        {
            Debug.Assert(TileObjects.Contains(tileObject));
            TileObjects.Remove(tileObject);
        }

        public bool HasTileObject(TileObject tileObject)
        {
            return TileObjects.Contains(tileObject);
        }

        public virtual void Draw(Vector2 position)
        {
            var packedTexture = GetTexture();
            SB.I.Draw(packedTexture.Texture,
                new Rectangle((int) position.X, (int) position.Y, Globals.TileSize, Globals.TileSize),
                packedTexture.Source, new Color(new Vector4(_alpha, _alpha, _alpha, _alpha)));
        }

        public abstract Tile Copy();
        protected abstract PackedTexture GetTexture();

        public abstract bool IsSolid{ get; }

        private List<TileObject> TileObjects
        {
            get
            {
                if (_tileObjects == null)
                {
                    _tileObjects = new List<TileObject>();
                }
                return _tileObjects;
            }
        }

        public List<TileObject> GetTileObjects()
        {
            if (_tileObjects == null)
            {
                return new List<TileObject>();
            }

            return TileObjects.ToList();
        }

        public bool HasAnyTileObjects()
        {
            if (_tileObjects == null)
            {
                return false;
            }

            return TileObjects.Count > 0;
        }

        public bool IsVisible
        {
            get { return _visible; }
        }
    }
}
