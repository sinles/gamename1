﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameName1.Extensions;

namespace GameName1.Tiles
{
    class TileWall:Tile
    {
        public override Tile Copy()
        {
            return new TileWall();
        }

        protected override PackedTexture GetTexture()
        {
            return CM.I.GetPacked("WallTile");
        }

        public override bool IsSolid
        {
            get { return true; }
        }
    }
}
