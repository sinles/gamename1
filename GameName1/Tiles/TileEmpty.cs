﻿using GameName1.Extensions;

namespace GameName1.Tiles
{
    public class TileEmpty:Tile
    {
        public override Tile Copy()
        {
            return new TileEmpty();
        }

        protected override PackedTexture GetTexture()
        {
            return CM.I.GetPacked("EmptyTile");
        }

        public override bool IsSolid
        {
            get { return false; }
        }
    }
}
