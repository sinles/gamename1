﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace GameName1
{
    public static class Input
    {
        public static EventHandler<KeyboardKeyEventArgs> KeyDown;
        public static EventHandler<KeyboardKeyEventArgs> KeyUp;

        public static EventHandler<MouseButtonEventArgs> MouseUp;
        public static EventHandler<MouseButtonEventArgs> MouseDown;

        public static EventHandler<MouseMoveEventArgs> MouseMove;
        public static EventHandler<MouseWheelEventArgs> WheelChanged;

        private static Keys[] _previousKeys;
        private static readonly List<Keys> _downKeys = new List<Keys>(16);
        private static readonly List<Keys> _upKeys = new List<Keys>(16);
        private static Point _previousPosition;
        public static void Update()
        {
            _downKeys.Clear();
            _upKeys.Clear();

            UpdateKeyboard();
            UpdateMouse();
        }

        public static bool IsKeyDown(Keys key)
        {
            return _downKeys.Contains(key);
        }

        public static bool IsKeyUp(Keys key)
        {
            return _upKeys.Contains(key);
        }

        public static bool IsKeyPressed(Keys key)
        {
            return _previousKeys.Contains(key);
        }

        private static void UpdateKeyboard()
        {
            var pressedKeys = Keyboard.GetState().GetPressedKeys();

            foreach (Keys key in pressedKeys)
            {
                if (!_previousKeys.Contains(key))
                {
                    if (KeyDown != null)
                    {
                        KeyDown(null, new KeyboardKeyEventArgs {Key = key});
                    }
                    _downKeys.Add(key);
                }
                else
                {
                    if (KeyUp != null)
                    {
                        KeyUp(null, new KeyboardKeyEventArgs {Key = key});
                    }
                    _upKeys.Add(key);
                }
            }

            _previousKeys = pressedKeys;
        }

        private static void UpdateMouse()
        {
            var mouse = Mouse.GetState();

            if (mouse.LeftButton == ButtonState.Pressed)
            {
                if (MouseDown != null)
                {
                    MouseDown(null, new MouseButtonEventArgs {Button = MouseButton.Left, Position = mouse.Position});
                }
            }

            if (mouse.RightButton == ButtonState.Pressed)
            {
                if (MouseDown != null)
                {
                    MouseDown(null, new MouseButtonEventArgs {Button = MouseButton.Right, Position = mouse.Position});
                }
            }


            if (mouse.LeftButton == ButtonState.Released)
            {
                if (MouseUp != null)
                {
                    MouseUp(null, new MouseButtonEventArgs {Button = MouseButton.Left, Position = mouse.Position});
                }
            }

            if (mouse.RightButton == ButtonState.Released)
            {
                if (MouseUp != null)
                {
                    MouseUp(null, new MouseButtonEventArgs {Button = MouseButton.Right, Position = mouse.Position});
                }
            }

            if (_previousPosition.X != mouse.X || _previousPosition.Y != mouse.Y)
            {
                if (MouseMove != null)
                {
                    MouseMove(null, new MouseMoveEventArgs {Position = mouse.Position});
                }
            }
        }


        public static Point GetTileFromMousePosition()
        {
            var state = Mouse.GetState();
            var position = state.Position;
            var worldSpace = Globals.Camera.ToWorldSpace(new Vector2(position.X, position.Y));
            var target = Globals.PositionToTile(worldSpace);
            return target;
        }
    }
}
