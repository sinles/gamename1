﻿using System;

namespace GameName1
{
    public class MouseWheelEventArgs:EventArgs
    {
        public int Change { get; set; }
    }
}
