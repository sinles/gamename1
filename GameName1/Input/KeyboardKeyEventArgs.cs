﻿using System;
using Microsoft.Xna.Framework.Input;

namespace GameName1
{
    public class KeyboardKeyEventArgs:EventArgs
    {
        public Keys Key { get; set; }
    }
}
