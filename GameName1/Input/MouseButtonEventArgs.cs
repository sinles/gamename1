﻿using System;
using Microsoft.Xna.Framework;

namespace GameName1
{
    public class MouseButtonEventArgs:EventArgs
    {
        public MouseButton Button { get; set; }

        public Point Position { get; set; }

        public int X {
            get { return Position.X; }
        }

        public int Y
        {
            get { return Position.Y; }
        }
    }
}
