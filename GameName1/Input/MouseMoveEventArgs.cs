﻿using System;
using Microsoft.Xna.Framework;

namespace GameName1
{
    public class MouseMoveEventArgs:EventArgs
    {
        public Point Position { get; set; }

        public int X 
        {
            get { return Position.X; }
        }
        public int Y
        {
            get { return Position.Y; }
        }
    }
}
