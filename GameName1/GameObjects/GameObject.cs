﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace GameName1.GameObjects
{
    public class GameObject
    {
        private Vector2 _position;

        public GameObject()
        {
            GameObjectManager.Instance.Add(this);
            Active = true;
            Visible = true;
        }

        public void Destroy()
        {
            GameObjectManager.Instance.Remove(this);
        }

        public virtual void Created()
        {

        }

        public virtual void Destroyed()
        {

        }

        public virtual void Update()
        {
        }

        public virtual void Draw()
        {
        }

        protected virtual void PositionChanged(Vector2 oldPosition, Vector2 newPosition)
        {
        }

        public bool Active { get; set; }
        public bool Visible { get; set; }

        public Vector2 Position
        {
            get { return _position; }
            set
            {
                PositionChanged(_position, value);
                _position = value;

            }
        }

        public float X
        {
            get { return Position.X; }
            set { Position = new Vector2(value, Position.Y); }
        }

        public float Y
        {
            get { return Position.Y; }
            set { Position = new Vector2(Position.X, value); }
        }
    }
}
