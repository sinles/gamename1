﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using GameName1.Fight;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameName1.GameObjects
{
    public class CreatureGroup:Creature
    {
        private readonly List<Creature> _creatures;

        public CreatureGroup(Texture2D texture, List<Creature> creatures) 
            : base(texture)
        {
            _creatures = creatures; 
            ResetActionPoints();
            BattlefieldArea = new Rectangle(-10000, -10000, 1000000, 1000000);
            DrawHealth = false;
            Debug.Assert(creatures.All(p => !(p is CreatureGroup)));//do not want to reference groups
        }

        public void Pack()
        {
            Active = true;
            foreach (var creature in Creatures)
            {
                creature.Active = false;
            }
        }

        public List<Creature> Unpack(Rectangle battlefieldArea)
        {
            Active = false;
            foreach (var creature in Creatures)
            {
                creature.CurrentTile = CurrentTile;
                creature.Active = true;
                creature.BattlefieldArea = battlefieldArea;
            }

            return Creatures.ToList();
        }

        public override void Destroyed()
        {
            base.Destroyed();

            foreach (var creature in _creatures)
            {
                creature.Destroy();
            }
        }

        public override bool IsDead
        {
            get { return _creatures.All(p => p.IsDead); }
        }
        public List<Creature> Creatures
        {
            get { return _creatures; }
        }
    }
}
