﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameName1.Extensions;
using GameName1.Level;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameName1.GameObjects
{
    public class Stairs:TileObject, IUseable
    {
        public Stairs()
            : base()
        {
            Solid = false;
        }

        public override void Draw()
        {
            base.Draw();
            SB.I.DrawCentered(CM.I.GetTex("Stairs"), Position, Color.White);
        }

        public void Use(Creature creature)
        {
            World.I.LevelController.GoTo(
                new Level.Level(
                    "Dungeon",
                    World.I.LevelController.Level.Depth + 1,
                    LevelType.Dungeon));
        }
    }
}
