﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameName1.Tiles;
using Microsoft.Xna.Framework;

namespace GameName1.GameObjects
{
    public class GameObjectManager
    {
        public static GameObjectManager Instance;

        private readonly List<GameObject> _gameObjects;
        private readonly Queue<GameObject> _addNewObjectsQueue;
        private readonly Queue<GameObject> _deleteObjectsQueue;

        public GameObjectManager()
        {
            _gameObjects = new List<GameObject>(150);
            _addNewObjectsQueue = new Queue<GameObject>(150);
            _deleteObjectsQueue = new Queue<GameObject>(150);

            Instance = this;
        }
        
        public void Add(GameObject gameObject)
        {
            if (gameObject == null) throw new ArgumentNullException("gameObject");
            _addNewObjectsQueue.Enqueue(gameObject);
        }

        public void Remove(GameObject gameObject)
        {
            if (gameObject == null) throw new ArgumentNullException("gameObject");
            _deleteObjectsQueue.Enqueue(gameObject);
        }

        public void Update()
        {
            for (int i = 0; i < _gameObjects.Count; i++)
            {
                var gameObject = _gameObjects[i];
                if (gameObject.Active)
                {
                    gameObject.Update();
                }
            }
            UpdateObjectsEnqueues();
        }

        public void Draw()
        {
            for (int i = 0; i < _gameObjects.Count; i++)
            {
                var gameObject = _gameObjects[i];
                if (gameObject.Active)
                {
                    if (gameObject.Visible)
                    {
                        gameObject.Draw();
                    }
                }
            }
        }

        public void Clear(params GameObject[] ignoreList)
        {
            UpdateObjectsEnqueues();
            foreach (var gameObject in _gameObjects.Where(gameObject => ignoreList.All(p => p != gameObject)))
            {
                Remove(gameObject);
            }
            UpdateObjectsEnqueues();
        }


        private void UpdateObjectsEnqueues()
        {
            while (_addNewObjectsQueue.Count > 0)
            {
                var gameObject = _addNewObjectsQueue.Dequeue();
                gameObject.Created();
                _gameObjects.Add(gameObject);
            }
            while (_deleteObjectsQueue.Count > 0)
            {
                var obj = _deleteObjectsQueue.Dequeue();
                obj.Destroyed();
                _gameObjects.Remove(obj);
            }
        }
    }
}
