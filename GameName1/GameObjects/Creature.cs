﻿using System.Threading.Tasks;
using GameName1.Extensions;
using GameName1.Fight;
using GameName1.GameObjects;
using GameName1.GameObjects.ActionSystem;
using GameName1.PathFinding;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameName1.GameObjects
{
    public class Creature:TileObject
    {
        protected readonly ActionController ActionController;
        private readonly Texture2D _texture;
        
        public Creature(Texture2D texture) 
            : base()
        {
            _texture = texture;
            ActionController = new ActionController(this);
            Health = 5;
            MaxHealth = 5;
            Scale = 1;
            DrawHealth = true;
            ResetActionPoints();
        }

        public void SetPlayer(Player player)
        {
            Player = player;
        }

        public bool FriendlyTo(Creature otherCreature)
        {
            return Player == otherCreature.Player;
        }

        public void UseActionPoint()
        {
            ActionPoints -= 1;
        }
        public void ResetActionPoints()
        {
            ActionPoints = 1;
        }

        public void Skip()
        {
            ActionPoints = 0;
        }

        public bool Use()
        {
            return ActionController.Do(new UseAction(this));
        }

        public bool Move(int x, int y)
        {
            var direction = new Point(x, y);
            return ActionController.Do(new MoveAction(this, direction));
        }

        public void RecieveDamage(float damage)
        {
            Health -= damage;
            if (IsDead)
            {
                Die();
            }
        }

        public void Die()
        {
            Destroy();
        }

        public override void Update()
        {
            base.Update();

            Visible = World.I.TileMap.IsVisible(CurrentTile);
        }

        public override void Draw()
        {
            SB.I.Draw(_texture, Position, null, Color.White, 0, new Vector2(_texture.Width/2f, _texture.Height/2f),
                Scale, SpriteEffects.None, 0);

            if (DrawHealth)
            {
                SB.I.DrawString(CM.I.GetFont(Fonts.Sample), Health + "/" + MaxHealth, Position - new Vector2(0, 42),  Color.White);
            }

            base.Draw();
        }

        public int ActionPoints { get; private set; }
        public bool HasActionPoints()
        {
            return ActionPoints > 0;
        }

        public float Health { get; set; }
        public float MaxHealth { get; set; }

        public virtual bool IsDead 
        {
            get { return Health <= 0; }
        }

        public float Scale { get; set; }

        public CreatureAlignment Alignment { get; set; }


        public Player Player { get; private set; }

        public Rectangle BattlefieldArea { get; set; }

        public bool DrawHealth { get; set; }

    }
}
