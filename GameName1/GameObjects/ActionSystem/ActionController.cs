﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameName1.GameObjects.ActionSystem
{
    public class ActionController
    {
        private readonly Creature _creature;
        private Action _currentAction;

        public ActionController(Creature creature)
        {
            _creature = creature;
        }

        public void Update()
        {
            _currentAction.Update();
        }

        public bool Do(Action action)
        {
            if (_creature.HasActionPoints())
            {
                if (_currentAction == null || _currentAction.IsEnded)
                {
                    _currentAction = action;
                    return _currentAction.Start();
                }
            }
            return false;
        }
    }
}
