﻿using System.Linq;

namespace GameName1.GameObjects.ActionSystem
{
    public class UseAction:Action
    {
        public UseAction(Creature creature) 
            : base(creature)
        {
        }

        protected override bool DoStart()
        {
            var tileObjects = World.I.TileMap.GetTileObjects(Creature.CurrentTile).Where(p => p is IUseable).ToList();

            if(tileObjects.Count > 0)
            {
                IUseable useable = tileObjects.First() as IUseable;
                useable.Use(Creature);
                End();
                return true;
            }

            return false;
        }
    }
}
