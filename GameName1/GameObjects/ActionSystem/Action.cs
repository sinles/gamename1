﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;

namespace GameName1.GameObjects.ActionSystem
{
    public abstract class Action
    {
        private readonly Creature _creature;


        public Action(Creature creature)
        {
            _creature = creature;
        }

        public bool Start()
        {
            bool started = DoStart();
            if (!started)
            {
                End();
            }

            return started;
        }

        protected virtual bool DoStart()
        {
            return true;
        }

        public virtual void Update()
        {

        }

        public void End()
        {
            IsEnded = true;
        }

        public bool IsEnded { get; private set; }
        protected Creature Creature 
        {
            get
            {
                return _creature;
            }
        }
    }
}
