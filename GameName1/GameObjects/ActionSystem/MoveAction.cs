﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameName1.Extensions;
using Glide;
using Microsoft.Xna.Framework;

namespace GameName1.GameObjects.ActionSystem
{
    public class MoveAction:Action
    {
        private readonly Point _direction;

        private Point _targetTile;
        private bool _attacking;

        private float _elapsed;

        private Vector2 _startPosition;
        private Vector2 _endPosition;


        public MoveAction(Creature creature, Point direction) 
            : base(creature)
        {
            _direction = direction;
        }

        protected override bool DoStart()
        {
            base.DoStart();

            var newTile = Creature.CurrentTile.Add(_direction);

            if (World.I.TileMap.IsFree(newTile))
            {
                return TryMoveToTile(newTile);
            }

            return false;
        }

        private bool TryMoveToTile(Point newTile)
        {
            if (!World.I.TileMap.InRange(newTile) || !Creature.BattlefieldArea.Contains(newTile))
            {
                return false;
            }

            _targetTile = newTile;

            var creatureOnTile = World.I.TileMap.GetCreatureFromTile(_targetTile);
            _startPosition = Creature.Position;
            _endPosition = Globals.TileToPosition(_targetTile);
            if (creatureOnTile != null)
            {
                if (!creatureOnTile.FriendlyTo(Creature))
                {
                    _attacking = true;



                    Tweener.I.Tween(Creature, new {X = _endPosition.X, Y = _endPosition.Y}, 0.15f)
                        .Ease(Ease.BackOut)
                        .OnComplete(() =>
                        {
                            Tweener.I.Tween(Creature, new {X = _startPosition.X, Y = _startPosition.Y}, 0.15f)
                                .Ease(Ease.BackOut)
                                .OnComplete(() =>
                                {
                                    Creature.UseActionPoint();
                                    creatureOnTile.RecieveDamage(1.0f);
                                    End();
                                });
                        });


                    return true;
                }
                return false;
            }



            Tweener.I.Tween(Creature, new { X = _endPosition.X, Y = _endPosition.Y }, 0.2f)
                .Ease(Ease.BackOut)
                .OnComplete(
                () =>
                {
                    Creature.UseActionPoint();
                    End();
                });

            return true;
        }

        public override void Update()
        {
            base.Update();
        }
    }
}
