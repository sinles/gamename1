﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using GameName1.Fight;
using GameName1.Level;
using GameName1.Tiles;
using Microsoft.Xna.Framework;

namespace GameName1.GameObjects
{
    public class World
    {
        public static World I { get; private set; }

        public TileMap TileMap { get; private set; }
        public GameObjectManager GameObjectManager { get; private set; }

        public TravelManager TravelManager { get; private set; }
        public CameraControler CameraControler { get; private set; }
        public LevelController LevelController { get; private set; }


        public World()
        {
            TileMap = new TileMap(23, 23);
            TileMap.FillWith(new TileEmpty());

            GameObjectManager= new GameObjectManager();
            TravelManager =new TravelManager();
            LevelController = new LevelController();

            I = this;


            CameraControler = new CameraControler
            {
                Sensetivity = 8000,
                MouseBorder = 0.1f,
                ZoomSensetivity = 0.005f,
                Camera = { Position = new Vector2(0,0)}
            };
            Globals.Camera = CameraControler.Camera;

            LevelController.LevelChanged += LevelControllerOnLevelChanged;
        }

        public void Update()
        {
            GameObjectManager.Update();
            CameraControler.Update();
        }
        
        public void Draw()
        {
            TileMap.Draw(CameraControler.Camera.TileBounds);

            GameObjectManager.Draw();
        }

        private void LevelControllerOnLevelChanged()
        {
            TileMap = LevelController.Level.GetTileMap();

            TravelManager.MainPlayer.MainCreature.CurrentTile = TileMap.GetFreeRandomPoint();
            CameraControler.Camera.TargetPosition = TravelManager.MainPlayer.MainCreature.Position;

            TravelManager.CleanOtherPlayers();

            LevelController.Level.CreateLevelObjects();
        }
    }
}
