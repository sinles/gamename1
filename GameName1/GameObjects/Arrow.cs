﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameName1.Extensions;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameName1.GameObjects
{
    public class Arrow:GameObject
    {
        private const float Speed = 4f;
        private const float Amount = 0.25f;
        private const float Offset = Globals.TileSize/2 + 12;



        private readonly Texture2D _texture;
        private float _elapsed;
        private float _offset;


        public Arrow()
            :base()
        {
            _texture = CM.I.GetTex("Arrow");
        }

        public override void Update()
        {
            base.Update();

            _offset += (float)Math.Cos(_elapsed * Speed) * Amount;
            _elapsed += Game.DT;
        }

        public override void Draw()
        {
            base.Draw();
            SB.I.DrawCentered(_texture, Position + new Vector2(0, _offset - Offset), Color.White);
        }
    }
}
