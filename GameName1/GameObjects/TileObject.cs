﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameName1.GameObjects.ActionSystem;
using GameName1.Tiles;
using Microsoft.Xna.Framework;

namespace GameName1.GameObjects
{
    public class TileObject:GameObject
    {
        public TileObject() 
            : base()
        {
            Solid = true;
        }

        public override void Destroyed()
        {
            base.Destroyed();
            World.I.TileMap.RemoveObjectFromTile(this);
        }

        protected override void PositionChanged(Vector2 oldPosition, Vector2 newPosition)
        {
            base.PositionChanged(oldPosition, newPosition);

            var oldTile = Globals.PositionToTile(oldPosition);
            var newTile = Globals.PositionToTile(newPosition);

            World.I.TileMap.SetObjectTile(this, oldTile, newTile);
        }

        public Point CurrentTile
        {
            get { return Globals.PositionToTile(Position); }
            set { Position = Globals.TileToPosition(value); }
        }

        public bool Solid { get; set; }
    }
}
