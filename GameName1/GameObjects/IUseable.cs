﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameName1.GameObjects
{
    interface IUseable
    {
        void Use(Creature creature);
    }
}
