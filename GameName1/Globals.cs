﻿using GameName1.Fight;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameName1
{
    public static class Globals
    {
        public const int TileSize = 64;

        public static Viewport Viewport;
        public static Camera Camera;

        public static Vector2 TileToPosition(Point tile)
        {
            return new Vector2(tile.X * TileSize + TileSize / 2, tile.Y * TileSize + TileSize / 2);
        }

        public static Point PositionToTile(Vector2 position)
        {
            return new Point((int)(position.X / TileSize), (int)(position.Y / TileSize));
        }
    }
}
