﻿using System;
using System.Collections.Generic;
using GameName1.Extensions;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Rectangle = Microsoft.Xna.Framework.Rectangle;


namespace GameName1
{
    /// <summary>Content Manager</summary>
    public class CM
    {
        private readonly ContentManager _content;
        /// <summary>Instance</summary>
        public static CM I { get; private set; }

        private readonly Dictionary<string, Texture2D> _textures;
        private readonly Dictionary<Fonts, SpriteFont> _fonts;
        private readonly Dictionary<string, PackedTexture> _packedTextures;


        // private readonly Dictionary<string, Audio> _sounds;

        public CM(ContentManager content)
        {
            if (I != null)
            {
                throw new Exception("two instances");
            }

            I = this;

            _content = content;
            _textures = new Dictionary<string, Texture2D>();
            _fonts = new Dictionary<Fonts, SpriteFont>();
            _packedTextures = new Dictionary<string, PackedTexture>();

            //   _sounds = new Dictionary<string, AudioClip>();
        }

        public void LoadTexture(string asset, string file)
        {
            if (!_textures.ContainsKey(asset))
                _textures.Add(asset, _content.Load<Texture2D>(file));
        }

        public void LoadTexture(string asset, Texture2D texture)
        {
            if (!_textures.ContainsKey(asset))
                _textures.Add(asset, texture);
        }



        public void AddPackedTexture(string asset, string packedTextureName, Rectangle source)
        {
            _packedTextures[packedTextureName] = new PackedTexture(_textures[asset], source);
        }

        public void LoadFont(Fonts asset, string file)
        {
            if (!_fonts.ContainsKey(asset))
                _fonts.Add(asset, _content.Load<SpriteFont>(file));
        }

//        public void LoadSound(string asset, string file)
//        {
//            if (!_sounds.ContainsKey(asset))
//                _sounds.Add(asset, new AudioClip(file));
//        }
        public void UnloadTexture(string texture)
        {
            _textures[texture].Dispose();
            _textures.Remove(texture);
        }

        public Texture2D GetTex(string asset)
        {
             return _textures[asset];
        }

        public PackedTexture GetPacked(string asset)
        {
            return _packedTextures[asset];
        }

        public SpriteFont GetFont(Fonts asset)
        {
            return _fonts[asset];
        }
//        public AudioClip Sound(string asset)
//        {
//            return _sounds[asset];
//        }
    }

    public enum Fonts
    {
        Sample
    }
}
