﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameName1.Level
{
    public class LevelController
    {
        public LevelController()
        {

        }

        public void SetDefaultLevel()
        {
            Level = new Level("Dungeon", 0, LevelType.Dungeon);
        }

        public void GoTo(Level level)
        {
            if (Level != null)
            {
                Level.Clean();
            }

            Level = level;

            if (LevelChanged != null)
            {
                LevelChanged();
            }
        }

        public event Action LevelChanged;
        public Level Level { get; private set; }
    }
}
