﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameName1.Fight;
using GameName1.GameObjects;
using GameName1.LevelGeneration;
using GameName1.Tiles;
using Microsoft.Xna.Framework;

namespace GameName1.Level
{
    public enum LevelType
    {
        Dungeon
    }

    public class Level
    {
        private List<TileObject> _tileObjects; 

        public Level(string name, int depth, LevelType levelType)
        {
            Name = name;
            Depth = depth;
            LevelType = levelType;
            _tileObjects = new List<TileObject>();
        }

        public TileMap GetTileMap()
        {
            var tileMap = new TileMap(23, 23);
            DungeonGenerator dungeonGenerator = new DungeonGenerator(tileMap);
            dungeonGenerator.Generate();

            return tileMap;
        }

        public void CreateLevelObjects()
        {
            var enemy = new Creature(CM.I.GetTex("SomeGuy"))
            {
                CurrentTile = new Point(4, 5),
                Scale = 2,
                Active = false
            };
            var dummyPlayer = new DummyPlayer();
            dummyPlayer.SetCreatures(new List<Creature>
            {
                new CreatureGroup(CM.I.GetTex("SomeGuy"), new List<Creature> {enemy})
                {
                    CurrentTile =World.I.TileMap.GetFreeRandomPoint(),
                    Scale = 2
                }
            });

            World.I.TravelManager.AddPlayer(dummyPlayer);



            AddObject(new Stairs {CurrentTile = World.I.TileMap.GetFreeRandomPoint()});
        }

        public void Clean()
        {
            foreach (var tileObject in _tileObjects)
            {
                tileObject.Destroy();
            }
        }

        public override string ToString()
        {
            return Name + " " + Depth;
        }

        private void AddObject(TileObject tileObject)
        {
            _tileObjects.Add(tileObject);
        }

        public string Name { get; private set; }
        public int Depth { get; private set; }
        public LevelType LevelType { get; private set; }
    }
}
