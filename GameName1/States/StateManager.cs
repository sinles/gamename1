﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace GameName1.States
{
    public class StateManager : IDisposable
    {
        private State _currentState;
        private State _nextGameState;
        public StateManager()
        {
            Exit = false;
        }


        public void SetState(State gameState)
        {
            _nextGameState = gameState;
            if (_nextGameState != null)
            {
                Debug.WriteLine("Change state to " + _nextGameState.GetType().Name);

                ChangeState();
                _nextGameState = null;
            }
        }

        public void Update()
        {
            _currentState.Update();
        }

        public void Draw()
        {
            _currentState.Draw();
        }


        private void ChangeState()
        {
            if (_currentState != null)
            {
                _currentState.UnloadContent();
                _currentState.Dispose();
            }
            _currentState = _nextGameState;
            _currentState.LoadContent();
            _currentState.Init();
        }

        public void Dispose()
        {
            if (_currentState != null)
                _currentState.Dispose();
            if (_nextGameState != null)
                _nextGameState.Dispose();
        }

        public bool Exit { get; set; }
    }
}
