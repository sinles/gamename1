﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using GameName1.Fight;
using GameName1.GameObjects;
using GameName1.Level;
using GameName1.LevelGeneration;
using GameName1.Tiles;
using Microsoft.Xna.Framework;

namespace GameName1.States
{
    public class LoadGameState:State
    {
        public LoadGameState(StateManager stateManager) : base(stateManager)
        {
        }

        internal override void LoadContent()
        {
            CM.I.LoadTexture("SomeGuy", "Graphics\\SomeWarrior");
            // CM.I.LoadTexture("EmptyTile", "Graphics\\Sand");
            CM.I.LoadTexture("EmptyTile", "Graphics\\WalkableTile");
            CM.I.LoadTexture("WalkableTile", "Graphics\\WalkableTile");
            CM.I.LoadTexture("PathPoint", "Graphics\\PathPoint");
            CM.I.LoadTexture("Tiles", "Graphics\\floor-tiles-20x20");
            CM.I.LoadTexture("Arrow", "Graphics\\Arrow");
            CM.I.LoadTexture("Stairs", "Graphics\\Steps-orange");

            const int tileSize = 20;
            CM.I.AddPackedTexture("Tiles", "EmptyTile", new Rectangle(1*tileSize, 0, tileSize, tileSize));
            CM.I.AddPackedTexture("Tiles", "WallTile", new Rectangle(4*tileSize, 9*tileSize, tileSize, tileSize));
            CM.I.AddPackedTexture("Tiles", "Darkness", new Rectangle(19*tileSize, 0*tileSize, tileSize, tileSize));


            CM.I.LoadFont(Fonts.Sample, "Fonts\\NewSpriteFont");
        }

        internal override void UnloadContent()
        {
        }

        internal override void Init()
        {
            new World();

            DungeonGenerator dungeonGenerator = new DungeonGenerator(World.I.TileMap, false);
            dungeonGenerator.Generate();



            var hero = new Creature(CM.I.GetTex("SomeGuy"))
            {
                CurrentTile = new Point(4, 4),
                Scale = 2,
                Active = false
            };

            var hero2 = new Creature(CM.I.GetTex("SomeGuy"))
            {
                CurrentTile = new Point(8, 4),
                Scale = 2,
                Active = false
            };

            World.I.TravelManager.MainPlayer.SetCreatures(new List<Creature>
            {
                new CreatureGroup(CM.I.GetTex("SomeGuy"), new List<Creature> {hero, hero2})
                {
                    CurrentTile = World.I.TileMap.GetFreeRandomPoint(),
                    Scale = 2
                }
            });

            World.I.TravelManager.SetPlayers(new List<Player>
            {
                World.I.TravelManager.MainPlayer
            });

            World.I.LevelController.GoTo(new Level.Level("Dungeon", 1, LevelType.Dungeon));

            World.I.TravelManager.Start();

            StateManager.SetState(new TravelState(StateManager));
        }

        internal override void Update()
        {
        }

        internal override void Draw()
        {
        }
    }
}
