﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameName1.Extensions;
using GameName1.Fight;
using GameName1.GameObjects;
using GameName1.GUI;
using GameName1.GUI.Elements;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameName1.States
{
    public class TravelState:State
    {
        private GUIManager _guiManager;
        private Label _label;

        public TravelState(StateManager stateManager) 
            : base(stateManager)
        {
            World.I.TravelManager.BattleStarted += TravelManagerOnBattleStarted;
            _guiManager = new GUIManager();
        }

            

        internal override void LoadContent()
        {
        }

        internal override void UnloadContent()
        {
        }

        internal override void Init()
        {
            World.I.TravelManager.ActivateTravel();
            World.I.CameraControler.TargetZoom = 0.5f;
            World.I.CameraControler.Camera.TargetPosition = World.I.TravelManager.MainPlayer.MainCreature.Position;

            _label = new Label(CM.I.GetFont(Fonts.Sample)) {Text = World.I.LevelController.Level.ToString()};
            _guiManager.Add(_label);
        }

        internal override void Update()
        {
            _label.Text = World.I.LevelController.Level.ToString();
            World.I.TravelManager.Update();
            World.I.Update();
            World.I.TileMap.UpdateVisibleTiles(World.I.TravelManager.MainPlayer.MainCreature.CurrentTile, World.I.CameraControler.Camera.TileBounds);
        }

        internal override void Draw()
        {
            SB.I.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, null, null, null, null, World.I.CameraControler.Camera.GetMatrix());

            World.I.Draw();
            World.I.TravelManager.Draw();

            SB.I.End();

            SB.I.Begin();
            _guiManager.Draw(Game.DT);
            SB.I.End();
        }


        private void TravelManagerOnBattleStarted(Player player)
        {
            World.I.TravelManager.BattleStarted -= TravelManagerOnBattleStarted;
            StateManager.SetState(new FightState(StateManager, player));
        }       
    }
}
