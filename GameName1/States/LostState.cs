﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameName1.Extensions;
using Glide;
using Microsoft.Xna.Framework;

namespace GameName1.States
{
    public class LostState:State
    {
        private float _x;

        public LostState(StateManager stateManager) : base(stateManager)
        {
        }

        internal override void LoadContent()
        {
            
        }

        internal override void UnloadContent()
        {
        }

        internal override void Init()
        {
            _x = 0;
            Tweener.I.Tween(this, new {_x = Globals.Viewport.Width*0.5f}, 2.0f).Ease(Ease.BounceOut);
        }

        internal override void Update()
        {
        }

        internal override void Draw()
        {
            SB.I.Begin();

            SB.I.DrawString(CM.I.GetFont(Fonts.Sample), "You lost", new Vector2(_x, Globals.Viewport.Height*0.5f), Color.White);

            SB.I.End();
        }
    }
}
