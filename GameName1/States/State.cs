﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameName1.States
{
    public abstract class State : IDisposable
    {
        protected StateManager StateManager;

        public State(StateManager stateManager)
        {
            StateManager = stateManager;
        }

        internal abstract void LoadContent();
        internal abstract void UnloadContent();

        internal abstract void Init();
        internal abstract void Update();
        internal abstract void Draw();

        public virtual void Dispose()
        {
            UnloadContent();
        }
    }
}
