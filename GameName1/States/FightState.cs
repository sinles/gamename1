﻿using System;
using System.Collections.Generic;
using GameName1.Extensions;
using GameName1.Fight;
using GameName1.GameObjects;
using GameName1.Tiles;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameName1.States
{
    public class FightState:State
    {
        private readonly Player _otherPlayer;
        private FightManager _fightManager;
        private Rectangle _battlefieldArea;

        public FightState(StateManager stateManager, Player otherPlayer) : base(stateManager)
        {
            _otherPlayer = otherPlayer;
        }

        internal override void LoadContent()
        {

        }

        internal override void UnloadContent()
        {
        }

        internal override void Init()
        {
            CalculateBattlefieldArea();

            _battlefieldArea = CalculateBattlefieldArea();

            var humanPlayer = new HumanPlayer();
            humanPlayer.SetCreatures(World.I.TravelManager.MainPlayer.UnpackCreatures(_battlefieldArea));
            humanPlayer.ActivateCreatures(true);

            var otherPlayer = new SimpleAIPlayer(humanPlayer);
            otherPlayer.SetCreatures(_otherPlayer.UnpackCreatures(_battlefieldArea));
            otherPlayer.ActivateCreatures(true);


            _fightManager = new FightManager();
            _fightManager.SetMainPlayer(humanPlayer);
            _fightManager.SetPlayers(new List<Player>
            {
                humanPlayer,
                otherPlayer
            });

            _fightManager.Start();

            World.I.CameraControler.TargetZoom = 0.6f;
        }

        internal override void Update()
        {
            _fightManager.Update();
            World.I.Update();
            World.I.TileMap.SetVisibleRect(_battlefieldArea);


            if (_fightManager.FightEnded)
            {
                _fightManager.Deinit();

                if (_fightManager.MainPlayerLost)
                {
                    StateManager.SetState(new LostState(StateManager));
                }
                else
                {
                    StateManager.SetState(new TravelState(StateManager));
                }
            }
        }

        internal override void Draw()
        {
            SB.I.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, null, null, null, null, World.I.CameraControler.Camera.GetMatrix());

            World.I.Draw();
            _fightManager.Draw();

            DrawDarkness();

            SB.I.End();
        }

        private void DrawDarkness()
        {
            var darkness = CM.I.GetPacked("Darkness");

            const int offset = 4000;
            Vector2 position = Globals.TileToPosition(new Point(_battlefieldArea.X, _battlefieldArea.Y));
            position.Y -= offset;

            SB.I.Draw(darkness.Texture,
                new Rectangle((int) position.X - Globals.TileSize/2,
                    (int) position.Y,
                    _battlefieldArea.Width*Globals.TileSize,
                    -Globals.TileSize/2 + offset),
                darkness.Source, Color.White);


            position = Globals.TileToPosition(new Point(_battlefieldArea.X, _battlefieldArea.Bottom));
            SB.I.Draw(darkness.Texture,
                new Rectangle((int) position.X - Globals.TileSize/2,
                    (int) position.Y - Globals.TileSize/2,
                    _battlefieldArea.Width*Globals.TileSize,
                    Globals.TileSize + offset),
                darkness.Source, Color.White);

            position = Globals.TileToPosition(new Point(_battlefieldArea.X, _battlefieldArea.Y));
            position.Y -= offset;
            position.X -= offset;
            SB.I.Draw(darkness.Texture,
                new Rectangle((int)position.X,
                    (int)position.Y,
                    -Globals.TileSize + offset + Globals.TileSize/2,
                    Globals.TileSize + offset*3),
                darkness.Source, Color.White);


            position = Globals.TileToPosition(new Point(_battlefieldArea.Right, _battlefieldArea.Y));
            position.Y -= offset;
            SB.I.Draw(darkness.Texture,
                new Rectangle((int)position.X - Globals.TileSize / 2,
                    (int)position.Y,
                    -Globals.TileSize + offset + Globals.TileSize / 2,
                    Globals.TileSize + offset * 3),
                darkness.Source, Color.White);

        }

        private Rectangle CalculateBattlefieldArea()
        {
            Point p1 = World.I.TravelManager.MainPlayer.MainCreature.CurrentTile;
            Point p2 = _otherPlayer.MainCreature.CurrentTile;

            int top = Math.Min(p1.Y, p2.Y);
            int bottom = Math.Max(p1.Y, p2.Y);
            int left = Math.Min(p1.X, p2.X);
            int right = Math.Max(p1.X, p2.X);
            const int bonus = 5;
            Rectangle rect = new Rectangle(left - bonus, top - bonus, right - left + bonus*2, bottom - top + bonus*2);

            return rect;
        }
    }   
}
